This webapp is still considered a development bad practice due to messy codes and unstandardized things, I was still learning React when I was developing it.

**Installation**
1. Run npm install on the client and server folders.
2. Change the .env file content according to your machine settings.
3. Database schema will be created automatically on server's start up by using Sequelize's auto alter.

**Run for development**

Run npm run dev on both client and server folders.

**Run for production**

Run npm run prod on both client and server folders.
