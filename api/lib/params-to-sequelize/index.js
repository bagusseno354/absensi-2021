const { _ } = require('lodash');
const { Sequelize } = require('sequelize');
const restfulFilter = require('restful-filter');

{
    DataContent: {
        attributes:
        where: {
            id__eq: 'val'
        }
        include: {
            Kelompok: {
                Desa: {
    
                }
            }
        }
    }
}

/*
 * Example: ?user[cars[spareparts]]
*/
function loopAssociations(associations, model)
{  
  const key = Object.keys(associations)[0]
  const cleanedKey = key.split('_')[0]

  if(!model.associations[cleanedKey])
    return
    
  let nestedInclude = {
    model: model.associations[cleanedKey].target,
    as: cleanedKey,
    
  }

  if(typeof associations[key] == 'object')
    nestedInclude.include = loopAssociations(associations[key], model.associations[cleanedKey].target)

  return nestedInclude 
}

// from https://github.com/slaveofcode/restful-filter/issues/2 with modifications
module.exports.paramsToSequelize = (queryParams, model) => {
  if (Object.keys(queryParams || {}).length > 0) {
    const processedParams = { where: {}, include: [] };
    const filter = restfulFilter();
    const { Op } = Sequelize;

    // separate all $attrs and $attrs_
    // $attrs -> set default table attributes to get
    // $attrs_ ->set associated table attributes to get
    const $attrs_ = {};
    const $attrs = queryParams.$attrs ? queryParams.$attrs.split(',') : null;

    console.log(JSON.stringify(queryParams));
    Object.keys(queryParams).forEach(queryParam => 
    {
      // cleaning data, turn %20 to space
      if(typeof queryParams[queryParam] == 'string')
        queryParams[queryParam] = queryParams[queryParam].replace(/%20/g, ' ')

      // search for association attributes
      if(queryParam.startsWith('$attrs_'))
      {
        const associatedName = queryParam.split('_')[1];
        $attrs_[associatedName] = queryParams[queryParam].split(',')
        delete queryParams[queryParam];        
      }
    });

    console.log(queryParams);

    processedParams.attributes = $attrs;

    const allowedColumns = Object.keys(model.rawAttributes);

    Object.keys(model.associations).forEach((associatedName) => 
    {
      const associatedModel = model.associations[associatedName].target;
      const columns = Object.keys(associatedModel.rawAttributes).map(a => `${associatedName}_${a}`);
      allowedColumns.push(...columns);

      if (Object.keys(queryParams).some(p => p.startsWith(`${associatedName}_`))) {
        const include = {
          model: associatedModel,
          as: associatedName,
          attributes: $attrs_[associatedName],           
          where: {},
        }

        if(typeof queryParams[`${associatedName}_`] == 'object')
          include.include = loopAssociations(queryParams[`${associatedName}_`], associatedModel)
        
        console.log(include);
        processedParams.include.push(include);
      }
    });

    const rawParams = filter.parse(queryParams, allowedColumns);
    console.log('RAW PARAMZ');
    console.log(rawParams);
    _.forIn(rawParams.filter, (item) => {
      const { operator, column, value } = item;
      if (column.includes('_')) {
        const [ asso, name ] = column.split('_');
        _.set(processedParams.include.find(a => a.as === asso),
          [ 'where', name, Op[operator.substr(1)] ],
          value);
      } else {
        _.set(processedParams,
          [ 'where', (column), Op[operator.substr(1)] ],
          value);
      }
    });
    _.set(processedParams, [ 'order' ], rawParams.order);
    _.assign(processedParams, rawParams.paginate);
    console.log('PROCEZZED');
    console.log(processedParams);
    return processedParams;
  }
  return {};
};