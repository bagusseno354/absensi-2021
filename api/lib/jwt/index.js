'use strict';

const jwt = require('jsonwebtoken');

const generateJwtToken = (payload) => {
    return jwt.sign(payload, process.env.JWT_SECRET_KEY)
}

module.exports = {generateJwtToken}