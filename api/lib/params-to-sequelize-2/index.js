// A powerful library to translate query parameters to sequelize

const {Op} = require('sequelize')

function sequelizeWheres(wheres, newWheres) // process wheres
{
  if(!wheres) 
    return

  if(!Array.isArray(wheres))
    Object.keys(wheres).forEach(key =>
    {
      console.log(key);
      let newKey = key // default
      let opKey = null

      if(key.startsWith('__') || key.startsWith('[__'))
      {
        if(key.startsWith('__'))
          opKey = key.split('__')[1]
        else if (key.startsWith('[__'))
          opKey = key.split('[__')[1].slice(0, -1)
          
        newKey = Op[opKey]    

        // override __in  
        if(opKey == 'in')
        {
          newWheres[newKey] = wheres[key].split(',')
          return
        }
      }

      // check if parent is array
      if(!Array.isArray(wheres))
        newWheres[newKey] = {}

      if(Array.isArray(wheres[key]))      
        newWheres[newKey] = []      

      if(typeof wheres[key] == 'object' && !Array.isArray(wheres[key]))
        sequelizeWheres(wheres[key], newWheres[newKey])    
      else               
      {
        if(!Array.isArray(wheres))
          newWheres[newKey] = wheres[key]
        else        
          newWheres.push(wheres[key])        
      }
    })
  else
  {
    let i = 0
    wheres.forEach(where =>
    {      
      newWheres[i] = {}
      sequelizeWheres(where, newWheres[i])
      i++
    })
  }
}

function sequelizeIncludes(includes, newIncludes, model)
{
  if(!includes)
    return

  Object.keys(includes).forEach(key =>
  {
    if(!model.associations[key])
      return;

    const include = {
      model: model.associations[key].target, 
      as: key,
      required: includes[key].required ? true : false,
      where: {},
      include: [],
    }

    // apply scope
    if(includes[key].attributes)
    {
      const attributes = includes[key].attributes.split(',')

      attributes.forEach((value, index) =>
      {
        if(model.options.defaultScope.attributes)
          if(model.options.defaultScope.attributes.exclude)
            if(model.options.defaultScope.attributes.exclude.includes(value))
              attributes.splice(index, index+1);
      })

      include.attributes = attributes
    }

    sequelizeWheres(includes[key].where, include.where);

    if(includes[key].include && typeof includes[key].include == 'object')
      sequelizeIncludes(includes[key].include, include.include, model.associations[key].target)

    newIncludes.push(include)
  })
}

function paramsToSequelize(queryParams, model, scope)
{
  const processedParams = {}
  processedParams.include = []
  processedParams.where = {}

  // apply scope
  if(queryParams.attributes)
  {
    const attributes = queryParams.attributes.split(',')
    
    if(scope)
      scope = model.options.scopes[scope]
    else
      scope = model.options.defaultScope

    attributes.forEach((value, index) =>
    {
      if(scope.attributes.exclude)
        if(scope.attributes.exclude.includes(value))
          attributes.splice(index, index+1);
    })

    processedParams.attributes = attributes
  }

  if(queryParams.order)
    processedParams.order = [[queryParams.order.split(',')]]

  sequelizeWheres(queryParams.where, processedParams.where, model)
  sequelizeIncludes(queryParams.include, processedParams.include, model)

  return processedParams
}

module.exports = {sequelizeIncludes, sequelizeWheres, paramsToSequelize}