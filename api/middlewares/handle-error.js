const { ValidationError } = require('sequelize');

module.exports.handleError = (app) =>
{
  app.use((e, req, res, next) =>
  {
    // default error status
    res.status(500);
    let error = 'internal server error';

    // message of ValidationError of Sequelize is safe to be shown
    if(e instanceof ValidationError)
    {
      // default validation error status
      res.status(400);

      if(e.errors[0].validatorKey == 'not_unique')
        res.status(409)

      error = e.errors[0].message
    }
    
    console.log(e);

    return res.json({
      error
    })
  })
}