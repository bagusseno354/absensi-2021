const validateRequiredFields = (...fields) => {
  return (req, res, next) => 
  {
    try
    {
        let required = [];

        fields.forEach(e => 
        {
          if(!(e in req.body)) 
          {
            required.push(e);
          }
        });

        if(required.length > 0)
          return res.status(400).json({
            'message': `Field is required: ${required.toString()}`
          })

        return next();
    } 
    catch(error) 
    {
        console.log(error);
        
        return res.status(500).json({
          'message': 'Internal server error'
        });
    }
  }
}

module.exports = {validateRequiredFields};