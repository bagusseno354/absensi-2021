'use strict';

const jwt = require('jsonwebtoken');
const models = require('../db/models');
const permissions = require('../db/predefined/permissions');
const variableOperator = require('../lib/variable-operator')

const getUser = () =>
{
  return (req, res, next) =>
  {
    const token = req.cookies.token;

    if(token)
    {
      jwt.verify(token, process.env.JWT_SECRET_KEY, (err, payload) => 
      {
        if(err)
          return console.log(err);

        req.user = payload;  

        if(req.user.username)
          req.user.username = req.user.username.toString().toLowerCase();
      }); 
    }
    
    return next();
  }
}

// can only be placed after getUser
const getFullUser = () =>
{
  return async (req, res, next) =>
  {
    if(req.user)
    {
      let user = await models.User.findByPk(req.user.id);
  
      req.user = {...req.user, ...user};

      if(req.user.username)
        req.user.username = req.user.username.toString().toLowerCase();
    }

    return next();
  }
}

const onlyAllowRole = (role) =>
{
  return (req, res, next) =>
  {
    if(req.user.role != role)
      return res.sendStatus(403);

    return next();
  }
}

const mustHavePermisisons = (...permissions) =>
{
  return async (req, res, next) =>
  {
    if(!req.user)
      return res.sendStatus(401);

    let role_permissions = await models.Role_permission.findAll({
      attributes: ['fkPermission_id'],
      where: {
        fkRole_id: req.user.role
      }
    });
    
    const userPermissions = role_permissions.map(role_permission => role_permission.fkPermission_id);

    for(let i = 0; i < permissions.length; i++)
      if(!userPermissions.includes(permissions[i]))
        return res.sendStatus(403);

    return next();     
  }
}

module.exports = {
  onlyAllowRole, 
  getUser,
  getFullUser,
  mustHavePermisisons,
};