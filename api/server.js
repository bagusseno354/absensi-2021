require('dotenv').config();

const http = require('http');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const { route } = require('./routes')
const { handleError } = require('./middlewares/handle-error')
const { getUser } = require('./middlewares/auth');
const cors = require('cors');

require('./db/models')

app.use(cors({
  origin: process.env.APP_BASE_URL,
  credentials: true
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

// set timeout
app.use((req, res, next) =>
{
  res.setTimeout(60000, () =>
  {
    console.log('Request has timed out.');
    res.sendStatus(408);
  });

  next();
})

// get user for all pages
app.use(getUser());

route(app);
handleError(app);

http.createServer(app).listen(process.env.PORT, err =>
{
  if (err) console.log(err);

  console.log(`<Running with config ${process.env.NODE_ENV} on port ${process.env.PORT}`);
})