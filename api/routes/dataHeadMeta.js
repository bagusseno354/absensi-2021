const jwt = require('../lib/jwt');
const router = require('express').Router();
const models = require('../db/models')
const { paramsToSequelize } = require('../lib/params-to-sequelize-2');
const permissions = require('../db/predefined/permissions')
const { mustHavePermisisons } = require('../middlewares/auth')

router.get('/api/dataHeadMeta', 
  async(req, res, next) =>
{
  const filters = paramsToSequelize(req.query, models.DataHeadMeta)  

  let result;

  try
  {
    result = await models.DataHeadMeta.findAll(filters);
  } 
  catch(error)
  {
    return next(error);
  }

  if(result.length == 0)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.get('/api/dataHeadMeta/:id', 
  async(req, res, next) =>
{
  const filters = paramsToSequelize(req.query, models.DataHeadMeta)  
  const id = req.params.id;

  let result;

  try
  {
    result = await models.DataHeadMeta.findByPk(id, filters);
  } 
  catch(error)
  {
    return next(error);
  }

  if(!result)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.post('/api/dataHeadMeta', 
  mustHavePermisisons('create_dataHeadMeta'),
  async(req, res, next) =>
{
  req.body.fkUser_creatorId = req.user.id;

  if(!Array.isArray(req.body))
    req.body = [req.body];

  await models.DataHeadMeta.bulkCreate(req.body, {returning: true})
  .then(dataHeadMetas =>
  {
    return res.status(200).json(dataHeadMetas);
  })
  .catch(e => 
  {
    return next(e);
  });
});

router.patch('/api/dataHeadMeta/:id', 
  mustHavePermisisons(permissions.update_dataHeadMeta.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.DataHeadMeta.update(req.body, {
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

router.delete('/api/dataHeadMeta/:id', 
  mustHavePermisisons(permissions.delete_dataHeadMeta.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.DataHeadMeta.destroy({
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

module.exports = router;