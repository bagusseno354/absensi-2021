const { mustHavePermisisons } = require('../middlewares/auth');
const router = require('express').Router();
const models = require('../db/models')
const { paramsToSequelize } = require('../lib/params-to-sequelize-2');
const permissions = require('../db/predefined/permissions');
const { QueryTypes } = require('sequelize');

router.get('/api/dataContent', 
  async(req, res, next) =>
{
  const filters = paramsToSequelize(req.query, models.DataContent)  

  let result;
    
  try
  {
    result = await models.DataContent.findAll(filters);
  } 
  catch(error)
  {
    return next(error);
  }

  if(result.length == 0)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.post('/api/dataContent', 
  async(req, res, next) =>
{
  if(!Array.isArray(req.body))
    req.body = [req.body];

  await models.DataContent.bulkCreate(req.body, {returning: true})
  .then(dataContents =>
  {
    return res.status(200).json(dataContents);
  })
  .catch(e => 
  {
    return next(e);
  });
});

router.patch('/api/dataContent/:id', 
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.DataContent.update(req.body, {
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

router.delete('/api/dataContent/:id', 
  mustHavePermisisons(permissions.delete_dataContent.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.DataContent.destroy({
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

// data import
router.post('/api/dataContent/bulk/:fkDataHead_id/:fkKelompok_id', async(req, res, next) =>
{
  const data = req.body;

  const { fkDataHead_id, fkKelompok_id } = req.params;

  let failures = [];

  // get metas
  const metas = await models.DataHeadMeta.findAll({where: {fkDataHead_id: fkDataHead_id}});

  for(let i = 0; i < data.length; i++)
  {
    const row = data[i];
    let insertedId = null;

    await models.DataContent.create({
      nama: row[0],
      fkKelompok_id: fkKelompok_id,
      fkDataHead_id: fkDataHead_id
    }, {returning: true})
    .then(inserted =>
    {
      insertedId = inserted.id;
    })
    .catch(e =>
    {
      failures.push(row.nama);
    });

    if(insertedId)
    {
      const metaValues = row.splice(1, row.length);

      for(let i = 0; i < metaValues.length; i++)
      {
        metaValues[i] = {
          value: metaValues[i],
          fkDataContent_id: insertedId,
          fkDataHeadMeta_id: metas[i].id
        }
      }
      
      await models.DataContentMetaValue.bulkCreate(metaValues)
      .catch(e =>
      {
        failures.push(`${row.nama} gagal insert meta value`);
      });
    }
  }

  if(failures.length == 0)
  {
    res.sendStatus(204);
  }
  else
  {
    res.status(500).json(failures);
  }
});

module.exports = router;