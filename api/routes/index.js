const auth = require('./methods/auth')
const dataContent = require('./dataContent')
const dataContentMetaValue = require('./dataContentMetaValue')
const dataHead = require('./dataHead')
const dataHeadMeta = require('./dataHeadMeta')
const dataHeadMetaValueOption = require('./dataHeadMetaValueOption')
const presence = require('./presence')
const presenceHead = require('./presenceHead')
const presenceHeadFilter = require('./presenceHeadFilter')
const role = require('./role')
const user_desa = require('./user_desa') 
const user_kelompok = require('./user_kelompok') 
const user = require('./user') 
const desa = require('./desa')
const kelompok = require('./kelompok')

module.exports.route = (app) =>
{
  app.use('/', auth);
  app.use('/', dataContent);
  app.use('/', dataContentMetaValue);
  app.use('/', dataHead);
  app.use('/', dataHeadMeta);
  app.use('/', dataHeadMetaValueOption);
  app.use('/', presence);
  app.use('/', presenceHead);
  app.use('/', presenceHeadFilter);
  app.use('/', role);
  app.use('/', user_desa);
  app.use('/', user_kelompok);
  app.use('/', user);
  app.use('/', user);
  app.use('/', desa);
  app.use('/', kelompok);
  app.use('*', (req, res) =>
  {
    return res.status(404).json({
      msg: 'endpoint not found'
    })
  })
}