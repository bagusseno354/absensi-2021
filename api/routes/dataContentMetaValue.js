const { mustHavePermisisons } = require('../middlewares/auth');
const router = require('express').Router();
const models = require('../db/models')
const { paramsToSequelize } = require('../lib/params-to-sequelize-2');
const permissions = require('../db/predefined/permissions');
const { QueryTypes } = require('sequelize');

router.get('/api/dataContentMetaValue', 
  async(req, res, next) =>
{
  const filters = paramsToSequelize(req.query, models.DataContentMetaValue)  

  let result;
    
  try
  {
    result = await models.DataContentMetaValue.findAll(filters);
  } 
  catch(error)
  {
    return next(error);
  }

  if(result.length == 0)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.post('/api/dataContentMetaValue', 
  async(req, res, next) =>
{
  if(!Array.isArray(req.body))
    req.body = [req.body];

  await models.DataContentMetaValue.bulkCreate(req.body, {returning: true})
  .then(dataContentMetaValues =>
  {
    return res.status(200).json(dataContentMetaValues);
  })
  .catch(e => 
  {
    return next(e);
  });
});

router.put('/api/dataContentMetaValue/:fkDataContent_id/:fkDataHeadMeta_id', 
  async(req, res, next) =>
{
  const { fkDataContent_id, fkDataHeadMeta_id } = req.params;

  req.body.fkDataContent_id = fkDataContent_id;
  req.body.fkDataHeadMeta_id = fkDataHeadMeta_id;
  
  await models.DataContentMetaValue.upsert(req.body)
  .then(count =>
  {
    return res.sendStatus(204);
  })
  .catch(e =>
  {
    return next(e);
  })
});

router.delete('/api/dataContentMetaValue/:fkDataContent_id/:fkDataHeadMeta_id', 
  mustHavePermisisons(permissions.delete_dataContentMetaValue.id),
  async(req, res, next) =>
{
  const { fkDataContent_id, fkDataHeadMeta_id } = req.params;

  await models.DataContentMetaValue.destroy({
    where: {
      fkDataContent_id,
      fkDataHeadMeta_id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

module.exports = router;