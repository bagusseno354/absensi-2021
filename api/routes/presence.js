const jwt = require('../lib/jwt');
const router = require('express').Router();
const models = require('../db/models')
const { paramsToSequelize } = require('../lib/params-to-sequelize-2');
const permissions = require('../db/predefined/permissions')
const { mustHavePermisisons } = require('../middlewares/auth')
const Op = require('sequelize').Op;
const moment = require('moment')

router.get('/api/presence', 
  async(req, res, next) =>
{
  const filters = paramsToSequelize(req.query, models.Presence)  

  let result;

  try
  {
    result = await models.Presence.findAll(filters);
  } 
  catch(error)
  {
    return next(error);
  }

  if(result.length == 0)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.get('/api/presence/:id', 
  async(req, res, next) =>
{
  const filters = paramsToSequelize(req.query, models.Presence);
  const id = req.params.id; 

  let result;

  try
  {
    result = await models.Presence.findByPk(id, filters);
  } 
  catch(error)
  {
    return next(error);
  }

  if(!result)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.post('/api/presence', 
  async(req, res, next) =>
{
  // get the presenceHead first, check whether it allows duplicates or not
  const presenceHead = await models.PresenceHead.findByPk(req.body.fkPresenceHead_id)
  
  if(!presenceHead)
    return res.status(404).json({
      msg: 'presence head not found'
    });

  // check if the time is limited
  if(presenceHead.startDatetime && presenceHead.endDatetime)
  {
    let currDate = moment();
    let endDatetime = moment(presenceHead.endDatetime);
    let startDatetime = moment(presenceHead.startDatetime);
    
    console.log(currDate);
    console.log(endDatetime);
    console.log(startDatetime);

    if(!(currDate <= endDatetime && currDate >= startDatetime))
      return res.status(403).json({
        msg: 'Waktu saat ini belum atau telah melewati batas mulai atau selesai waktu presensi!',
        errorCode: 'not_in_time'
      });
  }

  if(!presenceHead.allowDuplicate)
    await models.Presence.findOrCreate({
      where: req.body,
      defaults: req.body
    })
    .then(presence =>
    {
      return res.status(200).json(presence);
    })
    .catch(e => 
    {
      return next(e);
    });
  else
    await models.Presence.findOrCreate({
      where: {
        time: {
          [Op.gt]: (new Date().setHours(0, 0, 0, 0)),
          [Op.lt]: (new Date().setHours(23, 59, 0, 0))
        },
        ...req.body
      },
      defaults: {...req.body, time: Date()}
    })
    .then(presence =>
    {
      return res.status(200).json(presence);
    })
    .catch(e => 
    {
      return next(e);
    });
});

router.patch('/api/presence/:id', 
  mustHavePermisisons(permissions.update_presence.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.Presence.update(req.body, {
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

router.delete('/api/presence/:id', 
  mustHavePermisisons(permissions.delete_presence.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.Presence.destroy({
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

module.exports = router;