const jwt = require('../lib/jwt');
const router = require('express').Router();
const models = require('../db/models')
const { paramsToSequelize } = require('../lib/params-to-sequelize-2');
const { mustHavePermisisons } = require('../middlewares/auth');
const permissions = require('../db/predefined/permissions')

router.get('/api/user', 
  async(req, res, next) =>
{
  const filters = paramsToSequelize(req.query, models.User)  

  let result;

  try
  {
    result = await models.User.findAll({
      ...filters
    });
  } 
  catch(error)
  {
    return next(error);
  }

  if(result.length == 0)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.get('/api/user/me', 
  async(req, res) =>
{
  if(!req.user) 
    return res.sendStatus(401);
  
  const filters = paramsToSequelize(req.query, models.User)  

  const user = await models.User.findByPk(req.user.id, filters)

  if(!user)
    return res.sendStatus(404);

  return res.status(200).json(user);
})

router.get('/api/user/:id', 
  async(req, res, next) =>
{
  const filters = paramsToSequelize(req.query, models.User);
  const id = req.params.id;

  let result;

  try
  {
    result = await models.User.findByPk(id, {
      ...filters
    });
  } 
  catch(error)
  {
    return next(error);
  }

  if(!result)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.post('/api/user', 
  mustHavePermisisons(permissions.create_user.id),
  async(req, res, next) =>
{
  if(!Array.isArray(req.body))
    req.body = [req.body];

  await models.User.bulkCreate(req.body, {returning: true})
  .then(users =>
  {
    return res.status(200).json(users);
  })
  .catch(e => 
  {
    return next(e);
  });
});

router.patch('/api/user/:id', 
  mustHavePermisisons(permissions.update_user.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.User.update(req.body, {
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

router.delete('/api/user/:id', 
  mustHavePermisisons(permissions.delete_user.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.User.destroy({
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

// sub resources
router.put('/api/user/:id/desa', 
  mustHavePermisisons(permissions.update_user_desa.id),
  async (req, res, next) =>
{
  req.body.fkUser_id = req.params.id;

  await models.User_desa.upsert(req.body)
  .then(user_desa =>
  {
    return res.status(200).json(user_desa);
  })
  .catch(e => 
  {
    return next(e);
  });  
});

router.put('/api/user/:id/kelompok', 
  mustHavePermisisons(permissions.update_user_kelompok.id),
  async (req, res, next) =>
{
  req.body.fkUser_id = req.params.id;
  
  await models.User_kelompok.upsert(req.body)
  .then(user_kelompok =>
  {
    return res.status(200).json(user_kelompok);
  })
  .catch(e => 
  {
    return next(e);
  });  
});

module.exports = router;