const jwt = require('../lib/jwt');
const router = require('express').Router();
const models = require('../db/models')
const { paramsToSequelize } = require('../lib/params-to-sequelize-2');
const permissions = require('../db/predefined/permissions')
const { mustHavePermisisons } = require('../middlewares/auth')

router.get('/api/user_desa', 
  async(req, res, next) =>
{
  const filters = paramsToSequelize(req.query, models.User_desa)  

  let result;

  try
  {
    result = await models.User_desa.findAll(filters);
  } 
  catch(error)
  {
    return next(error);
  }

  if(result.length == 0)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.post('/api/user_desa', 
  mustHavePermisisons(permissions.create_user_desa.id),
  async(req, res, next) =>
{
  if(!Array.isArray(req.body))
    req.body = [req.body];

  await models.User_desa.bulkCreate(req.body, {returning: true})
  .then(user_desas =>
  {
    return res.status(200).json(user_desas);
  })
  .catch(e => 
  {
    return next(e);
  });
});

router.patch('/api/user_desa/:id',
  mustHavePermisisons(permissions.update_user_desa.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.User_desa.update(req.body, {
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

router.delete('/api/user_desa/:id', 
  mustHavePermisisons(permissions.delete_user_desa.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.User_desa.destroy({
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

module.exports = router;