const jwt = require('../lib/jwt');
const router = require('express').Router();
const models = require('../db/models')
const { paramsToSequelize } = require('../lib/params-to-sequelize-2');
const permissions = require('../db/predefined/permissions')
const { mustHavePermisisons } = require('../middlewares/auth')

router.get('/api/presenceHeadFilter', 
  async(req, res, next) =>
{
  const filters = paramsToSequelize(req.query, models.PresenceHeadFilter)  

  let result;

  try
  {
    result = await models.PresenceHeadFilter.findAll(filters);
  } 
  catch(error)
  {
    return next(error);
  }

  if(result.length == 0)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.post('/api/presenceHeadFilter', 
  mustHavePermisisons(permissions.create_presenceHeadFilter.id),
  async(req, res, next) =>
{
  if(!Array.isArray(req.body))
    req.body = [req.body];

  await models.PresenceHeadFilter.bulkCreate(req.body, {returning: true})
  .then(presenceHeadFilters =>
  {
    return res.status(200).json(presenceHeadFilters);
  })
  .catch(e => 
  {
    return next(e);
  });
});

router.patch('/api/presenceHeadFilter/:id', 
  mustHavePermisisons(permissions.update_presenceHeadFilter.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.PresenceHeadFilter.update(req.body, {
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

router.delete('/api/presenceHeadFilter/:id', 
  mustHavePermisisons(permissions.delete_presenceHeadFilter.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.PresenceHeadFilter.destroy({
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);  })
  .catch(e =>
  {
    return next(e);
  })
});

module.exports = router;