const jwt = require('../lib/jwt');
const router = require('express').Router();
const models = require('../db/models')
const { paramsToSequelize } = require('../lib/params-to-sequelize-2');
const permissions = require('../db/predefined/permissions')
const { mustHavePermisisons } = require('../middlewares/auth')

router.get('/api/desa', 
  async(req, res, next) =>
{
  const filters = paramsToSequelize(req.query, models.Desa)  

  let result;

  try
  {
    result = await models.Desa.findAll(filters);
  } 
  catch(error)
  {
    return next(error);
  }

  if(result.length == 0)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.get('/api/desa/:id', 
  async(req, res, next) =>
{
  const filters = paramsToSequelize(req.query, models.Desa)  
  const id = req.params.id;

  let result;

  try
  {
    result = await models.Desa.findByPk(id, filters);
  } 
  catch(error)
  {
    return next(error);
  }

  if(result.length == 0)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.post('/api/desa', 
  mustHavePermisisons(permissions.create_desa.id),
  async(req, res, next) =>
{
  if(!Array.isArray(req.body))
    req.body = [req.body];

  await models.Desa.bulkCreate(req.body, {returning: true})
  .then(desas =>
  {
    return res.status(200).json(desas);
  })
  .catch(e => 
  {
    return next(e);
  });
});

router.patch('/api/desa/:id',
  mustHavePermisisons(permissions.update_desa.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.Desa.update(req.body, {
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

router.delete('/api/desa/:id', 
  mustHavePermisisons(permissions.delete_desa.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.Desa.destroy({
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

module.exports = router;