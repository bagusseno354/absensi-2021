const jwt = require('../../lib/jwt');
const router = require('express').Router();
const models = require('../../db/models');
const bcrypt = require('bcryptjs');
const { validateRequiredFields } = require('../../middlewares/validate')

router.post('/api/methods/auth/signIn',
  validateRequiredFields('email', 'password'),
  async(req, res, next) =>
{
  await models.User.scope('withPassword').findOne({
    where: {
      email: req.body.email,
    },
    include: [{
      model: models.Role,
      attributes: ['id'],
    }]
  })
  .then(user =>
  {
    if(!user)
      return res.status(404).json({error: 'not found'});
      
    const isValid = bcrypt.compareSync(req.body.password, user.password)

    if(!isValid)
      return res.sendStatus(401);

    const jwtToken = jwt.generateJwtToken({
      id: user.id,
      role: user.fkRole_id ? user.fkRole_id : 'no_role'
    });

    return res.cookie('token', jwtToken, {
      secure: true,
      httpOnly: true,
      sameSite: 'none',            
    })
    .header('Access-Control-Allow-Credentials', 'true')
    .sendStatus(204)
  })
  .catch(e => 
  {
    return next(e);
  });
});

router.post('/api/methods/auth/signOut',
  async(req, res, next) =>
{
  return res.cookie('token', '', {
    httpOnly: true,
    secure: true,
    expires: new Date(Date.now() - 10000),
    sameSite: 'none',            
  })
  .header('Access-Control-Allow-Credentials', 'true')
  .sendStatus(204)
})

module.exports = router;