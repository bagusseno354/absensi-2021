const jwt = require('../lib/jwt');
const router = require('express').Router();
const models = require('../db/models')
const { paramsToSequelize } = require('../lib/params-to-sequelize-2');
const permissions = require('../db/predefined/permissions')
const { mustHavePermisisons } = require('../middlewares/auth')

router.get('/api/dataHead', 
  async(req, res, next) =>
{
  const filters = paramsToSequelize(req.query, models.DataHead)  

  let result;

  try
  {
    result = await models.DataHead.findAll(filters);
  } 
  catch(error)
  {
    return next(error);
  }

  if(result.length == 0)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.get('/api/dataHead/:id', 
  async(req, res, next) =>
{
  const filters = paramsToSequelize(req.query, models.DataHead)  
  const id = req.params.id;

  let result;

  try
  {
    result = await models.DataHead.findByPk(id, filters);
  } 
  catch(error)
  {
    return next(error);
  }

  if(!result)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.post('/api/dataHead', 
  mustHavePermisisons('create_dataHead'),
  async(req, res, next) =>
{
  req.body.fkUser_creatorId = req.user.id;

  if(!Array.isArray(req.body))
    req.body = [req.body];

  await models.DataHead.bulkCreate(req.body, {returning: true})
  .then(dataHeads =>
  {
    return res.status(200).json(dataHeads);
  })
  .catch(e => 
  {
    return next(e);
  });
});

router.patch('/api/dataHead/:id', 
  mustHavePermisisons(permissions.update_dataHead.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.DataHead.update(req.body, {
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

router.delete('/api/dataHead/:id', 
  mustHavePermisisons(permissions.delete_dataHead.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.DataHead.destroy({
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

module.exports = router;