const { mustHavePermisisons } = require('../middlewares/auth');
const router = require('express').Router();
const models = require('../db/models');
const permissions = require('../db/predefined/permissions')
const { paramsToSequelize } = require('../lib/params-to-sequelize-2');

router.get('/api/role', 
  async(req, res, next) =>
{
  const filters = paramsToSequelize(req.query, models.Role)  

  let result;

  try
  {
    result = await models.Role.findAll(filters);
  } 
  catch(error)
  {
    return next(error);
  }

  if(result.length == 0)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.post('/api/role', 
  mustHavePermisisons(permissions.create_role.id),
  async(req, res, next) =>
{
  if(!Array.isArray(req.body))
    req.body = [req.body];

  await models.Role.bulkCreate(req.body, {returning: true})
  .then(roles =>
  {
    return res.status(200).json(roles);
  })
  .catch(e => 
  {
    return next(e);
  });
});

router.delete('/api/role/:id', 
  mustHavePermisisons(permissions.delete_role.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.Role.destroy({
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

module.exports = router;