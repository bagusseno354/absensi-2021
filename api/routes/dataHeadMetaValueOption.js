const jwt = require('../lib/jwt');
const router = require('express').Router();
const models = require('../db/models')
const { paramsToSequelize } = require('../lib/params-to-sequelize-2');
const permissions = require('../db/predefined/permissions')
const { mustHavePermisisons } = require('../middlewares/auth')

router.get('/api/dataHeadMetaValueOption', 
  async(req, res, next) =>
{
  const filters = paramsToSequelize(req.query, models.DataHeadMetaValueOption)  

  let result;

  try
  {
    result = await models.DataHeadMetaValueOption.findAll(filters);
  } 
  catch(error)
  {
    return next(error);
  }

  if(result.length == 0)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.get('/api/dataHeadMetaValueOption/:id', 
  async(req, res, next) =>
{
  const filters = paramsToSequelize(req.query, models.DataHeadMetaValueOption)  
  const id = req.params.id;

  let result;

  try
  {
    result = await models.DataHeadMetaValueOption.findByPk(id, filters);
  } 
  catch(error)
  {
    return next(error);
  }

  if(!result)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.post('/api/dataHeadMetaValueOption', 
  mustHavePermisisons('create_dataHeadMetaValueOption'),
  async(req, res, next) =>
{
  req.body.fkUser_creatorId = req.user.id;

  if(!Array.isArray(req.body))
    req.body = [req.body];

  await models.DataHeadMetaValueOption.bulkCreate(req.body, {returning: true})
  .then(dataHeadMetaValueOptions =>
  {
    return res.status(200).json(dataHeadMetaValueOptions);
  })
  .catch(e => 
  {
    return next(e);
  });
});

router.patch('/api/dataHeadMetaValueOption/:id', 
  mustHavePermisisons(permissions.update_dataHeadMetaValueOption.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.DataHeadMetaValueOption.update(req.body, {
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

router.delete('/api/dataHeadMetaValueOption/:id', 
  mustHavePermisisons(permissions.delete_dataHeadMetaValueOption.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.DataHeadMetaValueOption.destroy({
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

module.exports = router;