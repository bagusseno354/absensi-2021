const jwt = require('../lib/jwt');
const router = require('express').Router();
const models = require('../db/models')
const { paramsToSequelize } = require('../lib/params-to-sequelize-2');
const permissions = require('../db/predefined/permissions')
const { mustHavePermisisons } = require('../middlewares/auth')

router.get('/api/kelompok', 
  async(req, res, next) =>
{
  const filters = paramsToSequelize(req.query, models.Kelompok); 

  let result;

  try
  {
    result = await models.Kelompok.findAll(filters);
  } 
  catch(error)
  {
    return next(error);
  }

  if(result.length == 0)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.get('/api/kelompok/:id', 
  async(req, res, next) =>
{
  const filters = paramsToSequelize(req.query, models.Kelompok);
  const id = req.params.id;
  let result;

  try
  {
    result = await models.Kelompok.findByPk(id, filters);
  } 
  catch(error)
  {
    return next(error);
  }

  if(result.length == 0)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.post('/api/kelompok', 
  mustHavePermisisons(permissions.create_kelompok.id),
  async(req, res, next) =>
{
  if(!Array.isArray(req.body))
    req.body = [req.body];

  await models.Kelompok.bulkCreate(req.body, {returning: true})
  .then(kelompoks =>
  {
    return res.status(200).json(kelompoks);
  })
  .catch(e => 
  {
    return next(e);
  });
});

router.patch('/api/kelompok/:id',
  mustHavePermisisons(permissions.update_kelompok.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.Kelompok.update(req.body, {
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

router.delete('/api/kelompok/:id', 
  mustHavePermisisons(permissions.delete_kelompok.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.Kelompok.destroy({
    where: {
      id: id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

module.exports = router;