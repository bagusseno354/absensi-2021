const jwt = require('../lib/jwt');
const router = require('express').Router();
const models = require('../db/models')
const { paramsToSequelize } = require('../lib/params-to-sequelize-2');
const permissions = require('../db/predefined/permissions')
const { mustHavePermisisons } = require('../middlewares/auth')

router.get('/api/presenceHead', 
  async(req, res, next) =>
{
  const filters = paramsToSequelize(req.query, models.PresenceHead)  

  let result;

  try
  {
    result = await models.PresenceHead.findAll(filters);
  } 
  catch(error)
  {
    return next(error);
  }

  if(result.length == 0)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.get('/api/presenceHead/:id', 
  async(req, res, next) =>
{
  const id = req.params.id;

  let result;

  try
  {
    result = await models.PresenceHead.findByPk(id);
  } 
  catch(error)
  {
    return next(error);
  }

  if(!result)
    return res.sendStatus(404);

  return res.status(200).send(result);
});

router.post('/api/presenceHead', 
  mustHavePermisisons(permissions.create_presenceHead.id),
  async(req, res, next) =>
{  
  if(!Array.isArray(req.body))
    req.body = [req.body];

  req.body = req.body.map(body => ({...body, fkUser_id: req.user.id}));

  await models.PresenceHead.bulkCreate(req.body, {returning: true})
  .then(presenceHeads =>
  {
    return res.status(200).json(presenceHeads);
  })
  .catch(e => 
  {
    return next(e);
  });
});

router.patch('/api/presenceHead/:id',
  mustHavePermisisons(permissions.update_presenceHead.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.PresenceHead.update(req.body, {
    where: {
      id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

router.delete('/api/presenceHead/:id', 
  mustHavePermisisons(permissions.delete_presenceHead.id),
  async(req, res, next) =>
{
  const id = req.params.id;

  await models.PresenceHead.destroy({
    where: {
      id
    }
  })
  .then(count =>
  {
    if(count > 0)
      return res.sendStatus(204);
    else
      return res.sendStatus(404);
  })
  .catch(e =>
  {
    return next(e);
  })
});

module.exports = router;