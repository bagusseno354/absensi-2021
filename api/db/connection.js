const { Sequelize } = require('sequelize') 
require('dotenv').config();  

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
  dialect: 'mysql',
  dialectOptions: {
    useUTC: false
  },
  sync: {
    alter: true
  }
})

try
{
  sequelize.authenticate();
  console.log('Connected to database.');
}
catch(e)
{
  console.error('Cannot connect to database: ', e);
}

module.exports = sequelize