'use strict';

const {
  Model, DataTypes, Sequelize
} = require('sequelize');

const connection = require('../connection')
const queryInterface = connection.getQueryInterface()
const predefined = require('../predefined')
const models = require('../models')

// const queryInterface = new QueryInterface()

module.exports = {
  up: async () => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */    

     await queryInterface.createTable('Role', {
      id: {
        primaryKey: true,
        type: DataTypes.STRING,
        allowNull: false
      },
      name: { 
        type: DataTypes.STRING(100),
        allowNull: false 
      },
      description: {
        type: DataTypes.STRING(200),
        allowNull: false
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
    });  

    await queryInterface.createTable('User', {
      id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
      },
      fkRole_id: {
        type: DataTypes.STRING,
        allowNull: false,
        references: {
          model: 'Role',
          key: 'id',
        },
        onDelete: 'RESTRICT'
      },
      name: { 
        type: DataTypes.STRING(100),
        allowNull: false,
      },      
      email: {
        type: DataTypes.STRING(100),
        allowNull: false,
        validate: {
          isEmail: true
        },
        unique: true
      },
      password: {
        type: DataTypes.STRING(100),
        allowNull: false,    
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,        
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
    });
    
    await queryInterface.createTable('Permission', {
      id: { 
        primaryKey: true,
        type: DataTypes.STRING(100),
        allowNull: false 
      },
      description: {
        type: DataTypes.STRING(200),
        allowNull: false
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,        
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
    });

    await queryInterface.createTable('Desa', {
      id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false
      },
      name: { 
        type: DataTypes.STRING(100),
        allowNull: false 
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,        
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
    });

    await queryInterface.createTable('Kelompok', {
      id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false
      },
      fkDesa_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'Desa',
          key: 'id',                             
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      name: { 
        type: DataTypes.STRING(100),
        allowNull: false 
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,        
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
    }); 

    await queryInterface.createTable('DataHead', {
      id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false
      },
      name: { 
        type: DataTypes.STRING(100),
        allowNull: false 
      },
      fkUser_creatorId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'User',
          key: 'id',                             
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,        
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
    });

    await queryInterface.createTable('DataContent', {
      id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false
      },
      fkDataHead_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'DataHead',
          key: 'id',                             
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      nama: { 
        type: DataTypes.STRING(100),
        allowNull: false 
      },
      jenisKelamin: {
        type: DataTypes.CHAR(1),
        allowNull: false
      },
      tglLahir: { 
        type: DataTypes.INTEGER(2),
        allowNull: false 
      },
      blnLahir: { 
        type: DataTypes.INTEGER(2),
        allowNull: false 
      },
      thnLahir: { 
        type: DataTypes.INTEGER(4),
        allowNull: false 
      },
      fkKelompok_id: { 
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'Kelompok',
          key: 'id',                             
        },
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT'
      },
      golDarah: { 
        type: DataTypes.CHAR(2),
        allowNull: false 
      },
      noTelp: { 
        type: DataTypes.STRING(15),
        allowNull: false 
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,        
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      }
    });    

    await queryInterface.createTable('PresenceHead', {
      id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false
      },
      name: { 
        type: DataTypes.STRING(100),
        allowNull: false 
      },
      fkDataHead_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'DataHead',
          key: 'id',                             
        },
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT'
      },
      allowDuplicate: {
        type: DataTypes.INTEGER(1),
        allowNull: false,
        defaultValue: 0
      },
      startDatetime: {
        type: DataTypes.DATE,
        allowNull: true
      },
      endDatetime: {
        type: DataTypes.DATE,
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,        
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
    });    

    await queryInterface.createTable('PresenceHeadFilter', {
      id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false
      },
      fkPresenceHead_id: { 
        type: DataTypes.INTEGER,
        allowNull: false ,
        references: {
          model: 'PresenceHead',
          key: 'id',                             
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      columnName: {
        type: DataTypes.STRING(100),
        allowNull: false
      },
      operator: {
        type: DataTypes.STRING(2),
        allowNull: false
      },
      columnValue: {
        type: DataTypes.STRING(100),
        allowNull: false
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,        
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
    });

    await queryInterface.createTable('Presence', {
      id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true
      },
      fkPresenceHead_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'PresenceHead',
          key: 'id',                             
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      fkDataContent_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'DataContent',
          key: 'id',                             
        },
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT'
      },
      status: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: 'hadir'
      },
      time: {
        type: Sequelize.DATE(),
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP()'),
      },
    });
      
    await queryInterface.createTable('Role_permission', {
      fkRole_id: {
        primaryKey: true,
        type: DataTypes.STRING,
        allowNull: false,
        references: {
          model: 'Role',
          key: 'id',                             
        },
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT'
      },
      fkPermission_id: {
        primaryKey: true,
        type: DataTypes.STRING,
        allowNull: false,
        references: {
          model: 'Permission',
          key: 'id',                             
        },
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT'
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,        
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
    });    

    await queryInterface.createTable('User_desa', {
      fkUser_id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'User',
          key: 'id',                             
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      fkDesa_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'Desa',
          key: 'id',                             
        },
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT'
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,        
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
    });
    
    await queryInterface.createTable('User_kelompok', {
      fkUser_id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'User',
          key: 'id',                             
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      fkKelompok_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'Kelompok',
          key: 'id',                             
        },
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT'
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,        
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
    });

    await predefined(models);
  },

  down: async () => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.dropTable('Role_permission');
    await queryInterface.dropTable('Presence');
    await queryInterface.dropTable('PresenceHeadFilter');
    await queryInterface.dropTable('PresenceHead');
    await queryInterface.dropTable('DataContent');
    await queryInterface.dropTable('DataHead');
    await queryInterface.dropTable('User_desa');
    await queryInterface.dropTable('User_kelompok');
    await queryInterface.dropTable('Kelompok');
    await queryInterface.dropTable('Desa');
    await queryInterface.dropTable('Permission');
    await queryInterface.dropTable('User');    
    await queryInterface.dropTable('Role');
  }
}
