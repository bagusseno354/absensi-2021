'use strict';

const {
  Model, DataTypes, Sequelize
} = require('sequelize');

const connection = require('../connection')
const queryInterface = connection.getQueryInterface()
const predefined = require('../predefined')
const models = require('../models')

module.exports = {
  up: async () => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addColumn('PresenceHead', 'fkUser_id', {
      type: DataTypes.INTEGER,
      references: {
        model: 'User',
        key: 'id'
      },
      allowNull: false,
      onUpdate: 'CASCADE',
      onDelete: 'RESTRICT'
    });
  },

  down: async () => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn('PresenceHead', 'fkUser_id');
  }
};
