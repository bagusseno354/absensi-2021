'use strict';

const {
  Model, DataTypes, Sequelize
} = require('sequelize');

const connection = require('../connection')
const queryInterface = connection.getQueryInterface()
const predefined = require('../predefined')
const models = require('../models')

module.exports = {
  up: async () => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.changeColumn('Presence', 'time', {
      type: Sequelize.DATE(),
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP()'),
      allowNull: false
    })
  },

  down: async () => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
     await queryInterface.changeColumn('Presence', 'time', {
      type: Sequelize.DATE(),
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP()'),
      allowNull: true
    })
  }
};
