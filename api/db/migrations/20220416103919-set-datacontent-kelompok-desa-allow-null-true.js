'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.changeColumn('DataContent', 'fkKelompok_id', {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'Kelompok',
        key: 'id',                             
      },
      onUpdate: 'CASCADE',
      onDelete: 'RESTRICT'
    })
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.changeColumn('DataContent', 'fkKelompok_id', {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Kelompok',
        key: 'id',                             
      },
      onUpdate: 'CASCADE',
      onDelete: 'RESTRICT'
    })
  }
};
