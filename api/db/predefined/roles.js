const roles = {
    superadmin: {
        id: 'superadmin',
        name: 'Super Admin',
        description: 'Total control'
    },
    pengurusDaerah: {
        id: 'pengurusDaerah',
        name: 'Pengurus Daerah',
        description: 'Melihat semua data, tapi tidak dapat membuat user'
    },
    pengurusDesa: {
        id: 'pengurusDesa',
        name: 'Pengurus Desa',
        description: 'Melihat data desa tertentu saja'
    },
    pengurusKelompok: {
        id: 'pengurusKelompok',
        name: 'Pengurus Kelompok',
        description: 'Melihat data kelompok tertentu saja'
    }
}

module.exports = roles;