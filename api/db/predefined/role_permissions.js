const permissions = require('./permissions')
const role_permissions = []

Object.keys(permissions).forEach(permissionId =>
{
    role_permissions.push({
        fkRole_id: 'superadmin',
        fkPermission_id: permissionId
    });

    role_permissions.push({
        fkRole_id: 'pengurusKelompok',
        fkPermission_id: permissionId
    });
    
    role_permissions.push({
        fkRole_id: 'pengurusDesa',
        fkPermission_id: permissionId
    });

    role_permissions.push({
        fkRole_id: 'pengurusDaerah',
        fkPermission_id: permissionId
    });    
})

module.exports = role_permissions;