const kelompoks = {
    baleendah: [
        {
            name: 'Kertamanah',
            fkDesa_id: 1
        },
        {
            name: 'Manggahang',
            fkDesa_id: 1
        },
        {
            name: 'Ciptim',
            fkDesa_id: 1
        },
        {
            name: 'Cipbar',
            fkDesa_id: 1
        },
        {
            name: 'Munjul',
            fkDesa_id: 1
        },
        {
            name: 'Palasari',
            fkDesa_id: 1
        },
        {
            name: 'Dayeuhkolot',
            fkDesa_id: 1
        },
    ],    
    banjaran: [
        {
            name: 'Nambo',
            fkDesa_id: 2
        },
        {
            name: 'Cimaung',
            fkDesa_id: 2
        },
        {
            name: 'Pangalengan',
            fkDesa_id: 2
        },
        {
            name: 'Cijulang',
            fkDesa_id: 2
        },
    ],
    ciparay: [
        {
            name: 'Cipaku',
            fkDesa_id: 3
        },
        {
            name: 'Cidawolong',
            fkDesa_id: 3
        },
        {
            name: 'Baranangsiang',
            fkDesa_id: 3
        },
        {
            name: 'Barujati',
            fkDesa_id: 3
        },
    ],
    soreang: [
        {
            name: 'Soreang 1',
            fkDesa_id: 4
        },
        {
            name: 'Soreang 2',
            fkDesa_id: 4
        },
        {
            name: 'Warunglobak 1',
            fkDesa_id: 4
        },
        {
            name: 'Warunglobak 2',
            fkDesa_id: 4
        },
        {
            name: 'Junti',
            fkDesa_id: 4
        },
        {
            name: 'Ciwidey',
            fkDesa_id: 4
        },
    ],
    majalaya: [
        {
            name: 'Pongporang',
            fkDesa_id: 5
        },
        {
            name: 'Muara',
            fkDesa_id: 5
        },
        {
            name: 'Sukma 1',
            fkDesa_id: 5
        },
        {
            name: 'Sukma 2',
            fkDesa_id: 5
        },
        {
            name: 'Sukma 3',
            fkDesa_id: 5
        },
        {
            name: 'Haurbuyut',
            fkDesa_id: 5
        },
        {
            name: 'Bojong',
            fkDesa_id: 5
        },
    ],
    sayati: [
        {
            name: 'Burujul',
            fkDesa_id: 6
        },
        {
            name: 'Margahayu Permai',
            fkDesa_id: 6
        },
        {
            name: 'Margahayu Kencana',
            fkDesa_id: 6
        },
        {
            name: 'Pertama Kopo',
            fkDesa_id: 6
        },
        {
            name: 'Taman Kopo Indah',
            fkDesa_id: 6
        },
        {
            name: 'Kopo Permai',
            fkDesa_id: 6
        },
        {
            name: 'Cibaduyut',
            fkDesa_id: 6
        },
    ],
    ppm: [
        {
            name: 'PPMRJ',
            fkDesa_id: 7
        },
        {
            name: 'KPMRJ',
            fkDesa_id: 7
        },
    ],
    pondokMini: [
        {
            name: 'Baitul Amal',
            fkDesa_id: 8
        },
    ],    
}

// strict, this allows the system to allow to input data content with unknwon kelompok and desa
kelompoks['unset'] = [{
    id: -1,
    name: 'Unset',
    fkDesa_id: -1
}],

module.exports = kelompoks;