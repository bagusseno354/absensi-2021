const permissions = {
  view_all_data: {
    id: 'view_all_data',
    description: 'no desc yet'
  },
  create_presenceHead: {
    id: 'create_presenceHead',
    description: 'no desc yet'
  },
  delete_presenceHead: {
    id: 'delete_presenceHead',
    description: 'no desc yet'
  },
  update_presenceHead: {
    id: 'update_presenceHead',
    description: 'no desc yet'
  },
  create_dataHead: {
    id: 'create_dataHead',
    description: 'no desc yet'
  },
  delete_dataHead: {
    id: 'delete_dataHead',
    description: 'no desc yet'
  },
  update_dataHead: {
    id: 'update_dataHead',
    description: 'no desc yet'
  },
  create_dataContent: {
    id: 'create_dataContent',
    description: 'no desc yet'
  },
  delete_dataContent: {
    id: 'delete_dataContent',
    description: 'no desc yet'
  },
  update_dataContent: {
    id: 'update_dataContent',
    description: 'no desc yet'
  },
  create_presence: {
    id: 'create_presence',
    description: 'no desc yet'
  },
  delete_presence: {
    id: 'delete_presence',
    description: 'no desc yet'
  },
  update_presence: {
    id: 'update_presence',
    description: 'no desc yet'
  },
  create_presenceHeadFilter: {
    id: 'create_presenceHeadFilter',
    description: 'no desc yet'
  },
  delete_presenceHeadFilter: {
    id: 'delete_presenceHeadFilter',
    description: 'no desc yet'
  },
  update_presenceHeadFilter: {
    id: 'update_presenceHeadFilter',
    description: 'no desc yet'
  },  
  create_role: {
    id: 'create_role',
    description: 'no desc yet'
  },
  delete_role: {
    id: 'delete_role',
    description: 'no desc yet'
  },
  update_role: {
    id: 'update_role',
    description: 'no desc yet'
  },
  create_user: {
    id: 'create_user',
    description: 'no desc yet'
  },
  delete_user: {
    id: 'delete_user',
    description: 'no desc yet'
  },
  update_user: {
    id: 'update_user',
    description: 'no desc yet'
  },
  create_desa: {
    id: 'create_desa',
    description: 'no desc yet'
  },
  delete_desa: {
    id: 'delete_desa',
    description: 'no desc yet'
  },
  update_desa: {
    id: 'update_desa',
    description: 'no desc yet'
  },
  create_kelompok: {
    id: 'create_kelompok',
    description: 'no desc yet'
  },
  delete_kelompok: {
    id: 'delete_kelompok',
    description: 'no desc yet'
  },
  update_kelompok: {
    id: 'update_kelompok',
    description: 'no desc yet'
  },
  create_user_kelompok: {
    id: 'create_user_kelompok',
    description: 'no desc yet'
  },
  delete_user_kelompok: {
    id: 'delete_user_kelompok',
    description: 'no desc yet'
  },
  update_user_kelompok: {
    id: 'update_user_kelompok',
    description: 'no desc yet'
  },
  create_user_desa: {
    id: 'create_user_desa',
    description: 'no desc yet'
  },
  delete_user_desa: {
    id: 'delete_user_desa',
    description: 'no desc yet'
  },
  update_user_desa: {
    id: 'update_user_desa',
    description: 'no desc yet'
  },
  create_dataHeadMeta: {
    id: 'create_dataHeadMeta',
    description: 'no desc yet'
  },
  delete_dataHeadMeta: {
    id: 'delete_dataHeadMeta',
    description: 'no desc yet'
  },
  update_dataHeadMeta: {
    id: 'update_dataHeadMeta',
    description: 'no desc yet'
  },
  create_dataContentMetaValue: {
    id: 'create_dataContentMetaValue',
    description: 'no desc yet'
  },
  delete_dataContentMetaValue: {
    id: 'delete_dataContentMetaValue',
    description: 'no desc yet'
  },
  update_dataContentMetaValue: {
    id: 'update_dataContentMetaValue',
    description: 'no desc yet'
  },
  create_dataHeadMetaValueOption: {
    id: 'create_dataHeadMetaValueOption',
    description: 'no desc yet'
  },
  delete_dataHeadMetaValueOption: {
    id: 'delete_dataHeadMetaValueOption',
    description: 'no desc yet'
  },
  update_dataHeadMetaValueOption: {
    id: 'update_dataHeadMetaValueOption',
    description: 'no desc yet'
  },
}

module.exports = permissions;