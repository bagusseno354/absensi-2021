const desas = {
    baleendah: {
        id: 1,
        name: 'Baleendah',
    },
    banjaran: {
        id: 2,
        name: 'Banjaran',
    },
    ciparay: {
        id: 3,
        name: 'Ciparay',
    },
    soreang: {
        id: 4,
        name: 'Soreang',
    },
    majalaya: {
        id: 5,
        name: 'Majalaya',
    },
    sayati: {
        id: 6,
        name: 'Sayati',
    },
    ppm: {
        id: 7,
        name: 'PPM',
    },
    pondokMini: {
        id: 8,
        name: 'Pondok Mini',
    },
}

// strict, this allows the system to allow to input data content with unknwon kelompok and desa
desas['unset'] = {
    id: -1,
    name: 'Unset'
}

module.exports = desas;