'use strict'

const permissions = require('./permissions');
const role_permissions = require('./role_permissions');
const roles = require('./roles');
const users = require('./users');
const desas = require('./desas')
const kelompoks = require('./kelompoks')

/*
    Here how the predefined data will be executed
*/

module.exports = async (models) =>
{   
    // desas
    let desaKeys = Object.keys(desas)

    for(let i = 0; i < desaKeys.length; i++)
    {
        await models.Desa.findOrCreate({
            where: desas[desaKeys[i]],
            defaults: desas[desaKeys[i]]
        })
    }
    
    Object.values(kelompoks).forEach(desa =>
    {
        desa.forEach(kelompok =>
        {
            models.Kelompok.findOrCreate({
                where: kelompok,
                defaults: kelompok
            })
        })
    })

    // roles
    let roleKeys = Object.keys(roles)

    for(let i = 0; i < roleKeys.length; i++)
    {
        await models.Role.findOrCreate({
            where: roles[roleKeys[i]],
            defaults: roles[roleKeys[i]]
        })
    }

    // users
    let superadminId = null;
    let userKeys = Object.keys(users)

    for(let i = 0; i < userKeys.length; i++)
    {
        const userCandidateKey = userKeys[i];
        const userCandidate = users[userCandidateKey];
        const {password, ...where} = userCandidate
    
        const [user, created] = await models.User.findOrCreate({
            where,
            defaults: userCandidate
        })

        if(userCandidateKey == 'superadmin')
            superadminId = user.id;
    }
    
    // permissions
    let permissionKeys = Object.keys(permissions)

    for(let i = 0; i < permissionKeys.length; i++)
    {
        await models.Permission.findOrCreate({
            where: permissions[permissionKeys[i]],
            defaults: permissions[permissionKeys[i]]
        })
    }

    console.log(role_permissions);

    // role_permissions
    let rolePermissionKeys = Object.keys(role_permissions)

    for(let i = 0; i < rolePermissionKeys.length; i++)
    {
        await models.Role_permission.findOrCreate({
            where: role_permissions[i],
            defaults: role_permissions[i]
        })
    }
}