require('dotenv').config();

const users = {
    superadmin: {
        name: 'superadmin',
        email: process.env.SUPERADMIN_EMAIL,
        password: 'test',
        fkRole_id: 'superadmin'
    }
}

module.exports = users;