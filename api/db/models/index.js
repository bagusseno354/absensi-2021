require('dotenv').config();

const DataContent = require('./DataContent')
const DataHead = require('./DataHead')
const DataContentMetaValue = require('./DataContentMetaValue')
const DataHeadMeta = require('./DataHeadMeta')
const DataHeadMetaValueOption = require('./DataHeadMetaValueOption')
const Kelompok = require('./Kelompok')
const Desa = require('./Desa')
const Permission = require('./Permission')
const Presence = require('./Presence')
const PresenceHeadFilter = require('./PresenceHeadFilter')
const PresenceHead = require('./PresenceHead')
const Role_permission = require('./Role_permission')
const Role = require('./Role')
const User_desa = require('./User_desa')
const User_kelompok = require('./User_kelompok')
const User = require('./User')
const bcrypt = require('bcryptjs')
const predefined = require('../predefined')
const sequelize = require('../connection')
const { Sequelize } = require('sequelize')
const predefine = process.env.PREDEFINE_DATA || false;

const db = {
  DataContent, 
  DataHead,
  DataHeadMeta,
  Permission,
  Presence,
  PresenceHeadFilter,
  PresenceHead,
  Role_permission,
  Role, 
  User,
  Kelompok,
  Desa,
  User_desa,
  User_kelompok,
  sequelize,
  Sequelize,
  DataHeadMetaValueOption,
  DataContentMetaValue,
}

async function initDb()
{
  await sequelize.sync({alter: true});

    // init predefined data
  if(predefine != 'false')
    predefined(db); 
};

initDb();

/*
  This section is used as the gate to the db.
  You can define any business logic initializations here.
  Don't do any business logic inside the db directly.
*/

Object.keys(db).forEach(model => 
{
  if(db[model].associate) 
    db[model].associate(db)
})

// init auto hash for user password
User.beforeCreate(user =>
{
  const salt = bcrypt.genSaltSync();
  user.password = bcrypt.hashSync(user.password, salt);
});

User.beforeBulkCreate((users, where) => 
{
  for (const user of users) {
    const salt = bcrypt.genSaltSync();
    user.password = bcrypt.hashSync(user.password, salt);
  }  
});

User.beforeBulkUpdate(({attributes, where}) => 
{
  if(attributes.password)
  {
    const salt = bcrypt.genSaltSync();
    attributes.password = bcrypt.hashSync(attributes.password, salt);
  }
});

User.beforeUpdate(user => 
{
  const salt = bcrypt.genSaltSync();
  user.password = bcrypt.hashSync(user.password, salt);
});

module.exports = db;