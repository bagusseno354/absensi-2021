const sequelize = require('../connection')

'use strict';
const {
  Model, DataTypes
} = require('sequelize');

class User_kelompok extends Model {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here    
    User_kelompok.belongsTo(models.User, {
      foreignKey: 'fkUser_id',
      onDelete: 'CASCADE'
    });

    User_kelompok.belongsTo(models.Kelompok, {
      foreignKey: 'fkKelompok_id',
      onDelete: 'RESTRICT'
    })
  }
};

User_kelompok.init({
  fkUser_id: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    allowNull: false,
    references: {
      model: 'User',
      key: 'id',                             
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE'
  },
  fkKelompok_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    references: {
      model: 'Kelompok',
      key: 'id',                             
    },
    onUpdate: 'CASCADE',
    onDelete: 'RESTRICT'
  },
}, {
  sequelize,
  modelName: 'User_kelompok',
  freezeTableName: true,
  createdAt: true,
  updatedAt: true,
  timestamps: true,
});

module.exports = User_kelompok;
  