const sequelize = require('../connection')

'use strict';
const {
  Model, DataTypes, Sequelize
} = require('sequelize');

class Presence extends Model {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here  
      Presence.belongsTo(models.DataContent, {
        foreignKey: 'fkDataContent_id',  
        onDelete: 'CASCADE'     
      })

      Presence.belongsTo(models.PresenceHead, {
        foreignKey: 'fkPresenceHead_id',  
        onDelete: 'CASCADE'     
      })
  }
};
Presence.init({
  id: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    allowNull: false,
    autoIncrement: true
  },
  fkPresenceHead_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    onDelete: 'CASCADE'
  },
  fkDataContent_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    onDelete: 'CASCADE'
  },
  status: {
    type: DataTypes.STRING,
    allowNull: false,
    defaultValue: 'hadir'
  },
  reason: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  time: {
    type: Sequelize.DATE(),
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP()'),
  },
}, {
  sequelize,
  modelName: 'Presence',
  freezeTableName: true,
  createdAt: false,
  updatedAt: false
});

module.exports = Presence;
  