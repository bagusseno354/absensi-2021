'use strict';
const sequelize = require('../connection')
const DataHead = require('./DataHead')

const {
  Model, DataTypes, Sequelize
} = require('sequelize');

class DataContent extends Model {

  static associate(models)
  {
    DataContent.belongsToMany(models.PresenceHead, {
      through: models.Presence,
      foreignKey: 'fkDataContent_id',
      onDelete: 'CASCADE'
    });

    DataContent.hasMany(models.Presence, {
      foreignKey: 'fkDataContent_id',
      onDelete: 'CASCADE'
    });

    DataContent.hasMany(models.DataContentMetaValue, {
      foreignKey: 'fkDataContent_id',
      onDelete: 'CASCADE'
    });

    DataContent.belongsTo(models.DataHead, {
      foreignKey: 'fkDataHead_id',
      onDelete: 'CASCADE'
    });

    DataContent.belongsTo(models.Kelompok, {
      foreignKey: 'fkKelompok_id'
    });
  }
}

DataContent.init({
  id: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false
  },
  fkDataHead_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    onDelete: 'CASCADE'
  },
  fkKelompok_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    onDelete: 'CASCADE'
  },
  nama: { 
    type: DataTypes.STRING(100),
    allowNull: false 
  },
}, {  
  sequelize,
  modelName: 'DataContent',
  freezeTableName: true,
  createdAt: true,
  updatedAt: true,
  timestamps: true
});

module.exports = DataContent;
  