'use strict';

const sequelize = require('../connection')

const {
  Model, DataTypes
} = require('sequelize');

class Permission extends Model {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
    Permission.hasMany(models.Role_permission, {
      foreignKey: 'fkPermission_id',
      onDelete: 'CASCADE'
    });
  }
};
Permission.init({  
  id: { 
    primaryKey: true,
    type: DataTypes.STRING(100),
    allowNull: false 
  },
  description: {
    type: DataTypes.STRING(200),
    allowNull: false
  },
}, {
  sequelize,
  modelName: 'Permission',
  freezeTableName: true,
  createdAt: true,
  updatedAt: true,
  timestamps: true
});

module.exports = Permission;
  