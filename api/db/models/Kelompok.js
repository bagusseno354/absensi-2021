'use strict';

const sequelize = require('../connection')

const {
  Model, DataTypes
} = require('sequelize');

class Kelompok extends Model {

  static associate(models)
  {
    Kelompok.belongsTo(models.Desa, {
      foreignKey: 'fkDesa_id',
      onDelete: 'RESTRICT'
    });

    Kelompok.hasOne(models.User_kelompok, {
      foreignKey: 'fkKelompok_id',
      onDelete: 'CASCADE'
    })
  }
}

Kelompok.init({
  id: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false
  },
  fkDesa_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    onDelete: 'CASCADE'
  },
  name: { 
    type: DataTypes.STRING(100),
    allowNull: false 
  },
}, {
  sequelize,
  modelName: 'Kelompok',
  freezeTableName: true,
  createdAt: true,
  updatedAt: true,
  timestamps: true
})

module.exports = Kelompok
  