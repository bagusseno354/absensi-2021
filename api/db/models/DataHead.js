'use strict';

const sequelize = require('../connection')

const {
  Model, DataTypes, Sequelize
} = require('sequelize');

class DataHead extends Model {

  static associate(models)
  {
    DataHead.hasMany(models.DataContent, {
      foreignKey: 'fkDataHead_id',
      onDelete: 'CASCADE'
    });

    DataHead.hasMany(models.DataHeadMeta, {
      foreignKey: 'fkDataHead_id',
      onDelete: 'CASCADE'
    })
  }
}

DataHead.init({
  id: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false
  },
  name: { 
    type: DataTypes.STRING(100),
    allowNull: false 
  },
  fkUser_creatorId: {
    type: DataTypes.INTEGER,
    allowNull: false,
    onDelete: 'CASCADE'
  },
}, {
  sequelize,
  modelName: 'DataHead',
  freezeTableName: true,
  createdAt: true,
  updatedAt: true,
  timestamps: true
})

module.exports = DataHead
  