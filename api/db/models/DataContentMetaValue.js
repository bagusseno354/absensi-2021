'use strict';
const sequelize = require('../connection')

const {
  Model, DataTypes, Sequelize
} = require('sequelize');

class DataContentMetaValue extends Model {

  static associate(models)
  {
    DataContentMetaValue.belongsTo(models.DataContent, {
      foreignKey: 'fkDataContent_id',
      onDelete: 'CASCADE'
    })
    
    DataContentMetaValue.belongsTo(models.DataHeadMeta, {
      foreignKey: 'fkDataHeadMeta_id',
      onDelete: 'CASCADE',      
    })
  }
}

DataContentMetaValue.init({
  fkDataHeadMeta_id: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  fkDataContent_id: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    allowNull: false
  },
  value: { 
    type: DataTypes.STRING(100),
    allowNull: false 
  },
}, {  
  sequelize,
  modelName: 'DataContentMetaValue',
  freezeTableName: true,
  createdAt: true,
  updatedAt: true,
  timestamps: true
});

module.exports = DataContentMetaValue;
  