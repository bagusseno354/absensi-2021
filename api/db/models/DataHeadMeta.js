'use strict';
const sequelize = require('../connection')
const DataHead = require('./DataHead')

const {
  Model, DataTypes, Sequelize
} = require('sequelize');

class DataHeadMeta extends Model {

  static associate(models)
  {
    DataHeadMeta.hasMany(models.DataContentMetaValue, {
      foreignKey: 'fkDataHeadMeta_id',
      onDelete: 'CASCADE'
    })
    
    DataHeadMeta.belongsTo(models.DataHead, {
      foreignKey: 'fkDataHead_id',
      onDelete: 'CASCADE',
    })

    DataHeadMeta.hasMany(models.DataHeadMetaValueOption, {
      foreignKey: 'fkDataHeadMeta_id',
      onDelete: 'CASCADE'
    })
    
  }
}

DataHeadMeta.init({
  id: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false
  },
  fkDataHead_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  name: { 
    type: DataTypes.STRING(100),
    allowNull: false 
  },
  type: {
    type: DataTypes.STRING(100),
    allowNull: false
  }
}, {  
  sequelize,
  modelName: 'DataHeadMeta',
  freezeTableName: true,
  createdAt: true,
  updatedAt: true,
  timestamps: true
});

module.exports = DataHeadMeta;
  