const sequelize = require('../connection')

'use strict';
const {
  Model, DataTypes
} = require('sequelize');

class User extends Model {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here    
    User.belongsTo(models.Role, {
      foreignKey: 'fkRole_id',
      onDelete: 'RESTRICT'
    });

    User.hasMany(models.DataHead, {
      foreignKey: 'fkUser_creatorId',
      onDelete: 'RESTRICT'
    });

    User.hasOne(models.User_desa, {
      foreignKey: 'fkUser_id',
      onDelete: 'CASCADE'
    });

    User.hasOne(models.User_kelompok, {
      foreignKey: 'fkUser_id',
      onDelete: 'CASCADE'
    });

    User.hasMany(models.PresenceHead, {
      foreignKey: 'fkUser_id',
      onDelete: 'RESTRICT'
    })
  }
};

User.init({
  id: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false,
  },
  fkRole_id: {
    type: DataTypes.STRING,
    allowNull: false,
    onDelete: 'RESTRICT'
  },
  name: { 
    type: DataTypes.STRING(100),
    allowNull: false,
  },
  email: {
    type: DataTypes.STRING(100),
    allowNull: false,
    validate: {
      isEmail: true
    },
    unique: true
  },
  password: {
    type: DataTypes.STRING(100),
    allowNull: false,  
  },
}, {
  sequelize,
  modelName: 'User',
  freezeTableName: true,
  createdAt: true,
  updatedAt: true,
  timestamps: true,
  defaultScope: {
    attributes: {
      exclude: ['password']
    }
  },
  scopes: {
    withPassword: {
      attributes: {}
    },
  }
});

module.exports = User;
  