const sequelize = require('../connection')

'use strict';
const {
  Model, DataTypes, Sequelize
} = require('sequelize');

class Role_permission extends Model {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
    Role_permission.belongsTo(models.Role, {
      foreignKey: 'fkRole_id',
      onDelete: 'CASCADE'
    });

    Role_permission.belongsTo(models.Permission, {
      foreignKey: 'fkPermission_id',
      onDelete: 'CASCADE'
    })
  }
};
Role_permission.init({
  fkRole_id: {
    primaryKey: true,
    type: DataTypes.STRING,
    allowNull: false,
    references: {
      model: 'Role',
      key: 'id',                             
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE'
  },
  fkPermission_id: {
    primaryKey: true,
    type: DataTypes.STRING,
    allowNull: false,
    references: {
      model: 'Permission',
      key: 'id',                             
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE'
  },
}, {
  sequelize,
  modelName: 'Role_permission',
  freezeTableName: true,
  createdAt: true,
  updatedAt: true,
  timestamps: true
});

Role_permission.removeAttribute('id')

module.exports = Role_permission;
  