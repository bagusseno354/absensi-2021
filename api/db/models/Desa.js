'use strict';

const sequelize = require('../connection')

const {
  Model, DataTypes
} = require('sequelize');

class Desa extends Model {

  static associate(models)
  {
    Desa.hasMany(models.Kelompok, {
      foreignKey: 'fkDesa_id',
      onDelete: 'RESTRICT'
    });

    Desa.hasOne(models.User_desa, {
      foreignKey: 'fkDesa_id',
      onDelete: 'CASCADE'
    })
  }
}

Desa.init({
  id: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false
  },
  name: { 
    type: DataTypes.STRING(100),
    allowNull: false 
  },
}, {
  sequelize,
  modelName: 'Desa',
  freezeTableName: true,
  createdAt: true,
  updatedAt: true,
  timestamps: true
})

module.exports = Desa
  