const sequelize = require('../connection')

'use strict';
const {
  Model, DataTypes
} = require('sequelize');

class User_desa extends Model {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here   
    User_desa.belongsTo(models.User, {
      foreignKey: 'fkUser_id',
      onDelete: 'CASCADE'
    });

    User_desa.belongsTo(models.Desa, {
      foreignKey: 'fkDesa_id',
      onDelete: 'CASCADE'
    })
  }
};

User_desa.init({
  fkUser_id: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    allowNull: false,
    references: {
      model: 'User',
      key: 'id',                             
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE'
  },
  fkDesa_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    references: {
      model: 'Desa',
      key: 'id',                             
    },
    onUpdate: 'CASCADE',
    onDelete: 'RESTRICT'
  },
}, {
  sequelize,
  modelName: 'User_desa',
  freezeTableName: true,
  createdAt: true,
  updatedAt: true,
  timestamps: true,
});

module.exports = User_desa;
  