const sequelize = require('../connection')

'use strict';
const {
  Model, DataTypes, Sequelize
} = require('sequelize');

class PresenceHead extends Model {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
     PresenceHead.belongsToMany(models.DataContent, {
       through: models.Presence,
       foreignKey: 'fkPresenceHead_id',
       onDelete: 'CASCADE'
     });

     PresenceHead.hasMany(models.PresenceHeadFilter, {
       foreignKey: 'fkPresenceHead_id'
     });

     PresenceHead.belongsTo(models.DataHead, {
       foreignKey: 'fkDataHead_id'
     });

     PresenceHead.belongsTo(models.User, {
       foreignKey: 'fkUser_id',
       onDelete: 'RESTRICT'
     })
  }
};
PresenceHead.init({
  id: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false
  },
  name: { 
    type: DataTypes.STRING(100),
    allowNull: false 
  },
  fkUser_id: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  fkDataHead_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    onDelete: 'CASCADE'
  },
  allowDuplicate: {
    type: DataTypes.INTEGER(1),
    allowNull: false,
    defaultValue: 0
  },
  startDatetime: {
    type: DataTypes.DATE,
    allowNull: true
  },
  endDatetime: {
    type: DataTypes.DATE,
    allowNull: true
  },
  inputDesaKelompok: {
    type: DataTypes.INTEGER(1),
    allowNull: false,
    defaultValue: 1
  }
}, {
  sequelize,
  modelName: 'PresenceHead',
  freezeTableName: true,
  createdAt: true,
  updatedAt: true,
  timestamps: true
});

module.exports = PresenceHead;
  