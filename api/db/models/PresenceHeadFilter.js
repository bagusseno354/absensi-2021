const sequelize = require('../connection')

'use strict';
const {
  Model, DataTypes, Sequelize
} = require('sequelize');

class PresenceHeadFilter extends Model {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
  }
};
PresenceHeadFilter.init({
  id: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false
  },
  fkPresenceHead_id: { 
    type: DataTypes.INTEGER,
    allowNull: false,
    onDelete: 'CASCADE' 
  },
  columnName: {
    type: DataTypes.STRING(100),
    allowNull: false
  },
  operator: {
    type: DataTypes.STRING(2),
    allowNull: false
  },
  columnValue: {
    type: DataTypes.STRING(100),
    allowNull: false
  },
}, {
  sequelize,
  modelName: 'PresenceHeadFilter',
  freezeTableName: true,
  createdAt: true,
  updatedAt: true,
  timestamps: true
});

module.exports = PresenceHeadFilter;
  