const sequelize = require('../connection')

'use strict';
const {
  Model, DataTypes, Sequelize
} = require('sequelize');

class Role extends Model {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
    Role.hasMany(models.Role_permission, {
      foreignKey: 'fkRole_id',
      onDelete: 'CASCADE'
    })

    Role.hasMany(models.User, {
      foreignKey: 'fkRole_id',
      onDelete: 'RESTRICT'
    })
  }
};
Role.init({
  id: {
    primaryKey: true,
    type: DataTypes.STRING,
    allowNull: false
  },
  name: { 
    type: DataTypes.STRING(100),
    allowNull: false 
  },
  description: {
    type: DataTypes.STRING(200),
    allowNull: false
  },
}, {
  sequelize,
  modelName: 'Role',
  freezeTableName: true,
  createdAt: true,
  updatedAt: true,
  timestamps: true
});

module.exports = Role;
  