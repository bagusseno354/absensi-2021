'use strict';
const sequelize = require('../connection')

const {
  Model, DataTypes, Sequelize
} = require('sequelize');

class DataHeadMetaValueOption extends Model {

  static associate(models)
  {
    DataHeadMetaValueOption.belongsTo(models.DataHeadMeta, {
      foreignKey: 'fkDataHeadMeta_id',
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE'
    })
  }
}

DataHeadMetaValueOption.init({
  id: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false
  },
  fkDataHeadMeta_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  value: { 
    type: DataTypes.STRING(100),
    allowNull: false 
  },
}, {  
  sequelize,
  modelName: 'DataHeadMetaValueOption',
  freezeTableName: true,
  createdAt: true,
  updatedAt: true,
  timestamps: true
});

module.exports = DataHeadMetaValueOption;
  