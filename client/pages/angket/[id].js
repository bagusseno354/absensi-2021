import { useState } from 'react';
import { useRouter } from 'next/router'
import InputAngketForm from '../../components/InputAngketForm';
import PublicLayout from '../../components/Layout/PublicLayout';

export default function Angket()
{
    const [options, setOptions] = useState([]);
    const router = useRouter()
    const presenceHeadId = router.query.id;

    return (
        <PublicLayout 
            title={'Angket + Sertifikat'} 
            style={{ minHeight: '100vh' }}
        >
            <InputAngketForm presenceHeadId={presenceHeadId} />
        </PublicLayout>
    )
}