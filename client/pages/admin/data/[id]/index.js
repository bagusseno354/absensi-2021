import React, { useState, useEffect } from 'react';
import UserLayout from '../../../../components/Layout/UserLayout';
import DataContentTable from '../../../../components/DataContentTable';

import "antd/dist/antd.css";
import { useRouter } from 'next/router';

export {getServerSideProps} from '../../../../lib/auth'

export default function ViewData(props) { 

  const router = useRouter()
  const [dataHead, setDataHead] = useState(null)
  const dataHeadId = router.query.id;

  let fetchedDataHead = false

  useEffect(() =>
  {
    if(!fetchedDataHead && router.isReady)
    {
      fetchedDataHead = true
      
      fetch(`${process.env.API_BASE_URL}/api/dataHead/${dataHeadId}`)
      .then(res => 
      {
        if(!res.ok)
          return Promise.reject(res.status)
        
        return res.json()
      })
      .then(json =>
      {
        setDataHead(json)
      })
      .catch(e =>
      {
        console.error(e);
      })
    }
  }, [router.isReady])  

  const breadcrumbs = [
    {
      url: '/admin/data',
      name: 'Data'
    },
    {
      url: `/admin/data/${dataHeadId}`,
      name: dataHead?.name
    }
  ]

  const getWhere = () =>
  {
    if(!props.user)
      return null
    
    if(props.user.Role.id == 'pengurusDesa')
      return `include[Kelompok][include][Desa][where][id][__eq]=${props.user.User_desa.Desa.id}&include[Kelompok][include][Desa][required]=true&include[Kelompok][required]=true`
    else if(props.user.Role.id == 'pengurusKelompok')
      return `where[fkKelompok_id][__eq]=${props.user.User_kelompok.Kelompok.id}`
  }

  const onlyDesaId = props.user.User_kelompok ? props.user.User_kelompok.Kelompok.Desa.id : props.user.User_desa?.Desa.id
  const onlyKelompokId = props.user.User_kelompok?.Kelompok.id

  return (
    <UserLayout 
      user={props.user}
      previousUrl='/admin/data' 
      title={dataHead?.name || 'Data tidak ditemukan'} 
      style={{ minHeight: '100vh' }}
      breadcrumbs={breadcrumbs}
    >
      {dataHead ? 
      <>                      
        {onlyKelompokId && <>Hanya khusus menampilkan <b>kelompok {`${props.user.User_kelompok.Kelompok.name}`}</b>. Data terintegrasi dengan data daerah.</>}
        {!onlyKelompokId && onlyDesaId && <>Hanya khusus menampilkan <b>desa {`${props.user.User_desa.Desa.name}`}</b>. Data terintegrasi dengan data daerah.</>}

        <DataContentTable onlyDesaId={onlyDesaId} onlyKelompokId={onlyKelompokId} where={getWhere()} dataHeadId={dataHeadId} enableActions={true} />        
      </>
      :
      <>
        Data tidak ditemukan
      </>
      }
    </UserLayout>
  )
  
}