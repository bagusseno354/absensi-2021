import React from 'react';
import UserLayout from '../../../components/Layout/UserLayout';

import "antd/dist/antd.css";
import Link from 'next/link';
import UserTable from '../../../components/UserTable';

export {getServerSideProps} from '../../../lib/auth'

export default function User(props) 
{   
  return (
    <UserLayout user={props.user} title='User' style={{ minHeight: '100vh' }}>
      <UserTable enableActions={true} />
    </UserLayout>
  )
}