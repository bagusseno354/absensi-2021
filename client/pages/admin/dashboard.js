import React from 'react';
import UserLayout from '../../components/Layout/UserLayout';

import "antd/dist/antd.css";

export {getServerSideProps} from '../../lib/auth'

export default function Home(props) 
{ 

  return (
    <UserLayout user={props.user} title='Dashboard' style={{ minHeight: '100vh' }}>
      <p>
        Selamat datang {props.user.name}!
      </p>
    </UserLayout>
  )
}