import React from 'react';
import { Layout, Row, Form, Input, message, Space, Typography } from 'antd';
import { Button } from 'antd';
import { withRouter } from 'next/router';
import cookie from 'cookie';

import "antd/dist/antd.css";  
const { Title } = Typography;

class Login extends React.Component { 
  
  constructor(props)
  {
    super(props)

    this.props = props
  }

  componentDidMount()
  {
    if(this.props.user)
      router.push('/admin/dashboard')
  }   

  onFinish = (values) =>
  {
    const hide = message.loading('Mencoba masuk...');

    fetch(`${process.env.API_BASE_URL}/api/methods/auth/signIn`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',      
      },
      body: JSON.stringify(values),
      credentials: 'include',      
    })
    .then(res => 
    {
      hide();

      if(!res.ok)
        return Promise.reject(res.status)
        
      if(res.status == 404)
        message.error('Salah email atau password!')
      
      if(res.status == 204)
      {
        fetch(`${process.env.API_BASE_URL}/api/user/me`, {
          credentials: 'include',
        })
        .then(res =>
        {
          if(!res.ok)
            return Promise.reject(res.status)
          
          return res.json()
        })
        .then(json =>
        {
          message.success('Email dan password valid')
          message.loading('Redirecting...')

          setTimeout(() => this.props.router.push(`${process.env.BASE_URL}/admin/dashboard`), 1000);                
        })
        .catch(e =>
        {
          console.error(e);

          message.error(`Failed to fetch user! Error: ${e}`)
        })
      }        
    })
    .catch(e =>
    {
      console.error(e);

      message.error(`Gagal login! Error: ${e}`)
    })
  }

  render() 
  {
    const layout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 16 },
    };
    const tailLayout = {
      wrapperCol: { offset: 8, span: 16 },
    };
  
    const onFinishFailed = (errorInfo) => 
    {
      console.log('Failed:', errorInfo);
    };

    return (
      <Layout style={{ minHeight: '100vh' }}>
        <Row justify="center" align="middle" style={{minHeight: '100vh'}}>
          <Space direction="vertical" style={{padding: 24, background: 'white'}}>
            <Title level={2}>
              <div style={{textAlign: 'center'}}>
                Login
              </div>
            </Title>
            <Form       
              {...layout}              
              onFinish={this.onFinish}
              onFinishFailed={onFinishFailed}
              >
              <Form.Item
                  label="Email"
                  name="email"
                  rules={[{ required: true, message: 'Please input your email!' }]}
              >
                  <Input />
              </Form.Item>

              <Form.Item
                  label="Password"
                  name="password"
                  rules={[{ required: true, message: 'Please input your password!' }]}
              >
                  <Input.Password />
              </Form.Item>

              <Form.Item {...tailLayout}>
                <Button type="primary" htmlType="submit">
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </Space>
        </Row>
      </Layout>
    )
  }
}

export default withRouter(Login);

export async function getServerSideProps(context) 
{
  const cookies = cookie.parse(context.req.headers.cookie || '')

  const res = await fetch(`${process.env.API_BASE_URL}/api/user/me`, {
    headers: {
      'cookie': `token=${cookies.token}`,
      'Content-Type': 'application/json'
    }
  })  

  if(res.ok)
  {
    context.res.writeHead(302, {
      Location: '/admin/dashboard'
    })
    context.res.end()

    return {
      props: {
          ...context.props
      }
    }
  }

  return {
    props: {}
  }
}