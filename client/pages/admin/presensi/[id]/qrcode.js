import { useEffect, useState } from "react";
import QRCode from 'qrcode';
import { useRouter } from "next/router";

export default function Qrcode()
{
    const router = useRouter()
    const presenceHeadId = router.query.id;

    const [presenceHead, setPresenceHead] = useState(null);

    useEffect(() =>
    {
        var canvas = document.getElementById('canvas');

        QRCode.toCanvas(canvas, `${process.env.BASE_URL}/presensi/${presenceHeadId}`, {width: 512}, function (error) 
        {
            if (error) 
            {
                console.error(error)
            }
            else
            {
                console.log('success!');
            }
        });

        fetch(`${process.env.API_BASE_URL}/api/presenceHead/${presenceHeadId}`)
        .then(res => 
        {
            if(!res.ok)
            return Promise.reject(res.status)
            
            return res.json()
        })
        .then(json =>
        {
            setPresenceHead(json);
        });

    }, [router.isReady]);

    return (
        <div className='flex items-center justify-center flex-col h-full'>
            <div className='mt-20 text-xl font-bold'>
                QR Code Acara {presenceHead?.name}
            </div>
            <canvas id='canvas'></canvas>
        </div>
    )
}