import React, { useState, useEffect } from 'react';
import UserLayout from '../../../../components/Layout/UserLayout';

import "antd/dist/antd.css";
import { useRouter } from 'next/router'
import PresenceTable from '../../../../components/PresenceTable';
import { message } from 'antd';

export {getServerSideProps} from '../../../../lib/auth'

export default function Presensi(props) 
{   
  const router = useRouter()
  const [presenceHead, setPresenceHead] = useState(null)
  const [where, setWhere] = useState(null)
  const [creator, setCreator] = useState(null)
  const presenceHeadId = router.query.id;

  let fetched = false

  // get presenceHead, then get user to get the kelompok dan desa filter
  useEffect(() =>
  {
    if(!fetched && router.isReady)
    {
      fetched = true
      
      fetch(`${process.env.API_BASE_URL}/api/presenceHead/${presenceHeadId}`)
      .then(res => 
      {
        if(!res.ok)
          return Promise.reject(res.status)
        
        return res.json()
      })
      .then(json =>
      {
        setPresenceHead(json)

        fetch(`${process.env.API_BASE_URL}/api/user/${json.fkUser_id}?include[Role]&include[User_desa][include][Desa]&include[User_kelompok][include][Kelompok][include][Desa]`)
        .then(res => 
        {
          if(!res.ok)
            return Promise.reject(res.status)
          
          return res.json()
        })
        .then(json =>
        {
          setCreator(json)

          if(json.Role.id == 'pengurusDesa')
            setWhere(`include[Kelompok][include][Desa][where][id][__eq]=${json.User_desa.Desa.id}&include[Kelompok][include][Desa][required]=true&include[Kelompok][required]=true`)
          else if(json.Role.id == 'pengurusKelompok')
            setWhere(`where[fkKelompok_id][__eq]=${json.User_kelompok.Kelompok.id}`)
        })
        .catch(e =>
          {
            console.error(e);
    
            message.error(`Gagal mengambil data pembuat presensi! Error: ${e}`)
          })
      })
      .catch(e =>
      {
        console.error(e);

        message.error(`Gagal mengambil data presensi! Error: ${e}`)
      })
    }
  }, [router.isReady])  

  const breadcrumbs = [
    {
      url: '/admin/presensi',
      name: 'Presensi'
    },
    {
      url: `/admin/presensi/${presenceHeadId}`,
      name: presenceHead?.name
    }
  ]

  return (
    <UserLayout 
      user={props.user} 
      title={`Presensi ${presenceHead?.name}` || 'Presensi tidak ditemukan'} 
      style={{ minHeight: '100vh' }}
      previousUrl='/admin/presensi' 
      breadcrumbs={breadcrumbs}
    >
      <PresenceTable where={where} presenceHeadId={presenceHeadId} enableActions={true} />
    </UserLayout>
  )
}