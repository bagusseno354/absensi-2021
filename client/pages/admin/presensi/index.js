import React from 'react';
import UserLayout from '../../../components/Layout/UserLayout';
import { message } from 'antd';

import "antd/dist/antd.css";
import Link from 'next/link';
import PresenceHeadTable from '../../../components/PresenceHeadTable';

export {getServerSideProps} from '../../../lib/auth'

export default function PresenceHead(props) 
{   
  const getWhere = () =>
  {
    if(!props.user)
      return null
    
    if(props.user.Role.id == 'pengurusDesa')
      return `include[User][include][Role]&where[__or][$User->Role.id$][__eq]=superadmin&where[__or][$User->User_desa->Desa.id$][__eq]=${props.user.User_desa.Desa.id}&include[User][include][User_desa][include][Desa]`
    else if(props.user.Role.id == 'pengurusKelompok')
      return `include[User][include][User_kelompok][Kelompok][where][id][__eq]=${props.user.User_kelompok.Kelompok.id}&include[User][include][User_kelompok][Kelompok][required]=true&include[User][include][User_kelompok][required]=true&include[User][required]=true`
  }

  return (
    <UserLayout user={props.user} title='Presensi' style={{ minHeight: '100vh' }}>
      <PresenceHeadTable where={getWhere()} enableActions={true} />
    </UserLayout>
  )
}