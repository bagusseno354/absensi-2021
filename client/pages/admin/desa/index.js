import React, { useState, useEffect } from 'react';
import UserLayout from '../../../components/Layout/UserLayout';
import { Button, List } from 'antd';
import { PlusCircleOutlined } from '@ant-design/icons'

import "antd/dist/antd.css";
import Link from 'next/link';
import DesaTable from '../../../components/DesaTable';

export {getServerSideProps} from '../../../lib/auth'

export default function Data(props) 
{   
  return (
    <UserLayout user={props.user} title='Desa' style={{ minHeight: '100vh' }}>
      <DesaTable enableActions={true} />
    </UserLayout>
  )
}