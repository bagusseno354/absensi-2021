import React, { useState, useEffect } from 'react';
import UserLayout from '../../../../components/Layout/UserLayout';
import KelompokTable from '../../../../components/KelompokTable';

import "antd/dist/antd.css";
import { useRouter } from 'next/router';

export {getServerSideProps} from '../../../../lib/auth'

export default function ViewDesa(props) { 

  const router = useRouter()
  const [desa, setDesa] = useState(null)
  const desaId = router.query.id;

  let fetched = false

  useEffect(() =>
  {
    if(!fetched && router.isReady)
    {
      fetched = true
      
      fetch(`${process.env.API_BASE_URL}/api/desa/${desaId}`)
      .then(res => 
      {
        if(!res.ok)
          return Promise.reject(res.status)
        
        return res.json()
      })
      .then(json =>
      {
        setDesa(json)
      })
      .catch(e =>
      {
        console.error(e);
      })
    }
  }, [router.isReady])  

  const breadcrumbs = [
    {
      url: '/admin/desa',
      name: 'Desa'
    },
    {
      url: `/admin/desa/${desaId}`,
      name: desa?.name
    }
  ]

  return (
    <UserLayout 
      user={props.user}
      previousUrl='/admin/desa' 
      title={desa?.name || 'Desa tidak ditemukan'} 
      style={{ minHeight: '100vh' }}
      breadcrumbs={breadcrumbs}
    >
      {desa ? 
      <>                      
        <KelompokTable desaId={desaId} enableActions={true} />        
      </>
      :
      <>
        Desa tidak ditemukan
      </>
      }
    </UserLayout>
  )
  
}