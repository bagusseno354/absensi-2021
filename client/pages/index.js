import PublicLayout from '../components/Layout/PublicLayout'
import "antd/dist/antd.css";

export default function()
{

    return(
        <PublicLayout title="Absensi2021">
            Selamat datang di Absensi2021! Aplikasi ini adalah aplikasi untuk
            mengatur absensi beserta datanya. Daerah kamu butuh aplikasi ini juga? Silakan kontak 085217080220.
            Alhamdulillah jazakumullahu khoiroo!
        </PublicLayout>
    )
}