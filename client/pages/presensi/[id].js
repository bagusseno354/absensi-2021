import React, { useState, useEffect } from 'react';
import PublicLayout from '../../components/Layout/PublicLayout';

import "antd/dist/antd.css";
import { useRouter } from 'next/router'
import PresenceAndRegistrationForm from '../../components/PresenceAndRegistrationForm';
import { Col, Row, message } from 'antd';
import RecentLocalPresenceTable from '../../components/RecentLocalPresenceTable';
import Title from 'antd/lib/typography/Title';
import moment from 'moment';

export default function Presensi(props) 
{   
  const router = useRouter()
  const [presenceHead, setPresenceHead] = useState(null)
  const [inTime, setInTime] = useState(false)
  const [where, setWhere] = useState(null)
  const [creator, setCreator] = useState(null)
  const [onlyDesaId, setOnlyDesaId] = useState(null)
  const [onlyKelompokId, setOnlykelompokId] = useState(null)
  const [recentLocalPresenceIds, setRecentLocalPresenceIds] = useState([])
  const presenceHeadId = router.query.id;

  let fetched = false

  // get presenceHead, then get user to get the kelompok dan desa filter
  // also load the list of presence data in this device
  useEffect(() =>
  {
    if(!fetched && router.isReady)
    {
      fetched = true
      
      fetch(`${process.env.API_BASE_URL}/api/presenceHead/${presenceHeadId}`)
      .then(res => 
      {
        if(!res.ok)
          return Promise.reject(res.status)
        
        return res.json()
      })
      .then(json =>
      {
        setPresenceHead(json)

        // set inTime      
        let {startDatetime, endDatetime} = json;

        // if time is limited
        if(startDatetime && endDatetime)
        {
          let currDate = moment();
          startDatetime = moment(startDatetime);
          endDatetime  =moment(endDatetime);
  
          if((currDate <= endDatetime && currDate >= startDatetime))
          {
            setInTime(true)
          }
        }        
        else
          setInTime(true);

        fetch(`${process.env.API_BASE_URL}/api/user/${json.fkUser_id}?include[Role]&include[User_desa][include][Desa]&include[User_kelompok][include][Kelompok][include][Desa]`)
        .then(res => 
        {
          if(!res.ok)
            return Promise.reject(res.status)
          
          return res.json()
        })
        .then(json2 =>
        {
          setCreator(json2)

          const onlyDesaId = json2.User_kelompok ? json2.User_kelompok.Kelompok.Desa.id : json2.User_desa?.Desa.id
          const onlyKelompokId = json2.User_kelompok?.Kelompok.id

          setOnlyDesaId(onlyDesaId)
          setOnlykelompokId(onlyKelompokId)

          if(json2.Role.id == 'pengurusDesa')
            setWhere(`include[Kelompok][include][Desa][where][id][__eq]=${json2.User_desa.Desa.id}&include[Kelompok][include][Desa][required]=true&include[Kelompok][required]=true`)
          else if(json2.Role.id == 'pengurusKelompok')
            setWhere(`where[fkKelompok_id][__eq]=${json2.User_kelompok.Kelompok.id}`)

          // load local presence data
          const localData = localStorage.getItem(`presenceHead:${json.id}`);

          if(localData)
          {
            let presences = JSON.parse(localData)

            // cross check to the server whether those data are valid
            fetch(`${process.env.API_BASE_URL}/api/Presence?where[id][__in]=${presences.join(',')}`)
            .then(res =>
            {
              if(!res.ok)
                return Promise.reject(res.status)

              return res.json()
            })
            .then(json =>
            {
              let ids = json.map(presence => presence.id)
                              
              setRecentLocalPresenceIds(ids)            
              localStorage.setItem(`presenceHead:${json.id}`, JSON.stringify(ids))
            })
            .catch(e =>
            {
              console.log(e);
            })
          }
        })
        .catch(e =>
          {
            console.error(e);
    
            message.error(`Gagal mengambil data pembuat presensi! Error: ${e}`)
          })
      })
      .catch(e =>
      {
        console.error(e);

        message.error(`Gagal mengambil data presensi! Error: ${e}`)
      })
    }
  }, [router.isReady])

  const onSuccess = (dataContent) =>
  { 
    if(!dataContent)
      return; 
      
    console.log('COTTF');
    console.log(dataContent);
    setRecentLocalPresenceIds([dataContent[0] ? dataContent[0].id : dataContent.id, ...recentLocalPresenceIds])
    localStorage.setItem(`presenceHead:${presenceHead.id}`, JSON.stringify([dataContent[0] ? dataContent[0].id : dataContent.id, ...recentLocalPresenceIds]))
  }

  return (
    <PublicLayout 
      title={presenceHead ? `Presensi ${presenceHead?.name}` : 'Presensi tidak ditemukan'} 
      style={{ minHeight: '100vh' }}
      >
      <Row gutter={64}>
        <Col xs={24} lg={12}>
        {(presenceHead || creator?.Role?.id == 'pengurusDaerah') ? 
          (inTime ?
          <PresenceAndRegistrationForm 
            where={where}
            presenceHeadId={presenceHeadId} 
            dataHeadId={presenceHead?.fkDataHead_id}
            onSuccess={onSuccess}
            resetFieldsOnSuccess={true}
            onlyDesaId={onlyDesaId}
            onlyKelompokId={onlyKelompokId}
            inputDesaKelompok={presenceHead?.inputDesaKelompok}
          /> 
          : <>Di luar waktu presensi</>)
        :
          <>Presensi tidak ditemukan</>}         
        </Col>
        <Col xs={24} lg={12}>
          <Title level={3}>
            Daftar Presensi di Device ini
          </Title>          
          <RecentLocalPresenceTable recentLocalPresenceIds={recentLocalPresenceIds} />
        </Col>
      </Row>
    </PublicLayout>
  )
}