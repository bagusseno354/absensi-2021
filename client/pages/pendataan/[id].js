import React, { useState, useEffect } from 'react';
import PublicLayout from '../../components/Layout/PublicLayout';

import "antd/dist/antd.css";
import { useRouter } from 'next/router'
import DataContentForm from '../../components/DataContentForm';
import { Col, Row, message } from 'antd';
import RecentLocalRegistrationTable from '../../components/RecentLocalRegistrationTable';
import Title from 'antd/lib/typography/Title';
import moment from 'moment';

export default function Presensi(props) 
{   
  const router = useRouter()
  const [dataHead, setDataHead] = useState(null)
  const [inTime, setInTime] = useState(false)
  const [where, setWhere] = useState(null)
  const [creator, setCreator] = useState(null)
  const [onlyDesaId, setOnlyDesaId] = useState(null)
  const [onlyKelompokId, setOnlykelompokId] = useState(null)
  const [recentLocalDataContentIds, setRecentLocalDataContentIds] = useState([])
  const dataHeadId = router.query.id;

  let fetched = false

  // get presenceHead, then get user to get the kelompok dan desa filter
  // also load the list of presence data in this device
  useEffect(() =>
  {
    if(!fetched && router.isReady)
    {
      fetched = true
      
      fetch(`${process.env.API_BASE_URL}/api/dataHead/${dataHeadId}`)
      .then(res => 
      {
        if(!res.ok)
          return Promise.reject(res.status)
        
        return res.json()
      })
      .then(json =>
      {
        setDataHead(json);                
      })
      .catch(e =>
      {
        console.error(e);

        message.error(`Gagal mengambil data! Error: ${e}`)
      });

      // load local presence data
      const localData = localStorage.getItem(`dataHead:${dataHeadId}`);

      if(localData)
      {
        let dataContents = JSON.parse(localData)

        // cross check to the server whether those data are valid
        fetch(`${process.env.API_BASE_URL}/api/DataContent?where[id][__in]=${dataContents.join(',')}`)
        .then(res =>
        {
          if(!res.ok)
            return Promise.reject(res.status)

          return res.json()
        })
        .then(json =>
        {
          let ids = json.map(presence => presence.id)
                          
          setRecentLocalDataContentIds(ids)
          localStorage.setItem(`dataHead:${dataHeadId}`, JSON.stringify(ids))
        })
        .catch(e =>
        {
          console.log(e);
        })
      }
    }
  }, [router.isReady])

  const onSuccessRegistrate = (dataContent) =>
  { 
    if(!dataContent)
      return; 
      
    setRecentLocalDataContentIds([dataContent[0].id, ...recentLocalDataContentIds])
    localStorage.setItem(`dataHead:${dataHead.id}`, JSON.stringify([dataContent[0].id, ...recentLocalDataContentIds]))
  }

  return (
    <PublicLayout 
    title={dataHead ? `Pendataan ${dataHead?.name}` : 'Pendataan tidak ditemukan'} 
    style={{ minHeight: '100vh' }}
    >
    <Row gutter={64}>
      <Col xs={24} lg={12}>
      {dataHead ? 
            <DataContentForm dataHeadId={dataHeadId} onSuccessRegistrate={onSuccessRegistrate} type='POST' resetFieldsOnSuccess={true} />
        :
          <>Pendataan tidak ditemukan</>}          
      </Col>
      <Col xs={24} lg={12}>
        <Title level={3}>
          Daftar Pendataan di Device ini
        </Title>          
        <RecentLocalRegistrationTable recentLocalDataContentIds={recentLocalDataContentIds} />
      </Col>
    </Row>
  </PublicLayout>
  )
}