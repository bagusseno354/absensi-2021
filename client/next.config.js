const withAntdLess = require('next-plugin-antd-less');
const env = require('dotenv').config();

module.exports = withAntdLess({
  env: {...env.parsed},
  async rewrites() {
    return [
      { source: '/api/proxy/:path*', destination: `${env.parsed.API_PROXY_URL}/:path*` }
    ]
  }
});