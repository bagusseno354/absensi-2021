import { Button, Form, message, Select, Input } from 'antd';
import { SearchOutlined } from '@ant-design/icons'
import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import $ from 'jquery';

export default function InputAngketForm(props)
{
  const router = useRouter()
  const [options, setOptions] = useState([])
  const [selectedDataContentId, setSelectedDataContentId] = useState(null)
  const [selectedName, setSelectedName] = useState('Test');
  const [justSelected, setJustSelected] = useState(false)
  const [inputValue, setInputValue] = useState('')
  const [presenceHead, setPresenceHead] = useState(null)
  const [form] = Form.useForm()
  let isSubmitting = false

  const {presenceHeadId} = props

  console.log('TAYOOOO');
  console.log([presenceHeadId]);
  
  useEffect(() => form.resetFields(), [props.presenceHeadId]);

  useEffect(() =>
  {          
    const abortController = new AbortController()
    const signal = abortController.signal

    fetch(`${process.env.API_BASE_URL}/api/presenceHead/${presenceHeadId}?include[User][include][User_desa][include][Desa][attributes]=id,name&include[User][include][User_kelompok][include][Kelompok][attributes]=id,name`, {signal})
    .then(res => 
    {
      if(!res.ok)
        return Promise.reject(res.status)
      
      return res.json()
    })
    .then(json =>
    {
      setPresenceHead(json)
    })
    .catch(e =>
    {
      console.log('inside input presence form');
      console.error(e);
    })   

    return function cleanup()
    {
      abortController.abort()
    }
  }, [presenceHeadId]) 

  const onSearch = (searchText) =>
  {
    if(searchText == '')
    {
      setOptions([])
      return
    }

    if(!presenceHead)
    {
      console.error('Presence head still not loaded');
      return
    }  
    
    fetch(`${process.env.API_BASE_URL}/api/dataContent?${props.where}&where[fkDataHead_id][__eq]=${presenceHead.fkDataHead_id}&where[nama][__like]=%${searchText}%&include[Kelompok][include][Desa][attributes]=name&include[Kelompok][attributes]=name&include[Presences][where][fkPresenceHead_id][__eq]=${presenceHead.id}&include[Presences][required][=]=true`)
    .then(res => 
    {
      if(!res.ok)
        return Promise.reject(res.status)
      
      return res.json()
    })
    .then(json =>
    {
      const searchResult = json.map(({nama, id, Kelompok: {name: kelompok, Desa: {name: desa}}, Presences }) => <Select.Option nama={nama} key={id} value={id}>
        {`${nama} - ${desa} - ${kelompok}`}
        </Select.Option>)
      
      setOptions(searchResult)
    })
    .catch(e =>
    {
      console.error(e);
    })   
  }

  const onFinish = (values) =>
  {
    if(isSubmitting)
      return

    isSubmitting = true

    const hide = message.loading(`Mencoba submit...`);
    
    const url = `${process.env.API_BASE_URL}/api/dataContent/${selectedDataContentId}`

    fetch(url, 
    {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',      
      },
      body: JSON.stringify(values),
      credentials: 'include',
    })
    .then((res) =>
    {
      if(res.status == 403)
        if(json.errorCode == 'not_in_time')
          return message.error('Tidak dalam waktu presensi! Presensi mungkin sudah tutup atau belum dibuka.')

      if(!res.ok)
        return Promise.reject(res.status)

      setOptions([])

      message.success(`Berhasil submit`)     
      
      if(props.resetFieldsOnSuccess)
        form.resetFields()                

        let ctxWidth = 1680;
        let ctxHeight = 1187;
        let ctx = $('#ctx')[0].getContext('2d');
        let certImage = document.getElementById('certImage');

        ctx.canvas.width = ctxWidth;
        ctx.canvas.height = ctxHeight;

        if(!selectedDataContentId) {
          message.error('Pilih terlebih dahulu');
          return;
        }

        ctx.drawImage(certImage, 0, 0, ctxWidth, ctxHeight);
        ctx.font = `50px Verdana`;
        ctx.fillStyle = '#000';
        ctx.textAlign = 'center';
        ctx.fillText(selectedName, ctxWidth/2, ctxHeight/2 - 35);

        var link = document.createElement('a');
        link.download = `sertifikat-${selectedName}.png`;
        link.href = ctx.canvas.toDataURL();
        link.click();

        message.loading('Mendownload sertifikat. Silakan cek Downloads pada browser');
    })
    .catch(e =>
    {
      console.error(e);

      isSubmitting = false      
      
      message.error(`Gagal presensi! Error: ${e}`)
    })
  }

  return(
    <>        
    <Form
      form={form}
      layout="vertical"
      onFinish={onFinish}>
      
      <Form.Item
        name='fkPresenceHead_id'
        initialValue={props.presenceHeadId}
        hidden={true}
      />

      <Form.Item
        name='status'
        initialValue='hadir'
        hidden={true}
      />

      <Form.Item
        label='Nama (Tersedia hanya untuk yang sudah presensi)'
        name='id'        
        rules={[{ required: true, message: 'Wajib diisi' }]}
      >
        <Select
          showSearch
          placeholder="Cari nama..."
          optionFilterProp="children"
          onSearch={onSearch}
          onSelect={(value, option) => {
            setSelectedDataContentId(value);
            setSelectedName(option.nama)
          }}
          suffixIcon={<SearchOutlined />}
          filterOption={(input, option) =>
            option.nama.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {options}
        </Select>
      </Form.Item>

      <Form.Item
        label='Anak Ke'
        name='anakKe'        
        rules={[{ required: true, message: 'Wajib isi data' }]}
      >
        <Input type="number" />
      </Form.Item>

      <Form.Item
        label='Dari berapa bersaudara?'
        name='dariJumlahSaudara'        
        rules={[{ required: true, message: 'Wajib isi data' }]}
      >
        <Input type="number" />
      </Form.Item>

      <Form.Item
        label='Pendidikan Terkahir'
        name='pendidikanTerakhir'        
        rules={[{ required: true, message: 'Wajib isi data' }]}
      >
        <Input type="text" />
      </Form.Item>

      <Form.Item
        label='Pekerjaan'
        name='pekerjaan'        
        rules={[{ required: true, message: 'Wajib isi data' }]}
      >
        <Input type="text" />
      </Form.Item>

      <Form.Item
        label='Hobi'
        name='hobby'        
        rules={[{ required: true, message: 'Wajib isi data' }]}
      >
        <Input type="text" />
      </Form.Item>

      <Form.Item
        label='Dapuan'
        name='dapuan'        
        rules={[{ required: true, message: 'Wajib isi data' }]}
      >
        <Input type="text" />
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit">
          Submit dan Download Sertifikat
        </Button>
      </Form.Item>
  </Form>

  <img id='certImage' src='../serti.jpg' hidden/>
      <canvas
        hidden
        id='ctx'
        width='1280'
        height='905'>
      </canvas>
    </>
  )
}