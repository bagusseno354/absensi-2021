import { Table, Space, Modal, Button, Popconfirm, message, Row, Col, Alert } from "antd";
import { useEffect, useState } from "react";
import DataContentForm from './DataContentForm'
import { PlusCircleOutlined, LinkOutlined, DownloadOutlined, ImportOutlined } from '@ant-design/icons'
import Title from 'antd/lib/typography/Title'
import axios from "axios";
import { CSVLink } from "react-csv";
import Papa from "papaparse";
import SelectDesa from "./SelectDesa";
import SelectKelompok from "./SelectKelompok";

export default function DataContentTable(props)
{
  const [dataHead, setDataHead] = useState(null)
  const [dataContent, setDataContent] = useState(null)
  const [columns, setColumns] = useState(null)
  const [dataContentVersion, setDataContentVersion] = useState(0)
  const [dataContentMetaValuesVersion, setDataContentMetaValuesVersion] = useState(0);
  const [dataHeadVersion, setDataHeadVersion] = useState(0)
  const [showPostForm, setShowPostForm] = useState(false)
  const [showUpdateForm, setShowUpdateForm] = useState(false)
  const [dataContentIdToUpdate, setDataContentIdToUpdate] = useState(null)
  const [dataHeadMetas, setDataHeadMetas] = useState(null);
  const [isImportModalOpen, setIsImportModalOpen] = useState(false)
  const [fileToImport, setFileToImport] = useState(null);
  const [kelompokIdToImport, setKelompokIdToImport] = useState(null);
  const [desaIdToImport, setDesaIdToImport] = useState(null);
  const [isImporting, setIsImporting] = useState(false);

  useEffect(() =>
  {                
    axios.get(`${process.env.API_BASE_URL}/api/dataHead/${props.dataHeadId}`)
    .then(res => 
    {
      console.log(res.data.name);
      setDataHead(res.data)
    })
    .catch(e =>
    {
      console.error(e);
    })    
  }, [dataHeadVersion])

  // get data head metas
  useEffect(() =>
  {
    console.log(props.where);

    const preColumns = [
      {
        title: 'id',
        dataIndex: 'id',
        key: 'id'
      },
      {
        title: 'Nama',
        dataIndex: 'nama',
        key: 'Nama',
        sorter: (a, b) => a.nama.localeCompare(b.nama),
        sortDirections: ['ascend', 'descend'],
      },
      {
        title: 'Desa',
        dataIndex: 'Desa',
        key: 'Desa',
        sorter: (a, b) => a.Desa.localeCompare(b.Desa),
        sortDirections: ['ascend', 'descend'],
      },
      {
        title: 'Kelompok',
        dataIndex: 'Kelompok',
        key: 'Kelompok',
        sorter: (a, b) => a.Kelompok.localeCompare(b.Kelompok),
        sortDirections: ['ascend', 'descend'],
      }
    ];

    let newColumns = [];

    // get metas first
    axios.get(`${process.env.API_BASE_URL}/api/dataHeadMeta?where[fkDataHead_id][__eq]=${props.dataHeadId}`)
    .then(res =>
    {
      newColumns = res.data.map(column => 
      {        
        return ({
          title: column.name,
          dataIndex: column.id,
          key: column.id,
          sorter: (a, b) => a[column.id]?.localeCompare(b[column.id]),
          sortDirections: ['ascend', 'descend'],
        })        
      })

      if(props.enableActions)
        newColumns.push({
          title: 'Action',
          dataIndex: 'action',
          key: 'action',
          render: (text, record) =>
          (
            <Space size="middle">
              <a onClick={() => edit(record.id)}>Edit</a>
              <Popconfirm title="Yakin menghapus data orang ini? Seluruh data absensi atas nama orang ini akan ikut terhapus!" placement="leftTop" onConfirm={() => del(record.id)} okText="Yes" cancelText="No">
                <a>Delete</a>
              </Popconfirm>
            </Space>
          )
        })

      setColumns([...preColumns, ...newColumns]);
      setDataHeadMetas(res.data);
    })   
    .catch(e =>
    {
      if(e.response.status == 404)
      {
        if(props.enableActions)
          newColumns.push({
            title: 'Action',
            dataIndex: 'action',
            key: 'action',
            render: (text, record) =>
            (
              <Space size="middle">
                <a onClick={() => edit(record.id)}>Edit</a>
                <Popconfirm title="Yakin menghapus data orang ini? Seluruh data presensi atas nama orang ini akan ikut terhapus!" placement="leftTop" onConfirm={() => del(record.id)} okText="Yes" cancelText="No">
                  <a>Delete</a>
                </Popconfirm>
              </Space>
            )
          })

        setColumns([...preColumns, ...newColumns]);
      }
    })
    
  }, [])

  // get data contents
  useEffect(() =>
  {
    const url = props.url ? props.url : `${process.env.API_BASE_URL}/api/dataContent?${props.where}&where[fkDataHead_id][__eq]=${props.dataHeadId}&include[Kelompok][include][Desa]`;

    axios.get(url)
    .then(res =>
    {
      res.data = res.data.map(data => ({...data, Kelompok: data.Kelompok.name, Desa: data.Kelompok.Desa.name}));      

      setDataContent(res.data);
      setDataContentMetaValuesVersion(prev => prev + 1);
    })
    .catch(e =>
    {
      setDataContent(null);
    })

  }, [dataContentVersion, columns])

  // get data content metas
  useEffect(() =>
  {
    const url = `${process.env.API_BASE_URL}/api/dataContentMetaValue?include[Kelompok][include][Desa]&include[DataContent][attributes]=fkDataHead_id&include[DataContent][where][fkDataHead_id][__eq]=${props.dataHeadId}&include[DataContent][required]=true`;

    axios.get(url)
    .then(res =>
    {
      const tempDataContents = [...dataContent];

      res.data.forEach(meta =>
      {
        const row = tempDataContents.find(tempDataContent => tempDataContent.id == meta.fkDataContent_id);

        if(row)
          row[meta.fkDataHeadMeta_id] = meta.value;
      });

      setDataContent(tempDataContents)

      console.log(tempDataContents);
    })
    .catch(e =>
    {
      console.log(e);
    })

  }, [dataContentMetaValuesVersion]) 

  const onSuccess = () => 
  {
    setDataContentVersion(dataContentVersion+1)

    if(showUpdateForm)
      setShowUpdateForm(false)
  }

  const edit = (dataContentId) =>
  {
    setDataContentIdToUpdate(dataContentId)
    setShowUpdateForm(true)
  }

  const del = (dataContentId) =>
  {
    const hide = message.loading('Menghapus...')

    fetch(`${process.env.API_BASE_URL}/api/dataContent/${dataContentId}`, {
      method: 'DELETE',
      credentials: 'include'
    }).then(res =>
    {
      hide() 

      if(!res.ok)
        return Promise.reject(res.status)

      message.success('Terhapus')
      
      setDataContentVersion(prev => prev+1)

    })
    .catch(e =>
    {
      message.error(`Gagal menghapus! Error: ${e}`)

      console.error(e);
    })
  }

  const handleFileChange = (e) => 
  {
    const allowedExtensions = ['csv'];

    // Check if user has entered the file
    if (e.target.files.length) {
        const inputFile = e.target.files[0];

        // Check the file extensions, if it not
        // included in the allowed extensions
        // we show the error
        const fileExtension =
            inputFile?.type.split("/")[1];
        if (
            !allowedExtensions.includes(fileExtension)
        ) {
            alert("Please input a csv file");
            return;
        }

        // If input type is correct set the state
        setFileToImport(inputFile);
    }
  };

  const importCSV = () => 
  {
    if(isImporting)
    {
      alert('Sedang proses mengimport...');

      return;
    }

    // If user clicks the parse button without
    // a file we show a error
    if (!fileToImport) return alert("Enter a valid file");

    // Initialize a reader which allows user
    // to read any file or blob.
    const reader = new FileReader();

    // Event listener on reader when the file
    // loads, we parse it and set the data.
    reader.onload = async ({ target }) => {
        const csv = Papa.parse(target.result, {
            header: true,
        });
        const parsedData = csv?.data;
        const rows = Object.keys(parsedData[0]);

        const columns = Object.values(parsedData[0]);
        const res = rows.reduce((acc, e, i) => {
            return [...acc, [[e], columns[i]]];
        }, []);

        const cleanData = parsedData.map(row => Object.values(row));

        console.log(cleanData);

        axios.post(`${process.env.API_BASE_URL}/api/dataContent/bulk/${dataHead.id}/${kelompokIdToImport}`, cleanData)
        .then(res =>
        {
          if(res.status == 204)
          {
            message.success('Berhasil mengimport semua data!');

            setFileToImport(null);               
            setIsImportModalOpen(false);
            setDataContentVersion(dataContentVersion+1);

            setIsImporting(false);

            const fileToImport = document.getElementById('fileToImport');
            
            if(fileToImport)
              fileToImport.value = null;
          }
          else
          {
            message.error('Gagal mengimport sebagian atau seluruh data! Lihat console untuk informasi lebih lanjut.');

            console.log(res.data);

            setIsImporting(false);
          }
        })
        .catch(e =>
        {
          setIsImporting(false);

          console.error(e);
        })
    };

    setIsImporting(true);
    reader.readAsText(fileToImport);
};

  return(
    <>
      <Row>
        <Col xl={8} md={8} xs={24} style={{alignItems: 'center', display: 'flex'}}>
          {dataHead?.name}
        </Col>
        <Col xl={16} md={16} xs={24} style={{textAlign: 'right'}}>
          <Row gutter={16} style={{display: 'flex', justifyContent: 'flex-end'}}>    
            <Col>
              <Button                             
                icon={<ImportOutlined />} 
                type="link"
                onClick={() => setIsImportModalOpen(true)}
              >              
                Import CSV
              </Button>
            </Col>

            <Col>
              <a target='_blank' rel='noreferrer noopener' href={`${process.env.BASE_URL}/pendataan/${props.dataHeadId}`}>
                <Button                             
                  icon={<LinkOutlined />} 
                  type="link"
                >              
                  Halaman pendataan publik
                </Button>
              </a>
            </Col>
            {dataHead && dataContent ?
              <Col>
                <CSVLink
                data={(() =>
                {
                  const dataContentTemp = JSON.parse(JSON.stringify(dataContent));

                  dataContentTemp.forEach(data =>
                  {
                    delete data['fkDataHead_id'];
                    delete data['fkKelompok_id'];
                    delete data['id'];

                    if(dataHeadMetas)
                      dataHeadMetas.forEach(meta =>
                      {
                        data[meta.name] = data[meta.id];
                        delete data[meta.id];                        
                      })
                  })

                  return dataContentTemp
                })()}
                filename={`Data ${dataHead.name} ${Date()}`}>
                  <Button                             
                    icon={<DownloadOutlined />} 
                    type="link"
                  >              
                    Download CSV
                  </Button>
                </CSVLink>
              </Col>            
            : ''}
            <Col style={{textAlign: 'right'}}>
              <Button 
                icon={<PlusCircleOutlined />} 
                type="primary"
                onClick={() => setShowPostForm(true)}
              >
                Registrasi
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>
      <div style={{overflow: 'scroll'}}>
        <Table dataSource={dataContent} columns={columns} style={{marginTop: 24}}  />
      </div>
      <Modal 
        visible={showPostForm}
        onCancel={() => setShowPostForm(false)}
        footer={[]}
      >
        <Title level={2}>
          Registrasi
        </Title>
        <DataContentForm onlyDesaId={props.onlyDesaId} onlyKelompokId={props.onlyKelompokId} dataHeadId={props.dataHeadId} onSuccess={onSuccess} type='POST' resetFieldsOnSuccess={true} />
      </Modal>
      <Modal 
        visible={showUpdateForm}
        onCancel={() => setShowUpdateForm(false)}
        footer={[]}
      >
        <Title level={2}>
          Ubah data
        </Title>
        <DataContentForm dataHeadId={props.dataHeadId} onSuccess={onSuccess} type='PATCH' dataContentId={dataContentIdToUpdate} />
      </Modal>
      <Modal title='Import CSV' okText={isImporting ? 'Mengimport...' : 'Import'} visible={isImportModalOpen} onOk={importCSV} onCancel={() => setIsImportModalOpen(false)}>
        <div>
          Pastikan file CSV memiliki urutan data sebagai berikut: <b>Nama lengkap - {dataHeadMetas?.map(meta => meta.name)?.join(' - ')}</b>.          
        </div>
        <div className='mt-4'>
          <Alert type='warning' showIcon message='Baris pertama langsung berupa data, bukan nama kolom.' />        
        </div>
        <div className='flex justify-between gap-4 mt-4'>
          <div>
            Desa: 
          </div>
          <SelectDesa 
            onSelect={desaId => setDesaIdToImport(desaId)}            
          />
        </div>
        <div className='flex justify-between gap-4 mt-2'>
          <div>
            Kelompok: 
          </div>
          <SelectKelompok 
            desaId={desaIdToImport} onSelect={kelompokId => setKelompokIdToImport(kelompokId)} 
          />
        </div>
        <input id='fileToImport' className='mt-8' type='file' onChange={handleFileChange} />
      </Modal>
    </>
  )
}