import { Space } from 'antd'
import Title from 'antd/lib/typography/Title'
import InputPresenceForm from '../components/InputPresenceForm'
import DataContentForm from '../components/DataContentForm'

export default function PresenceAndRegistrationForm(props)
{
  return (
    <Space size='large' direction='vertical'>
      <div>
        <Title level={2}>
          Presensi
        </Title>
        <InputPresenceForm where={props.where} onSuccess={props.onSuccess} presenceHeadId={props.presenceHeadId} resetFieldsOnSuccess={props.resetFieldsOnSuccess} />
      </div>
      <div>
        <Title level={2}>
          Registrasi + Auto Presensi
        </Title>
        <p>
          Registrasi sudah termasuk presensi, ya! Jadi tidak perlu presensi setelah registrasi :)
        </p>
        <DataContentForm inputDesaKelompok={props.inputDesaKelompok} onlyDesaId={props.onlyDesaId} onlyKelompokId={props.onlyKelompokId} presenceStatus='present' presenceHeadId={props.presenceHeadId} dataHeadId={props.dataHeadId} onSuccess={props.onSuccess} type='POST' resetFieldsOnSuccess={props.resetFieldsOnSuccess} />
      </div>
    </Space>
  )
}