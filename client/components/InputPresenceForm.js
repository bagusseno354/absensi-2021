import { Button, Form, message, Select, Input } from 'antd';
import { SearchOutlined } from '@ant-design/icons'
import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';

export default function InputPresenceForm(props)
{
  const router = useRouter()
  const [options, setOptions] = useState([])
  const [selectedDataContentId, setSelectedDataContentId] = useState(null)
  const [justSelected, setJustSelected] = useState(false)
  const [inputValue, setInputValue] = useState('')
  const [presenceHead, setPresenceHead] = useState(null)
  const [form] = Form.useForm()
  const [isSubmitting, setIsSubmitting] = useState({status: false, values: null})
  const {presenceHeadId, presenceStatus} = props

  console.log('TAYOOOO');
  console.log([presenceHeadId]);
  
  useEffect(() => form.resetFields(), [props.presenceHeadId]);

  useEffect(() =>
  {          
    const abortController = new AbortController()
    const signal = abortController.signal

    fetch(`${process.env.API_BASE_URL}/api/presenceHead/${presenceHeadId}?include[User][include][User_desa][include][Desa][attributes]=id,name&include[User][include][User_kelompok][include][Kelompok][attributes]=id,name`, {signal})
    .then(res => 
    {
      if(!res.ok)
        return Promise.reject(res.status)
      
      return res.json()
    })
    .then(json =>
    {
      setPresenceHead(json)
    })
    .catch(e =>
    {
      console.log('inside input presence form');
      console.error(e);
    })   

    return function cleanup()
    {
      abortController.abort()
    }
  }, [presenceHeadId]) 

  const onSearch = (searchText) =>
  {
    if(searchText == '')
    {
      setOptions([])
      return
    }

    if(!presenceHead)
    {
      console.error('Presence head still not loaded');
      return
    }  
    
    fetch(`${process.env.API_BASE_URL}/api/dataContent?${props.where}&where[fkDataHead_id][__eq]=${presenceHead.fkDataHead_id}&where[nama][__like]=%${searchText}%&include[Kelompok][include][Desa][attributes]=name&include[Kelompok][attributes]=name&include[Presences][where][fkPresenceHead_id][__eq]=${presenceHead.id}`)
    .then(res => 
    {
      if(!res.ok)
        return Promise.reject(res.status)
      
      return res.json()
    })
    .then(json =>
    {
      const searchResult = json.map(({nama, id, Kelompok: {name: kelompok, Desa: {name: desa}}, Presences }) => <Select.Option nama={nama} key={id} value={id}>
        {Presences.length == 0 ? 
          `${nama} - ${desa} - ${kelompok}`
        : 
          <span style={{opacity: 0.5}}>{`${nama} - ${desa} - ${kelompok} (Sudah ${Presences[0].status == 'hadir' ? 'presensi' : 'izin'})`}</span>
        }
        </Select.Option>)
      
      setOptions(searchResult)
    })
    .catch(e =>
    {
      console.error(e);
    })   
  }

  const onFinish = (values) =>
  {
    setIsSubmitting(prev => ({status: true, values, trigger: prev.trigger + 1}));
  }

  useEffect(async () =>
  {
    if(!isSubmitting.status)
      return

    const hide = message.loading(`Mencoba presensi...`);
    
    const url = `${process.env.API_BASE_URL}/api/presence`

    fetch(url, 
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',      
      },
      body: JSON.stringify(isSubmitting.values),
      credentials: 'include',
    })
    .then(res => 
    {
      setIsSubmitting({status: false, values: null})
      hide();

      const contentType = res.headers.get('content-type')

      if(res.ok || contentType?.indexOf('application/json') !== -1)                            
        return res.json().then(json => ({res, json}))          
      else return Promise.reject(res.status)
    })
    .then(({res, json}) =>
    {
      if(res.status == 403)
        if(json.errorCode == 'not_in_time')
          return message.error('Tidak dalam waktu presensi! Presensi mungkin sudah tutup atau belum dibuka.')

      if(!res.ok)
        return Promise.reject(res.status)

      setOptions([])

      message.success(`Berhasil ${presenceStatus == 'permit' ? 'izin' : 'presensi'}`)     
      
      if(props.resetFieldsOnSuccess)
        form.resetFields()        
        
      if(props.redirectUrl)
      {
        message.loading('Redirecting...')                
        router.push(props.redirectUrl)
      }

      if(props.onSuccess)
        props.onSuccess(json)
    })
    .catch(e =>
    {
      console.error(e);

      setIsSubmitting(prev => ({...prev, status: false}));
      
      message.error(`Gagal presensi! Error: ${e}`)
    })

  }, [isSubmitting])

  return(
    <>        
    <Form
      form={form}
      layout="vertical"
      onFinish={onFinish}>
      
      <Form.Item
        name='fkPresenceHead_id'
        initialValue={props.presenceHeadId}
        hidden={true}
      />

      <Form.Item
        name='status'
        initialValue={presenceStatus == 'permit' ? 'izin' : 'hadir'}
        hidden={true}
      />

      <Form.Item
        label='Nama'
        name='fkDataContent_id'        
        rules={[{ required: true, message: 'Wajib diisi' }]}
      >
        <Select
          showSearch
          placeholder="Cari nama..."
          optionFilterProp="children"
          onSearch={onSearch}
          suffixIcon={<SearchOutlined />}
          filterOption={(input, option) =>
            option.nama.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {options}
        </Select>
      </Form.Item>
      
      {
        presenceStatus == 'permit' &&
        <Form.Item
          label='Alasan izin'
          name='reason'          
          rules={[{ required: true, message: 'Wajib diisi' }]}
        >
          <Input /> 
        </Form.Item>
      }

      <Form.Item>
        <Button type="primary" htmlType="submit">
          {presenceStatus!='permit' ? 'Presensi' : 'Izin'}
        </Button>
      </Form.Item>
  </Form>
    </>
  )
}