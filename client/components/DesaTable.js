import { Table, Space, Modal, Button, Popconfirm, message, Row, Col } from "antd";
import { useEffect, useState } from "react";
import DesaForm from './DesaForm'
import { PlusCircleOutlined } from '@ant-design/icons'
import Title from 'antd/lib/typography/Title'
import Link from "next/link";

export default function DesaTable(props)
{
  const [desas, setDesas] = useState(null)
  const [columns, setColumns] = useState(null)
  const [desaVersion, setDataVersion] = useState(0)
  const [showPostForm, setShowPostForm] = useState(false)
  const [showUpdateForm, setShowUpdateForm] = useState(false)
  const [desaIdToUpdate, setDesaIdToUpdate] = useState(null)

  let fetched = false

  useEffect(() =>
  {
    if(fetched)
      return

    fetched = true

    fetch(props.url ? props.url : `${process.env.API_BASE_URL}/api/desa`)
    .then(res =>
    {
      if(!res.ok)
        return Promise.reject(res.status)
      
      return res.json()      
    })    
    .then(json =>
    {              
      const {...columns} = json[0]

      setDesas(json)

      console.log(json);

      if(json.length == 0)
        return;

      const newColumns = Object.keys(columns).map(column => 
      {
        if(column == 'name')
          return ({
            title: column.charAt(0).toUpperCase() + column.slice(1),
            dataIndex: column,
            key: column,              
            render: (text, record) =>
            (
              <Link href={`/admin/desa/${record.id}`}>{record.name}</Link> 
            )
          })

        return ({
          title: column.charAt(0).toUpperCase() + column.slice(1),
          dataIndex: column,
          key: column,    
        })
      })

      if(props.enableActions)
        newColumns.push({
          title: 'Action',
          dataIndex: 'action',
          key: 'action',
          render: (text, record) =>
          (
            <Space size="middle">
              <a onClick={() => edit(record.id)}>Edit</a>
              <Popconfirm title="Yakin menghapus desa ini? Seluruh kelompok pada desa ini akan ikut terhapus!" placement="leftTop" onConfirm={() => del(record.id)} okText="Yes" cancelText="No">
                <a>Delete</a>
              </Popconfirm>
            </Space>
          )
        })

      setColumns(newColumns)
    })
    .catch(e =>
    {
      console.error(e);
    })
  }, [desaVersion])

  const onSuccess = () => 
  {
    setDataVersion(desaVersion+1)

    if(showUpdateForm)
      setShowUpdateForm(false)
  }

  const edit = (desaId) =>
  {
    setDesaIdToUpdate(desaId)
    setShowUpdateForm(true)
  }

  const del = (desaId) =>
  {
    const hide = message.loading('Menghapus...')

    fetch(`${process.env.API_BASE_URL}/api/desa/${desaId}`, {
      method: 'DELETE',
      credentials: 'include'
    }).then(res =>
    {
      hide()

      if(!res.ok)
        return Promise.reject(res.status)

      message.success('Terhapus')
      
      setDataVersion(prev => prev+1)

    })
    .catch(e =>
    {
      message.error(`Gagal menghapus! Error: ${e}`)

      console.error(e);
    })
  }

  return(
    <div style={{overflow: 'scroll'}}>
      <Row>
        <Col span={12} style={{alignItems: 'center', display: 'flex'}}>
          Daftar desa
        </Col>
        <Col span={12} style={{textAlign: 'right'}}>
          <Button 
            icon={<PlusCircleOutlined />} 
            type="primary"
            onClick={() => setShowPostForm(true)}
          >
            Tambah desa
          </Button>
        </Col>
      </Row>
      <Table dataSource={desas} columns={columns} style={{marginTop: 24}} />
      <Modal 
        visible={showPostForm}
        onCancel={() => setShowPostForm(false)}
        footer={[]}
      >
        <Title level={2}>
          Tambah desa
        </Title>
        <DesaForm onSuccess={onSuccess} type='POST' resetFieldsOnSuccess={true} />
      </Modal>
      <Modal 
        visible={showUpdateForm}
        onCancel={() => setShowUpdateForm(false)}
        footer={[]}
      >
        <Title level={2}>
          Ubah desa
        </Title>
        <DesaForm onSuccess={onSuccess} type='PATCH' desaId={desaIdToUpdate} />
      </Modal>
    </div>
  )
}