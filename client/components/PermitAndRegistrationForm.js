import { Space } from 'antd'
import Title from 'antd/lib/typography/Title'
import InputPresenceForm from './InputPresenceForm'
import DataContentForm from './DataContentForm'

export default function PresenceAndRegistrationForm(props)
{
  return (
    <Space size='large' direction='vertical'>
      <div>
        <Title level={2}>
          Izin
        </Title>
        <InputPresenceForm where={props.where} onSuccess={props.onSuccess} presenceHeadId={props.presenceHeadId} presenceStatus='permit' resetFieldsOnSuccess={props.resetFieldsOnSuccess} />
      </div>
      <div>
        <Title level={2}>
          Registrasi + Auto Izin
        </Title>
        <p>
          Registrasi sudah termasuk izin, ya! Jadi tidak perlu izin lagi setelah registrasi :)
        </p>
        <DataContentForm inputDesaKelompok={props.inputDesaKelompok} onlyDesaId={props.onlyDesaId} onlyKelompokId={props.onlyKelompokId} presenceStatus='permit' presenceHeadId={props.presenceHeadId} dataHeadId={props.dataHeadId} onSuccess={props.onSuccess} type='POST' resetFieldsOnSuccess={props.resetFieldsOnSuccess} />
      </div>
    </Space>
  )
}