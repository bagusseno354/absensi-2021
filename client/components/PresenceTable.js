import { Table, Space, Modal, Button, Popconfirm, message, Row, Col } from "antd";
import { useEffect, useState } from "react";
import { PlusCircleOutlined, LinkOutlined, DownloadOutlined } from '@ant-design/icons'
import moment from 'moment'
import PresenceAndRegistrationForm from "./PresenceAndRegistrationForm";
import { CSVLink } from "react-csv";
import axios from 'axios';

export default function PresenceTable(props)
{
  const [presenceHead, setPresenceHead] = useState(null)
  const [presences, setPresences] = useState([])
  const [columns, setColumns] = useState(null)
  const [presencesVersion, setPresencesVersion] = useState(0)
  const [presenceHeadVersion, setPresenceHeadVersion] = useState(0)
  const [showPostForm, setShowPostForm] = useState(false)
  const [showUpdateForm, setShowUpdateForm] = useState(false)
  const [dataContentMetaValuesVersion, setDataContentMetaValuesVersion] = useState(0);
  const [dataHeadMetas, setDataHeadMetas] = useState([]);
  const [dataContent, setDataContent] = useState([]);

  // get data contents
  useEffect(() =>
  {
    if(!presenceHead)
      return;

    const url = `${process.env.API_BASE_URL}/api/dataContent?where[fkDataHead_id][__eq]=${presenceHead.fkDataHead_id}`;

    axios.get(url)
    .then(res =>
    {
      setDataContent(res.data);
    })
    .catch(e =>
    {
      setDataContent(null);
    })

  }, [presenceHead])

  // get presence head info
  useEffect(() =>
  {
    fetch(`${process.env.API_BASE_URL}/api/presenceHead/${props.presenceHeadId}`)
    .then(res => 
    {
      if(!res.ok)
        return Promise.reject(res.status)
      
      return res.json()
    })
    .then(json =>
    {
      setPresenceHead(json)
    })
    .catch(e =>
    {
      console.error(e);
    })    
  }, [presenceHeadVersion])

  // get data head metas
  useEffect(() =>
  {
    if(!presenceHead)
      return; 

    const preColumns = [    
      {
        title: 'No',
        dataIndex: 'no',
        key: 'no'
      },    
      {
        title: 'Nama',
        dataIndex: 'nama',
        key: 'Nama',
        sorter: (a, b) => a.nama.localeCompare(b.nama),
        sortDirections: ['ascend', 'descend'],
      },
      {
        title: 'Desa',
        dataIndex: 'Desa',
        key: 'Desa',
        sorter: (a, b) => a.Desa.localeCompare(b.Desa),
        sortDirections: ['ascend', 'descend'],
      },
      {
        title: 'Kelompok',
        dataIndex: 'Kelompok',
        key: 'Kelompok',
        sorter: (a, b) => a.Kelompok.localeCompare(b.Kelompok),
        sortDirections: ['ascend', 'descend'],
      },
      {
        title: 'Waktu Presensi',
        dataIndex: 'waktuPresensi',
        key: 'waktuPresensi'
      },
      {
        title: 'id',
        dataIndex: 'id',
        key: 'id'
      },
      {
        title: 'Status Kehadiran',
        dataIndex: 'status',
        sorter: (a, b) => a.status.localeCompare(b.status),
        key: 'status'
      },
    ];

    let newColumns = [];

    // get metas first
    axios.get(`${process.env.API_BASE_URL}/api/dataHeadMeta?where[fkDataHead_id][__eq]=${presenceHead?.fkDataHead_id}`)
    .then(res =>
    {            
      if(res.data)
        newColumns = res.data.map(dataHeadMeta => 
        {
          console.log('DATAHEADMETAID: ' + dataHeadMeta.id);
          return ({
            title: dataHeadMeta.name,
            dataIndex: dataHeadMeta.id,
            key: dataHeadMeta.id,
            sorter: (a, b) => a[dataHeadMeta.id].localeCompare(b[dataHeadMeta.id]),
            sortDirections: ['ascend', 'descend'],
          })
        });
      
      if(props.enableActions)
      newColumns.push({
        title: 'Action',
        dataIndex: 'action',
        key: 'action',
        render: (text, record) =>
        (
          <Space size="middle">
            <Popconfirm title="Yakin menghapus presensi ini?" placement="leftTop" onConfirm={() => del(record.id)} okText="Yes" cancelText="No">
              <a>Delete</a>
            </Popconfirm>
          </Space>
        )
      })

      setColumns([...preColumns, ...newColumns]);
      setDataHeadMetas(res.data);
    })
    .catch(e =>
    {
      if(e.response.status == 404)
      {
        if(props.enableActions)
          newColumns.push({
            title: 'Action',
            dataIndex: 'action',
            key: 'action',
            render: (text, record) =>
            (
              <Space size="middle">
                <Popconfirm title="Yakin menghapus presensi ini?" placement="leftTop" onConfirm={() => del(record.id)} okText="Yes" cancelText="No">
                  <a>Delete</a>
                </Popconfirm>
              </Space>
            )
          })

        setColumns([...preColumns, ...newColumns]);
      }
    }) 
    
  }, [presenceHead])

  // get all presences
  useEffect(() =>
  {
    fetch(props.url ? props.url : `${process.env.API_BASE_URL}/api/presence?where[fkPresenceHead_id][__eq]=${props.presenceHeadId}&include[DataContent][include][Kelompok][include][Desa][attributes]=name`)
    .then(res =>
    {
      if(res.ok)
        return res.json() 
      
      if(res.status == 404)
        setPresences([])
        
      return Promise.reject(res.status)
    })    
    .then(json =>
    {     
      if(!Array.isArray(json))
        json = [json];

      console.log(json);
      const newPresences = json.map(({id, fkDataContent_id, status, reason, DataContent: {nama, DataContentMetaValues, Kelompok: {name: Kelompok, Desa: {name: Desa}}}, time}, k) => ({no: k+1, nama, Desa, Kelompok, waktuPresensi: moment(time).format('yyyy-MM-DD (hh:mm:ss)'), id, status: status == 'hadir' ? status : `${status} (alasan: ${reason})`, fkDataContent_id}))
                
      setPresences(newPresences);
      setDataContentMetaValuesVersion(prev => prev + 1);

      console.log(newPresences);  
    })
    .catch(e =>
    {
      console.error(e);
    })
  }, [presencesVersion, columns])

  // get data content metas
  useEffect(() =>
  {
    const url = `${process.env.API_BASE_URL}/api/dataContentMetaValue?include[DataContent][include][Presences][where][fkPresenceHead_id][__eq]=${props.presenceHeadId}&include[DataContent][include][Presences][required]=true&include[DataContent][required]=true]`;

    axios.get(url)
    .then(res =>
    {
      console.log('res.data');
      console.log(res.data);
      const tempPresences = [...presences];
      console.log(tempPresences);

      let metaData = res.data ? res.data : [];

      metaData.forEach(meta =>
      {
        const row = tempPresences.find(tempPresence => tempPresence.fkDataContent_id == meta.fkDataContent_id);

        if(row)       
          row[meta.fkDataHeadMeta_id] = meta.value;
      });

      // fill the empty meta with ''
      tempPresences.forEach(presence =>
      {
        metaData.forEach(meta =>
        {
          if(!presence[meta.fkDataHeadMeta_id])
            presence[meta.fkDataHeadMeta_id] = '';
        });
        
        // const row = metaData.find(meta => presence[meta.fkDataHeadMeta_id] == meta.fkDataHeadMeta_id);

        // if(!row)
        //   presence[meta.fkDataHeadMeta_id] = '';          
      })

      setPresences(tempPresences)
      console.log('PRESENCESS TEAYO2');
      console.log(tempPresences);
    })
    .catch(e =>
    {
      console.log(e);
    })

  }, [dataContentMetaValuesVersion]) 

  const onSuccess = () => 
  {
    setPresencesVersion(presencesVersion+1)

    if(showUpdateForm)
      setShowUpdateForm(false)
  }

  const del = (presenceId) =>
  {
    const hide = message.loading('Menghapus...')

    fetch(`${process.env.API_BASE_URL}/api/presence/${presenceId}`, {
      method: 'DELETE',
      credentials: 'include'
    }).then(res =>
    {
      hide() 

      if(!res.ok)
        return Promise.reject(res.status)

      message.success('Terhapus')
      
      setPresencesVersion(prev => prev+1)      
    })
    .catch(e =>
    {
      console.error(e);

      message.error(`Gagal menghapus! Error: ${e}`)
    })
  }

  const totalPresent = presences.filter(presence => presence.status == 'hadir').length;
  const totalPermit = presences.filter(presence => presence.status.includes('izin')).length;

  return(
    <>
      {presenceHead && dataHeadMetas ?
        <>          
          <Row>
            <Col xl={8} md={8} xs={24} style={{alignItems: 'center', display: 'flex'}}>
              {presenceHead?.name} - Total kehadiran: {totalPresent} ({(totalPresent / dataContent?.length * 100).toFixed(2)}%) - Total izin: {totalPermit} ({(totalPermit / dataContent?.length * 100).toFixed(2)}%)
            </Col>
            <Col xl={16} md={16} xs={24} style={{textAlign: 'right'}}>
              <Row gutter={16} style={{display: 'flex', justifyContent: 'flex-end'}}>     
                <Col>
                  <a target='_blank' rel='noreferrer noopener' href={`${process.env.BASE_URL}/admin/presensi/${presenceHead.id}/qrcode`}>
                    <Button                             
                      icon={<LinkOutlined />} 
                      type="link"
                    >              
                      QR Code
                    </Button>
                  </a>
                </Col>

                <Col>
                  <a target='_blank' rel='noreferrer noopener' href={`${process.env.BASE_URL}/presensi/${presenceHead.id}/izin`}>
                    <Button                             
                      icon={<LinkOutlined />} 
                      type="link"
                    >              
                      Halaman izin publik
                    </Button>
                  </a>
                </Col>  

                <Col>
                  <a target='_blank' rel='noreferrer noopener' href={`${process.env.BASE_URL}/presensi/${presenceHead.id}`}>
                    <Button                             
                      icon={<LinkOutlined />} 
                      type="link"
                    >              
                      Halaman presensi publik
                    </Button>
                  </a>
                </Col>                

                <Col>
                  <CSVLink
                  data={(() =>
                  {
                    const presencesTemp = JSON.parse(JSON.stringify(presences));
                    console.log('ai');
                    console.log(dataHeadMetas);
                    presencesTemp.forEach(presence =>
                    {
                      delete presence['id'];
                      delete presence['fkDataContent_id'];

                      dataHeadMetas.forEach(meta =>
                      {
                        console.log('COKKK');
                        presence[meta.name] = presence[meta.id];                        
                        delete presence[meta.id];
                      })
                    })

                    return presencesTemp
                  })()}
                  filename={`Daftar Presensi ${presenceHead.name} ${Date()}`}>
                    <Button                             
                      icon={<DownloadOutlined />} 
                      type="link"
                    >              
                      Export CSV
                    </Button>
                  </CSVLink>
                </Col>                

                <Col>
                  <Button 
                    icon={<PlusCircleOutlined />} 
                    type="primary"
                    onClick={() => setShowPostForm(true)}
                  >
                    Presensi
                  </Button>       
                </Col>   
              </Row>
            </Col>
          </Row>
          <div style={{overflow: 'scroll'}}>
            <Table dataSource={presences} columns={columns} style={{marginTop: 24}}  />
          </div>
          <Modal 
            visible={showPostForm}
            onCancel={() => setShowPostForm(false)}
            footer={[]}
          >
            <PresenceAndRegistrationForm 
              where={props.where}
              presenceHeadId={presenceHead?.id} 
              onSuccess={onSuccess} 
              dataHeadId={presenceHead?.fkDataHead_id}
              resetFieldsOnSuccess={true}
              inputDesaKelompok={presenceHead?.inputDesaKelompok}
            />
          </Modal>        
        </>
        : <p>Loading...</p>}
    </>
  )
}