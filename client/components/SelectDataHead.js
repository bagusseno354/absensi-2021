import { Select } from "antd";
import { useEffect, useState } from "react";

export default function SelectDataHead(props)
{
  const [dataHeads, setDataHeads] = useState([])

  let fetchedDataHead = false

  useEffect(() =>
  {
    if(fetchedDataHead)
      return

    const abortController = new AbortController()
    const signal = abortController.signal

    fetchedDataHead = true

    fetch(`${process.env.API_BASE_URL}/api/dataHead`, {signal})
    .then(res =>
    {
      if(!res.ok)
        return Promise.reject(res.status)
      
      return res.json()      
    })    
    .then(json =>
    {              
      setDataHeads(json)

      if(json.length == 0)
        return;  
        
      })
    .catch(e =>
    {
      console.error(e);
    })

    return function cleanup()
    {
      abortController.abort()
    }
  }, [])

  const dataHeadsElements = dataHeads.map(dataHead => 
    <Select.Option key={dataHead.id} value={dataHead.id}>
      {dataHead.name}
    </Select.Option>)

  return(
    <Select {...props}>
      {dataHeadsElements}
    </Select>
  )
}