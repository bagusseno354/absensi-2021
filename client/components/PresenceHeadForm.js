import { Button, Form, Input, message, Switch } from 'antd';
import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import SelectDataHead from './SelectDataHead';
import moment from 'moment'

export default function PresenceHeadForm(props)
{
  const router = useRouter()
  const [isTimeLimited, setIsTimeLimited] = useState(false)
  const [form] = Form.useForm()
  let isSubmitting = false;
  let action = 'menambah'

  // default
  const {type = 'POST'} = props

  if(type == 'PATCH')
    action = 'mengubah'

  const onFinish = (values) =>
  {
    if(isSubmitting)
      return

    isSubmitting = true

    if(!values.isTimeLimited)
    {
      values.startDatetime = null
      values.endDatetime = null
    }

    delete values.isTimeLimited

    const hide = message.loading(`Mencoba ${action}...`);
    
    const url = type == 'POST' ? `${process.env.API_BASE_URL}/api/presenceHead` : `${process.env.API_BASE_URL}/api/presenceHead/${props.presenceHeadId}`
    
    fetch(url, 
    {
      method: type,
      headers: {
        'Content-Type': 'application/json',      
      },
      body: JSON.stringify(values),
      credentials: 'include',
    })
    .then(res => 
    {
      isSubmitting = false;
      hide();

      if(res.ok)
      {
        message.success(`Berhasil ${action}`)     
        
        if(props.resetFieldsOnSuccess)
          form.resetFields()

        if(props.onSuccess)
          props.onSuccess()
          
        if(props.redirectUrl)
        {
          message.loading('Redirecting...')                
          router.push(props.redirectUrl)
        }        
      }
      else return Promise.reject(res.status)
    })
    .catch(e =>
    {
      console.error(e);

      isSubmitting = false

      message.error(`Gagal ${action}! Error: ${e}`)
    })
  }

  useEffect(() =>
  {
    if(props.presenceHeadId == null)
      return form.resetFields()
    
    const abortController = new AbortController()
    const signal = abortController.signal

    fetch(`${process.env.API_BASE_URL}/api/presenceHead?where[id][__eq]=${props.presenceHeadId}`, {signal})
    .then(res =>
    {
      if(!res.ok)
        return Promise.reject(res.status)
      
      return res.json()
    })
    .then(json =>
    {
      const data = json[0]
        
      data.isTimeLimited = data.startDatetime && data.endDatetime ? 1 : 0    
      setIsTimeLimited(data.isTimeLimited)  
      data.startDatetime = moment(data.startDatetime).format('yyyy-MM-DDTHH:mm')
      data.endDatetime = moment(data.endDatetime).format('yyyy-MM-DDTHH:mm')
      form.setFieldsValue(data)
    })
    .catch(e =>
    {
      console.error(e);
    })

    return function cleanup()
    {
      abortController.abort()
    }

  }, [props.presenceHeadId])

  return(
    <Form
      form={form}
      wrapperCol={{ span: 14 }}
      layout="vertical"
      onFinish={onFinish}>

      <Form.Item
        label='Nama presensi'
        name='name'
        rules={[{ required: true, message: 'Wajib diisi' }]}
      >
        <Input /> 
      </Form.Item>

      <Form.Item
        label='Sumber data'
        name='fkDataHead_id'
        rules={[{ required: true, message: 'Wajib diisi' }]}
      >
        <SelectDataHead />
      </Form.Item>

      <Form.Item
        label='Perbolehkan duplikat'
        name='allowDuplicate'
        valuePropName='checked'
      >
        <Switch />
      </Form.Item>

      <Form.Item
        label='Input Desa dan Kelompok'
        name='inputDesaKelompok'
        valuePropName='checked'      
      >
        <Switch defaultChecked />
      </Form.Item>
      
      <Form.Item
        label='Bataskan waktu'
        name='isTimeLimited'
        valuePropName='checked'
      >
        <Switch onChange={value => setIsTimeLimited(value)} /> 
      </Form.Item>

      <Form.Item
        label='Waktu mulai'
        name='startDatetime'
        rules={isTimeLimited && [{ required: true, message: 'Wajib diisi' }]}
      >
        <Input disabled={!isTimeLimited} type='datetime-local' /> 
      </Form.Item>

      <Form.Item
        label='Waktu berakhir'
        name='endDatetime'         
        rules={isTimeLimited && [{ required: true, message: 'Wajib diisi' }]}
      >
        <Input disabled={!isTimeLimited} type='datetime-local' /> 
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit">
          {type == 'POST' && 'Buat presensi baru'}
          {type == 'PATCH' && 'Ubah presensi'}
        </Button>
      </Form.Item>
  </Form>
  )
}