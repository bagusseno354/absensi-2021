import { Select } from "antd";
import { useEffect, useState } from "react";

export default function SelectDesa(props)
{
  const [desas, setDesas] = useState([])

  let fetched = false

  useEffect(() =>
  {
    if(fetched)
      return

    fetched = true

    const abortController = new AbortController()
    const signal = abortController.signal

    fetch(`${process.env.API_BASE_URL}/api/desa`, {signal})
    .then(res =>
    {
      if(!res.ok)
        return Promise.reject(res.status)
      
      return res.json()      
    })    
    .then(json =>
    {              
      if(!Array.isArray(json) || json.length == 0)
        return;      
      
      setDesas(json)
    })
    .catch(e =>
    {
      console.error(e);
    })

    return function cleanup()
    {
      abortController.abort()
    }
  }, [])

  const options = desas
  .filter(desa => !props.onlyId || desa.id == props.onlyId)
  .filter(desa => desa.id != -1)
  .map(desa => ({
    label: desa.name,
    value: desa.id
  }))

  return(
    <Select       
      {...props}     
      placeholder='Pilih desa'      
      options={options}
    >      
    </Select>
  )
}