import { Select } from "antd";

export default function DataTable(props)
{
  return(
    <Select {...props}>
      <Select.Option value={1}>Januari</Select.Option>
      <Select.Option value={2}>Februari</Select.Option>
      <Select.Option value={3}>Maret</Select.Option>
      <Select.Option value={4}>April</Select.Option>
      <Select.Option value={5}>Mei</Select.Option>
      <Select.Option value={6}>Juni</Select.Option>
      <Select.Option value={7}>Juli</Select.Option>
      <Select.Option value={8}>Agustus</Select.Option>
      <Select.Option value={9}>September</Select.Option>
      <Select.Option value={10}>Oktober</Select.Option>
      <Select.Option value={11}>November</Select.Option>
      <Select.Option value={12}>Desember</Select.Option>
    </Select>
  )
}