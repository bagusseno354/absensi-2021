import { Button, Form, Input, Select, message, Space } from 'antd';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import axios from 'axios';

const { Option } = Select;

export default function DataHeadForm(props)
{
  const router = useRouter()
  const [form] = Form.useForm()
  const [metas, setMetas] = useState([]);
  let isSubmitting = false;
  let action = 'menambah'

  // default
  const {type = 'POST'} = props

  if(type == 'PATCH')
    action = 'mengubah'

  const onFinish = (values) =>
  {
    if(isSubmitting)
      return

    isSubmitting = true

    const hide = message.loading(`Mencoba ${action}...`);
    
    const url = (type == 'POST' ? `${process.env.API_BASE_URL}/api/dataHead` : `${process.env.API_BASE_URL}/api/dataHead/${props.dataHeadId}`)
    
    // submit datahead
    const request = (type == 'POST' ? axios.post(url, {name: values.name}, {withCredentials: true}) : axios.patch(url, {name: values.name}, {withCredentials: true}))

    request
    .then(res =>
    {
      message.success(`Berhasil ${action} data`);

      // get datahead id
      const dataHeadId = type == 'POST' ? res.data[0].id : props.dataHeadId;

      // submit dataheadmeta with fkDataHead_id
      const metas = values.metas;

      console.log(metas);

      if(metas)
      {
        metas.forEach((meta, metaIndex) =>
        {
          const { id, columnType, columnName } = meta;

          console.log('META NICH');
          console.log(meta);

          const columnData = {
            name: columnName,
            type: columnType,
            fkDataHead_id: dataHeadId
          }

          let url;
          let finalType = '';
          
          if(type == 'POST')
          {
            url = `${process.env.API_BASE_URL}/api/dataHeadMeta`;
            finalType = 'POST';
          }
          else
          {
            if(id)
            {
              url = `${process.env.API_BASE_URL}/api/dataHeadMeta/${id}`;
              finalType = 'PATCH';
            }
            else
            {
              url = `${process.env.API_BASE_URL}/api/dataHeadMeta`;
              finalType = 'POST';
            }
          }

          const request = (finalType == 'POST' ? axios.post(url, columnData, { withCredentials: true }) : axios.patch(url, columnData, { withCredentials: true }))

          request
          .then(res =>
          {
            message.success(`Berhasil ${action} kolom ${columnName}`);
            
            // get dataHeadMeta
            const dataHeadMetaId = id ? id : res.data[0].id;

            if(columnType == 'multipleChoice')
            {
              const { options } = meta;

              options.forEach(option =>
              {
                const { value, id } = option;

                const optionData = {
                  fkDataHeadMeta_id: dataHeadMetaId,
                  value
                }

                let url;
                let finalType;

                if(type == 'POST')
                {
                  url = `${process.env.API_BASE_URL}/api/dataHeadMetaValueOption`;
                  finalType = 'POST';
                }
                else
                {
                  if(id)
                  {
                    url = `${process.env.API_BASE_URL}/api/dataHeadMetaValueOption/${id}`;
                    finalType = 'PATCH';
                  }
                  else
                  {
                    url = `${process.env.API_BASE_URL}/api/dataHeadMetaValueOption`;
                    finalType = 'POST';
                  }
                }

                const request = (finalType == 'POST' ? axios.post(url, optionData, { withCredentials: true }) : axios.patch(url, optionData, { withCredentials: true }))  

                request
                .then(res =>
                {
                  message.success(`Berhasil ${action} opsi ${value}`);
                })            
                .catch(e =>
                {
                  message.error(`Gagal ${action} opsi ${value}!`);
                })
              });
            }

            if(metaIndex == metas.length - 1) 
            {
              isSubmitting = false;

              if(props.resetFieldsOnSuccess)
              form.resetFields()

              if(props.onSuccess)
                props.onSuccess()
                
              if(props.redirectUrl)
              {
                message.loading('Redirecting...')                
                router.push(props.redirectUrl)
              }
            }
          })
          .catch(e =>
          {
            message.error(`Gagal ${action} kolom ${columnName}!`);
            isSubmitting = false;
          })
        })
      }
      else
      {
        isSubmitting = false;

        if(props.resetFieldsOnSuccess)
        form.resetFields()

        if(props.onSuccess)
          props.onSuccess()
          
        if(props.redirectUrl)
        {
          message.loading('Redirecting...')                
          router.push(props.redirectUrl)
        }
      }
    })
    .catch(e =>
    {
      message.error(`Gagal ${action} data!`);
      console.error(e);
      isSubmitting = false;
    })

    // fetch(url, 
    // {
    //   method: type,
    //   headers: {
    //     'Content-Type': 'application/json',      
    //   },
    //   body: JSON.stringify(values),
    //   credentials: 'include',
    // })
    // .then(res => 
    // {
    //   isSubmitting = false;
    //   hide();

    //   if(res.ok)
    //   {
    //     message.success(`Berhasil ${action}`)     
        
    //     if(props.resetFieldsOnSuccess)
    //       form.resetFields()

    //     if(props.onSuccess)
    //       props.onSuccess()
          
    //     if(props.redirectUrl)
    //     {
    //       message.loading('Redirecting...')                
    //       router.push(props.redirectUrl)
    //     }        
    //   }
    //   else return Promise.reject(res.status)
    // })
    // .catch(e =>
    // {
    //   console.error(e);

    //   isSubmitting = false

    //   message.error(`Gagal ${action}! Error: ${e}`)
    // })
  }

  useEffect(() =>
  {
    if(props.dataHeadId == null)
      return form.resetFields()
    
    axios.get(`${process.env.API_BASE_URL}/api/dataHead?where[id][__eq]=${props.dataHeadId}&include[DataHeadMeta][include][DataHeadMetaValueOptions]`)
    .then(res =>
    {
      let data = res.data[0];
      const metas = [];

      data.DataHeadMeta.forEach((dataHeadMeta, index) =>
      {
        const { id, name: columnName, type: columnType, DataHeadMetaValueOptions } = dataHeadMeta;
        metas[index] = {
          id,
          columnName,
          columnType,
          options: DataHeadMetaValueOptions || []
        };
      })

      console.log('DATADATADATA');
      
      data = {...data, metas}
      console.log(data);

      form.setFieldsValue(data)
    })
    .catch(e =>
    {
      console.error(e);
    })

  }, [props.dataHeadId])

  return(
    <Form
      form={form}
      layout="vertical"
      onFinish={onFinish}>

      <Form.Item
          label='Nama data'
          name='name'
          rules={[{ required: true, message: 'Wajib diisi' }]}
      >
          <Input /> 
      </Form.Item>

      <Form.List name="metas">
      {(fields, { add, remove }) => (
          <Space direction='vertical' size='middle' className='w-full'>
            {fields.map((field, index) => (
              <Space key={field.key} size='small' direction='vertical' className='w-full border-2 p-4'>
                <Form.Item
                    label="Nama kolom"
                    name={[field.name, 'columnName']}
                    fieldKey={[field.fieldKey, 'columnName']}
                    rules={[{ required: true, message: 'Wajib isi nama kolom' }]}
                >
                    <Input /> 
                </Form.Item>
                <Form.Item
                    label="Tipe kolom"
                    name={[field.name, 'columnType']}
                    fieldKey={[field.fieldKey, 'columnType']}
                    rules={[{ required: true, message: 'Wajib isi tipe kolom' }]}
                >
                    <Select>
                      <Option value="number">Number</Option>
                      <Option value="text">Text</Option>
                      <Option value="multipleChoice">Multiple choice</Option>
                    </Select>
                </Form.Item>
                <Form.Item
                  noStyle
                  shouldUpdate={(prevValues, curValues) =>
                    prevValues.metas[index]?.columnType !== curValues.metas[index]?.columnType
                  }
                >
                  {() =>
                    form.getFieldsValue()['metas'][index]?.columnType == 'multipleChoice' && (
                      <Form.List name={[field.name, 'options']}>
                        {(fields, { add, remove }) => (
                            <Space direction='vertical' size='middle' className='w-full'>
                              {fields.map((field, index) => (
                                <Space key={field.key} size='small' direction='vertical' className='w-full border-2 border-green-400 p-4'>
                                  <Form.Item
                                    label='Value opsi'
                                    name={[field.name, 'value']}
                                    fieldKey={[field.fieldKey, 'value']}
                                    rules={[{ required: true, message: 'Wajib isi nama opsi' }]}
                                  >
                                      <Input /> 
                                  </Form.Item>
                                </Space>
                              ))}
                              <Form.Item>
                                <Button type="dashed" onClick={() => add()} block>
                                  Tambah opsi
                                </Button>
                              </Form.Item>
                            </Space>
                        )}
                        </Form.List>
                    )
                  }
                </Form.Item>
                <Form.Item>
                <Button type="dashed" onClick={() => remove(index)} block>
                  Hapus kolom
                </Button>
              </Form.Item>
              </Space>
            ))}
            <Form.Item>
              <Button type="dashed" onClick={() => add()} block>
                Tambah kolom
              </Button>
            </Form.Item>
          </Space>
      )}
      </Form.List>
      {
        metas.map(meta => (
          <div>
            
          </div>          
        ))
      }
      
      <Form.Item>
          <Button type="primary" htmlType="submit">
              {type == 'POST' && 'Buat data baru'}
              {type == 'PATCH' && 'Ubah data'}
          </Button>
      </Form.Item>
  </Form>
  )
}