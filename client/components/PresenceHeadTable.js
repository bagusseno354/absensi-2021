import { Table, Space, Modal, Button, Popconfirm, message, Row, Col } from "antd";
import { useEffect, useState } from "react";
import PresenceHeadForm from './PresenceHeadForm'
import { PlusCircleOutlined } from '@ant-design/icons'
import Title from 'antd/lib/typography/Title'
import Link from "next/link";
import moment from 'moment'

export default function PresenceHeadTable(props)
{
  const [presenceHeads, setPresenceHeads] = useState(null)
  const [columns, setColumns] = useState(null)
  const [dataVersion, setDataVersion] = useState(0)
  const [showPostForm, setShowPostForm] = useState(false)
  const [showUpdateForm, setShowUpdateForm] = useState(false)
  const [presenceHeadIdToUpdate, setPresenceHeadIdToUpdate] = useState(null)

  let fetched = false

  useEffect(() =>
  {
    if(fetched)
      return

    fetched = true

    fetch(props.url ? props.url : `${process.env.API_BASE_URL}/api/presenceHead?${props.where}&include[User][attributes]=name&include[User][include][Role][attributes]=id&include[User][include][User_kelompok][include][Kelompok]&include[User][include][User_desa][include][Desa]&include[DataHead][attributes]=name`)
    .then(res =>
    {
      if(res.status == 404)
        setPresenceHeads([])

      if(!res.ok)
        return Promise.reject(res.status)
      
      return res.json()      
    })    
    .then(json =>
    {              
      // convert all null user_desa & user_kelompok to empty object so that the table shows up
      json = json.map(({User: {User_desa, User_kelompok, ...restUser}, ...rest}) => ({User: {User_desa: User_desa ? User_desa : {}, User_kelompok: User_kelompok ? User_kelompok : {}, ...restUser}, ...rest}))

      // structure the table
      json = json.map(({fkDataHead_id, name, startDatetime = null, endDatetime, allowDuplicate, DataHead: {name: data}, User: {name: creatorName, Role: {id: roleId}, User_desa: {Desa: {name: desaName = ''} = {}} = {}, User_kelompok: {Kelompok: {name: kelompokName = ''} = {}} = {}}, fkUser_id, ...rest}, k) => ({no: k+1, oleh: roleId == 'pengurusKelompok' ? `Kelompok ${kelompokName}` : (roleId == 'pengurusDesa' ? `Desa ${desaName}` : 'Daerah'), name, creatorName, data, bolehDuplikat: allowDuplicate ? 'Ya' : 'Tidak', startDatetime: startDatetime ? moment(startDatetime).format('yyyy-MM-DD HH:mm') : '-', endDatetime: endDatetime ? moment(endDatetime).format('yyyy-MM-DD HH:mm') : '-', ...rest}))

      const {...columns} = json[0]

      // add key 
      json = json.map((v, k) => ({key: k, ...v}))
      
      setPresenceHeads(json)

      if(json.length == 0)
        return;

      const newColumns = Object.keys(columns).map(column => 
      { 
        if(column == 'name')
        {
          return ({
            title: column.charAt(0).toUpperCase() + column.slice(1),
            dataIndex: column,
            key: column,                    
            render: (text, record) =>
            (
              <Link href={`/admin/presensi/${record.id}`}>{record.name}</Link> 
            )
          })
        }

        return ({
          title: column.charAt(0).toUpperCase() + column.slice(1),
          dataIndex: column,
          key: column,                
        })
      })

      if(props.enableActions)
        newColumns.push({
          title: 'Action',
          dataIndex: 'action',
          key: 'action',
          render: (text, record) =>
          (
            <Space size="middle">
              <a onClick={() => edit(record.id)}>Edit</a>
              <Popconfirm title="Yakin menghapus daftar presensi ini? Seluruh data presensi orang-orang yang sudah presensi akan terhapus!" placement="leftTop" onConfirm={() => del(record.id)} okText="Yes" cancelText="No">
                <a>Delete</a>
              </Popconfirm>
            </Space>
          )
        })

      setColumns(newColumns)
    })
    .catch(e =>
    {
      console.error(e);
    })
  }, [dataVersion])

  const onSuccess = () => 
  {
    setDataVersion(dataVersion+1)

    if(showUpdateForm)
      setShowUpdateForm(false)
  }

  const edit = (dataHeadId) =>
  {
    setPresenceHeadIdToUpdate(dataHeadId)
    setShowUpdateForm(true)
  }

  const del = (dataHeadId) =>
  {
    const hide = message.loading('Menghapus...')

    fetch(`${process.env.API_BASE_URL}/api/presenceHead/${dataHeadId}`, {
      method: 'DELETE',
      credentials: 'include'
    }).then(res =>
    {
      hide() 

      if(!res.ok)
        return Promise.reject(res.status)

      message.success('Terhapus')
      
      setDataVersion(prev => prev+1)

    })
    .catch(e =>
    {
      message.error(`Gagal menghapus! Error: ${e}`)

      console.error(e);
    })
  }

  return(
    <>
      <Row>
        <Col span={12} style={{alignItems: 'center', display: 'flex'}}>
          Daftar presensi
        </Col>
        <Col span={12} style={{textAlign: 'right'}}>
          <Button 
            icon={<PlusCircleOutlined />} 
            type="primary"
            onClick={() => setShowPostForm(true)}
          >
            Tambah presensi
          </Button>
        </Col>
      </Row>
      <div style={{overflow: 'scroll'}}>
        <Table dataSource={presenceHeads} columns={columns} style={{marginTop: 24}} enableActions={true} />
      </div>
      <Modal 
        visible={showPostForm}
        onCancel={() => setShowPostForm(false)}
        footer={[]}
      >
        <Title level={2}>
          Tambah presensi
        </Title>
        <PresenceHeadForm onSuccess={onSuccess} type='POST' resetFieldsOnSuccess={true} />
      </Modal>
      <Modal 
        visible={showUpdateForm}
        onCancel={() => setShowUpdateForm(false)}
        footer={[]}
      >
        <Title level={2}>
          Ubah presensi
        </Title>
        <PresenceHeadForm onSuccess={onSuccess} type='PATCH' presenceHeadId={presenceHeadIdToUpdate} />
      </Modal>
    </>
  )
}