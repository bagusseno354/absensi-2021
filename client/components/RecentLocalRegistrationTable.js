 import { Table } from 'antd'
import { useEffect, useState } from 'react'
import moment from 'moment'

export default function RecentLocalRegistrationTable(props)
{
  const [columns, setColumns] = useState(null)
  const [recentLocalRegistrations, setRecentLocalRegistrations] = useState(null)
  const { recentLocalDataContentIds } = props

  useEffect(() =>
  {
    if(!recentLocalDataContentIds)
      return

    console.log(('recent'));
    console.log(recentLocalDataContentIds);

    fetch(`${process.env.API_BASE_URL}/api/dataContent?where[id][__in]=${recentLocalDataContentIds.join(',')}&include[Kelompok][include][Desa][attributes]=name&order=id,DESC`)
    .then(res =>
    {
      if(!res.ok)
        return Promise.reject(res.status)
      
      return res.json()      
    })    
    .then(json =>
    {            
      json = json.map(({nama, Kelompok: {name: Kelompok, Desa: {name: Desa}}}, k) => ({no: k+1, nama, Desa, Kelompok}))
      
      console.log(json);
      setRecentLocalRegistrations(json)

      if(json.length == 0)
        return;

      const newColumns = Object.keys(json[0]).map(column => 
      ({
        title: column.charAt(0).toUpperCase() + column.slice(1),
        dataIndex: column,
        key: column
      }))

      setColumns(newColumns)
    })
    .catch(e =>
    {
      console.error(e);
    })
  }, [recentLocalDataContentIds])

  return(
    <div style={{overflow: 'scroll'}}>
      <Table columns={columns} dataSource={recentLocalRegistrations} />
    </div>
  )
}