 import { Table } from 'antd'
import { useEffect, useState } from 'react'
import moment from 'moment'

export default function RecentLocalPresenceTable(props)
{
  const [columns, setColumns] = useState(null)
  const [recentLocalPresences, setRecentLocalPresences] = useState(null)
  const { recentLocalPresenceIds } = props

  useEffect(() =>
  {
    if(!recentLocalPresenceIds)
      return

    console.log(('recent'));
    console.log(recentLocalPresenceIds);

    fetch(`${process.env.API_BASE_URL}/api/presence?where[id][__in]=${recentLocalPresenceIds.join(',')}&include[DataContent][include][Kelompok][include][Desa][attributes]=name&order=id,DESC`)
    .then(res =>
    {
      if(!res.ok)
        return Promise.reject(res.status)
      
      return res.json()      
    })    
    .then(json =>
    {            
      json = json.map(({DataContent: {nama, Kelompok: {name: Kelompok, Desa: {name: Desa}}}, time}, k) => ({no: k+1, nama, Desa, Kelompok, waktuPresensi: moment(time).format('yyyy-MM-DD (hh:mm:ss)')}))
      
      console.log(json);
      setRecentLocalPresences(json)

      if(json.length == 0)
        return;

      const newColumns = Object.keys(json[0]).map(column => 
      ({
        title: column.charAt(0).toUpperCase() + column.slice(1),
        dataIndex: column,
        key: column
      }))

      setColumns(newColumns)
    })
    .catch(e =>
    {
      console.error(e);
    })
  }, [recentLocalPresenceIds])

  return(
    <div style={{overflow: 'scroll'}}>
      <Table columns={columns} dataSource={recentLocalPresences} />
    </div>
  )
}