import { Button, Col, Form, Input, message, Row, Select, Space } from 'antd'
import SelectRole from '../SelectRole'
import SelectDesa from '../SelectDesa'
import SelectKelompok from '../SelectKelompok'
import PropTypes from 'prop-types'
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { useEffect, useState } from 'react';

export default function UserForm(props)
{
  const { type, form, onFinish, ...restProps } = props

  // to be passed to SelectKelompok
  const [desaId, setDesaId] = useState(props.desaId)
  const [roleId, setRoleId] = useState(props.roleId)

  // pass desa id prop changes to the state
  // as the useState only runs once, if the prop changes, the state won't change
  useEffect(() =>
  {
    setDesaId(props.desaId)
  }, [props.desaId])

  useEffect(() =>
  {
    setRoleId(props.roleId)
  }, [props.roleId])

  // clear kelompok when desa changes
  const onDesaChange = (value) =>
  {
    setDesaId(value)
    form.setFieldsValue({fkKelompok_id: null})
  }

  return(
    <Form
      {...restProps}
      form={form}
      layout="vertical"
      onFinish={onFinish}>

      <Form.Item
          label='Nama user'
          name='name'
          rules={[{ required: true, message: 'Wajib diisi' }]}
      >
          <Input /> 
      </Form.Item>

      <Form.Item
          label='Email'
          name='email'
          rules={[{ required: true, message: 'Wajib diisi' }]}
      >
          <Input /> 
      </Form.Item>
      
      <Form.Item
          label={(type == 'PATCH' && 'New password') || (type == 'POST' && 'Password')}
          name='password'     
          extra={type == 'PATCH' &&  'Kosongkan jika tidak ingin mengubah.'}     
      >
          <Input type="password" /> 
      </Form.Item>

      <Form.Item
          label='Role'
          name='fkRole_id'
          rules={[{ required: true, message: 'Wajib diisi' }]}
      >
          <SelectRole onChange={(value) => {setRoleId(value)}} /> 
      </Form.Item>

      {(roleId == 'pengurusDesa' || roleId == 'pengurusKelompok') 
      &&
      <Form.Item
          label='Desa'
          name='fkDesa_id'        
          rules={[{ required: true, message: 'Wajib diisi' }]}
      >
          <SelectDesa value={desaId} onChange={onDesaChange} /> 
      </Form.Item>    
      }

      {roleId == 'pengurusKelompok' 
      &&
      <Form.Item                   
          label='Kelompok'
          name='fkKelompok_id'
          rules={[{ required: true, message: 'Wajib diisi' }]}
      >
          <SelectKelompok desaId={desaId} /> 
      </Form.Item>    
      }

      <Form.Item>
          <Button type="primary" htmlType="submit">
            {type == 'PATCH' && 'Ubah user'}
            {type == 'POST' && 'Tambah user'}
          </Button>
      </Form.Item>
  </Form>
  )
}

UserForm.propTypes = {
    type: PropTypes.string.isRequired,
    form: PropTypes.any.isRequired,
    onFinish: PropTypes.func
}