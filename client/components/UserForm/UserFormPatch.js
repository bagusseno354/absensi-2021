import { Button, Form, Input, message } from 'antd';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import UserForm from './UserForm'

export default function UserFormPatch(props)
{
  const router = useRouter()
  const [form] = Form.useForm()
  let isSubmitting = false;
  const [roleId, setRoleId] = useState(null)
  const [desaId, setDesaId] = useState(null)
  const [kelompokId, setKelompokId] = useState(null)

  const onSuccess = () =>
  {
    message.success(`Berhasil mengubah`)     
      
    if(props.resetFieldsOnSuccess)
      form.resetFields()

    if(props.onSuccess)
      props.onSuccess()
      
    if(props.redirectUrl)
    {
      message.loading('Redirecting...')                
      router.push(props.redirectUrl)
    }    
  }

  const onFinish = (values) =>
  {
    if(isSubmitting)
      return

    isSubmitting = true

    const hide = message.loading(`Mencoba mengubah...`);
    
    const url = `${process.env.API_BASE_URL}/api/user/${props.userId}`
    
    const {fkDesa_id, fkKelompok_id, ...valuesToSend} = values

    // remove password if empty
    if(!valuesToSend.password)
      delete valuesToSend.password

    fetch(url, 
    {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',      
      },
      body: JSON.stringify(valuesToSend),
      credentials: 'include',
    })
    .then(res => 
    {
      isSubmitting = false;
      hide();

      if(!res.ok)
        return Promise.reject(res.status)
      
      if(values.fkRole_id == 'pengurusKelompok')
      {        
        fetch(`${process.env.API_BASE_URL}/api/user/${props.userId}/kelompok`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',      
          },
          body: JSON.stringify({fkKelompok_id}),
          credentials: 'include',
        }).then(res =>
        {
          if(!res.ok)
            return Promise.reject(res.status)        

          onSuccess()  
        }) 
        .catch(e =>
        {
          console.error(e);
    
          isSubmitting = false
    
          message.error(`Gagal mengubah! Error: ${e}`)
        })               
      }
      else if(values.fkRole_id == 'pengurusDesa')
      {        
        fetch(`${process.env.API_BASE_URL}/api/user/${props.userId}/desa`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',      
          },
          body: JSON.stringify({fkDesa_id}),
          credentials: 'include',
        }).then(res =>
        {
          if(!res.ok)
            return Promise.reject(res.status)        

          onSuccess()  
        }) 
        .catch(e =>
        {
          console.error(e);
    
          isSubmitting = false
    
          message.error(`Gagal mengubah! Error: ${e}`)
        })               
      }
      else
      {
        onSuccess()  
      }
    })
    .catch(e =>
    {
      console.error(e);

      isSubmitting = false

      message.error(`Gagal mengubah! Error: ${e}`)
    })
  }

  useEffect(() =>
  {
    if(props.userId == null)
      return

    form.resetFields();
    
    fetch(`${process.env.API_BASE_URL}/api/user?where[id][__eq]=${props.userId}&include[Role][attributes]=id,name&include[User_desa][include][Desa]&include[User_kelompok][include][Kelompok][include][Desa]`)
    .then(res =>
    {
      if(!res.ok)
        return Promise.reject(res.status)
      
      return res.json()
    })
    .then(json =>
    {
      const desaId = json[0].User_desa?.Desa.id || json[0].User_kelompok?.Kelompok.Desa.id

      setRoleId(json[0].fkRole_id)      
      setDesaId(desaId)      
        
      json[0].fkDesa_id = desaId;
      json[0].fkKelompok_id = json[0].User_kelompok?.Kelompok.id;

      form.setFieldsValue(json[0])
    })
    .catch(e =>
    {
      console.error(e);
    })

  }, [props.callTrigger])

  return(
    <UserForm form={form} onFinish={onFinish} roleId={roleId} desaId={desaId} type='PATCH' />
  )
}