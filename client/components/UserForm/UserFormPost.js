import { Button, Form, Input, message } from 'antd';
import { useEffect } from 'react';
import { useRouter } from 'next/router';
import UserForm from './UserForm'

export default function UserFormPost(props)
{
  const router = useRouter()
  const [form] = Form.useForm()
  let isSubmitting = false;

  const onSuccess = () =>
  {
    message.success(`Berhasil menambah`)     
      
    if(props.resetFieldsOnSuccess)
      form.resetFields()

    if(props.onSuccess)
      props.onSuccess()
      
    if(props.redirectUrl)
    {
      message.loading('Redirecting...')                
      router.push(props.redirectUrl)
    }    
  }

  const onFinish = (values) =>
  {
    if(isSubmitting)
      return

    isSubmitting = true
    const hide = message.loading(`Mencoba menambah...`)

    const url = `${process.env.API_BASE_URL}/api/user`
    
    let {fkDesa_id, fkKelompok_id, ...valuesToSend} = values

    fetch(url, 
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',      
      },
      body: JSON.stringify(valuesToSend),
      credentials: 'include',
    })
    .then(res => 
    {
      isSubmitting = false;
      hide();

      if(!res.ok)
        return Promise.reject(res.status)
      
      return res.json()                
    })
    .then(json =>
    {
      const userId = json[0].id

      if(values.fkRole_id == 'pengurusKelompok')
      {        
        fetch(`${process.env.API_BASE_URL}/api/user/${userId}/kelompok`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',      
          },
          body: JSON.stringify({fkKelompok_id}),
          credentials: 'include',
        }).then(res =>
        {
          if(!res.ok)
            return Promise.reject(res.status)        

          onSuccess()  
        }) 
        .catch(e =>
        {
          console.error(e);
    
          isSubmitting = false
    
          message.error(`Gagal menambah! Error: ${e}`)
        })               
      }     
      else if(values.fkRole_id == 'pengurusDesa')
      {        
        fetch(`${process.env.API_BASE_URL}/api/user/${userId}/desa`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',      
          },
          body: JSON.stringify({fkDesa_id}),
          credentials: 'include',
        }).then(res =>
        {
          if(!res.ok)
            return Promise.reject(res.status)        

          onSuccess()  
        }) 
        .catch(e =>
        {
          console.error(e);
    
          isSubmitting = false
    
          message.error(`Gagal menambah! Error: ${e}`)
        })               
      }  
      else
      {
        onSuccess()  
      }     
    })
    .catch(e =>
    {
      console.error(e);

      isSubmitting = false

      message.error(`Gagal menambah! Error: ${e}`)
    })
  }

  useEffect(() =>
  {
    if(props.userId == null)
      return form.resetFields()
    
    fetch(`${process.env.API_BASE_URL}/api/user?where[id][__eq]=${props.userId}&include[Role][attributes]=id,name`)
    .then(res =>
    {
      if(!res.ok)
        return Promise.reject(res.status)
      
      return res.json()
    })
    .then(json =>
    {
      if(json[0])
        json[0].roles = json[0].Roles.map(role => role.id)

      form.setFieldsValue(json[0])
    })
    .catch(e =>
    {
      console.error(e);
    })

  }, [props.callTrigger])

  return(
    <UserForm form={form} onFinish={onFinish} type='POST' />
  )
}