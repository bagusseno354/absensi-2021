import { Layout, Breadcrumb, Typography, Space } from 'antd';
import Sidebar from './Sidebar/Sidebar'
import { ArrowLeftOutlined } from '@ant-design/icons'
import Link from 'next/link';

const { Content, Footer } = Layout;
const { Title } = Typography;

export default function UserLayout(props)
{
  const breadcrumbs = props.breadcrumbs ? props.breadcrumbs.map((breadcrumb, k) => 
    <Breadcrumb.Item key={k}><Link href={breadcrumb.url}><a>{breadcrumb.name}</a></Link></Breadcrumb.Item>) : <Breadcrumb.Item>{props.title}</Breadcrumb.Item>

  return(
    <Layout style={{ minHeight: '100vh' }}>
        <Sidebar user={props.user} />
        <Layout className="site-layout">
          <Content style={{ margin: '0 16px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
              {breadcrumbs}
            </Breadcrumb>
            <div className="site-layout-background" style={{ padding: 24, minHeight: 360, background: 'white' }}>
              <Title>
                <Space size='large'>
                  {props.previousUrl && 
                  <Link href={props.previousUrl}>
                    <ArrowLeftOutlined />
                  </Link>
                  }
                  {props.title}
                </Space>
              </Title>
              {props.children}
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>Absensi 2021 - Info ABCD</Footer>
        </Layout>
      </Layout>
  )
}