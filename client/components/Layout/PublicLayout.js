import { Layout, Typography } from 'antd';

const { Content, Footer } = Layout;
const { Title } = Typography;

export default function UserLayout(props)
{  
  return(
    <Layout style={{ minHeight: '100vh' }}>
      <Layout className="site-layout">
        <Content style={{ margin: '0 16px', display: 'flex', justifyContent: 'center' }} >            
          <div className="site-layout-background" style={{ maxWidth: 1200, width: '100%', padding: 24, minHeight: 360, background: 'white' }}>
            <Title>             
              {props.title}
            </Title>
            <div style={{ marginTop: 64}}>
              {props.children}
            </div>
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>Absensi 2021 - Info ABCD</Footer>
      </Layout>
    </Layout>
  )
}