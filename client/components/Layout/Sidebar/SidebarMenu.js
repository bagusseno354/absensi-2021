
import { Menu,  } from 'antd';
import {
    TeamOutlined,
    HomeOutlined,
    CheckCircleOutlined,
    UserOutlined
  } from '@ant-design/icons';
import Link from 'next/link';
import { useRouter } from 'next/router';

export default function SiderbarMenu(props)
{
    const router = useRouter();

    const rootPaths = [
        '/admin/dashboard',
        '/admin/data',
        '/admin/presensi',
        '/admin/user',
        '/admin/desa'
    ]
    
    let pathName = ''

    rootPaths.forEach(rootPath =>
    {        
        if(router.pathname.startsWith(rootPath))
            pathName = rootPath
    })

    console.log(props.user?.Role);

    return(
        <Menu theme="dark" defaultSelectedKeys={[pathName]} mode="inline">            
            <Menu.Item key='/admin/dashboard' icon={<HomeOutlined />}>
                <Link href='/admin/dashboard'>Dashboard</Link> 
            </Menu.Item>
            <Menu.Item key='/admin/data' icon={<TeamOutlined />}>
                <Link href='/admin/data'>Data</Link> 
            </Menu.Item>
            <Menu.Item key={'/admin/presensi'} icon={<CheckCircleOutlined />}>
                <Link href='/admin/presensi'>Presensi</Link> 
            </Menu.Item>
            {(props.user?.Role.id == 'superadmin')
            &&        
            <>
                <Menu.Item key={'/admin/user'} icon={<UserOutlined />}>
                    <Link href='/admin/user'>User</Link> 
                </Menu.Item>
                <Menu.Item key={'/admin/desa'} icon={<UserOutlined />}>
                    <Link href='/admin/desa'>{`Desa & Kelompok`}</Link> 
                </Menu.Item>
            </>
            }
        </Menu>
    )
}