import { Layout, Avatar, Space, Button, message } from 'antd';
import React, { useState, useEffect } from 'react';
import { UserOutlined } from '@ant-design/icons';
const { Sider } = Layout;
import { useRouter } from 'next/router';

import SidebarLogo from './SidebarLogo';
import SidebarMenu from './SidebarMenu';

export default function Sidebar(props)
{
    const [collapsible, setCollapsible] = useState(false)
    const [collapsed, setCollapsed] = useState(false)
    const router = useRouter()

    const onCollapse = collapsed => {
        setCollapsed(collapsed)
    };

    if(typeof window !== 'undefined')
    {
        useEffect(() =>
        {
            if(window.innerWidth < 480)
                setCollapsible(true)
            else
            {
                setCollapsible(false)
                setCollapsed(false)
            }
        }, [window.innerWidth])
    }

    const signOut = () =>
    {
        fetch(`${process.env.API_BASE_URL}/api/methods/auth/signOut`, {
            method: 'POST',
            credentials: 'include'
        })
        .then(res =>
        {
            if(!res.ok)
                return Promise.reject(res.status)

            router.push('/admin/login')
        })
        .catch(e =>
        {
            message.error(`Gagal sign out! Error: ${e}`)

            console.log(e);
        })

        console.log(props.user);
    }

    return(
        <Sider style={{position: collapsible ? 'fixed' : '', zIndex: 10, height: collapsible ? '100%' : ''}} breakpoint='xs' collapsible={collapsible} defaultCollapsed={false} collapsed={collapsed} onCollapse={onCollapse} collapsedWidth={0}>
            <Space size='middle' direction='vertical' style={{width: '100%'}} >
                <SidebarLogo />
                <div style={{display: 'flex', justifyContent: 'center', padding: 0 }}>
                    <Avatar size={48} icon={<UserOutlined />} />
                </div>
                <div style={{display: 'flex', justifyContent: 'center', color: 'white'}}>
                    {props.user?.name}
                </div>
                <div style={{display: 'flex', justifyContent: 'center', color: 'white', padding: '0 24px', textAlign: 'center'}}>
                    {props.user?.Role && props.user?.Role.name} {props.user?.Role.id == 'pengurusDesa' && props.user.User_desa?.Desa.name} {props.user?.Role.id == 'pengurusKelompok' && props.user.User_kelompok?.Kelompok.name}
                </div>              
                <div style={{display: 'flex', justifyContent: 'center', color: 'white'}}>
                    <Button type='link' onClick={signOut}>Sign out</Button>
                </div>
                <SidebarMenu user={props.user} />
            </Space>
        </Sider>
    )
}