export default function SidebarLogo(props)
{
    return(
        <div style={{color: 'white', padding: 24, textAlign: props.collapsed ? 'center' : ''}}>
            {process.env.APP_NAME}            
        </div>
        
    )
}