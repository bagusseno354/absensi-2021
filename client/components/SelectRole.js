import { Select } from "antd";
import { useEffect, useState } from "react";

export default function SelectRole(props)
{
  const [roles, setRoles] = useState([])

  let fetched = false

  useEffect(() =>
  {
    if(fetched)
      return

    fetched = true

    fetch(`${process.env.API_BASE_URL}/api/role`)
    .then(res =>
    {
      if(!res.ok)
        return Promise.reject(res.status)
      
      return res.json()      
    })    
    .then(json =>
    {              
      setRoles(json)

      if(json.length == 0)
        return;      
    })
    .catch(e =>
    {
      console.error(e);
    })
  }, [])

  const options = roles.map(role => 
    ({
      label: role.name,
      value: role.id
    }))

  return(
    <Select 
      {...props}
      options={options}
    >      
    </Select>
  )
}