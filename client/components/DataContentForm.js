import { Button, Form, Input, message, Select } from 'antd';
import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import SelectDate from './SelectDate';
import SelectMonth from './SelectMonth';
import SelectDesa from './SelectDesa';
import SelectKelompok from './SelectKelompok';
import moment from 'moment';
import axios from 'axios';
import { data } from 'autoprefixer';

const { Option } = Select;

export default function DataContentForm({type = 'POST', presenceStatus = null, resetFieldsOnSuccess, onSuccess, onSuccessRegistrate, redirectUrl, onlyDesaId, onlyKelompokId, presenceHeadId, dataHeadId, dataContentId, inputDesaKelompok = true})
{
  const router = useRouter()
  const [desaId, setDesaId] = useState(null)
  const [birthMonth, setBirthMonth] = useState(null)
  const [birthYear, setBirthYear] = useState(null)
  const [dataHeadMetas, setDataHeadMetas] = useState([]);
  const [form] = Form.useForm()
  const [isSubmitting, setIsSubmitting] = useState({status: false, values: null, trigger: 0});

  let action = 'registrasi';

  if(type == 'PATCH')
    action = 'mengubah'

  const refershForm = () =>
  {
    setIsSubmitting(prev => ({...prev, status: false}));
            
    if(resetFieldsOnSuccess)
      form.resetFields()      
  }

  // get dataheadmeta
  useEffect(() =>
  {
    axios.get(`${process.env.API_BASE_URL}/api/dataHeadMeta?where[fkDataHead_id][__eq]=${dataHeadId}&include[DataHeadMetaValueOptions]=true`)
    .then(res =>
    {
      setDataHeadMetas(res.data);
    });

  }, [dataHeadId]);

  // publish
  const onFinish = (values) =>
  {
    setIsSubmitting(prev => ({status: true, values, trigger: prev.trigger + 1}));
  }

  useEffect(async () =>
  {
    if(!isSubmitting.status)
      return

    // if presence is set, check the time first
    if(presenceStatus != null)
    {
      let presenceRes = await fetch(`${process.env.API_BASE_URL}/api/presenceHead/${presenceHeadId}`)
      
      if(presenceRes.ok)
      {
        let presenceJson = await presenceRes.json();

        let {startDatetime, endDatetime} = presenceJson;

        // if time is limited
        if(startDatetime && endDatetime)
        {
          let currDate = moment();
          startDatetime = moment(startDatetime);
          endDatetime = moment(endDatetime);
  
          if(!(currDate <= endDatetime && currDate >= startDatetime))
          {
            message.error(`Gagal presensi! Anda presensi di luar waktu presensi.`);
            return;
          }
        }
      }
    }

    isSubmitting.values.fkDataHead_id = dataHeadId;

    const hide = message.loading(`Mencoba ${action}...`);
    
    const url = type == 'POST' ? `${process.env.API_BASE_URL}/api/dataContent` : `${process.env.API_BASE_URL}/api/dataContent/${dataContentId}`

    // submit datacontent first
    const { nama, fkKelompok_id, fkDataHead_id, reason } = isSubmitting.values;

    const request = (type == 'POST' ? axios.post(url, { nama, fkKelompok_id: inputDesaKelompok ? fkKelompok_id : -1, fkDataHead_id }, { withCredentials: true }) : axios.patch(url, { nama, fkKelompok_id, fkDataHead_id }, { withCredentials: true }));

    request
    .then(res =>
    {
      message.success(`Berhasil ${action} data`);

      // get datacontent id
      dataContentId = (type == 'POST' ? res.data[0].id : dataContentId);          

      console.log('RES DATA');
      console.log(res.data);
      
      if(onSuccessRegistrate)
        onSuccessRegistrate(res.data);

      // If presence is set
      if(presenceStatus != null)
      {
        axios.post(`${process.env.API_BASE_URL}/api/presence`, 
        {
          fkDataContent_id: res.data[0].id,
          fkPresenceHead_id: presenceHeadId,
          status: presenceStatus == 'present' ? 'hadir' : 'izin',
          reason: presenceStatus == 'permit' ? reason : null
        },
        {
          withCredentials: true,        
        })
        .then(resPresence => 
        {
          setIsSubmitting(prev => ({...prev, status: false}));
          hide();
  
          message.success(`Berhasil ${presenceStatus == 'permit' ? 'izin' : 'presensi'}`)     
          
          if(resetFieldsOnSuccess)
            form.resetFields()       
          
          if(onSuccess)
            onSuccess(resPresence.data)
        })
        .catch(e =>
        {
          console.error(e);
  
          setIsSubmitting(prev => ({...prev, status: false}));
  
          message.error(`Gagal presensi! Error: ${e}`)
        })    
      }

      if(dataHeadMetas.length == 0)
      {         
        if(onSuccess)
            onSuccess();

        refershForm();
        return;
      }
      
      // submit metas
      dataHeadMetas.forEach((dataHeadMeta, dataHeadMetaIndex) =>
      {
        let value;

        if(isSubmitting.values[dataHeadMeta.id] != 'Isi lainnya...')
          value = isSubmitting.values[dataHeadMeta.id];
        else 
          value = isSubmitting.values[`${dataHeadMeta.id}.other`];

        const dataContentMetaValueData = {
          value
        }

        const request = axios.put(`${process.env.API_BASE_URL}/api/dataContentMetaValue/${dataContentId}/${dataHeadMeta.id}`, dataContentMetaValueData, { withCredentials: true })

        request
        .catch(e =>
          {
            alert(e)
            message.error(`Gagal input ${dataHeadMeta.name}`);
            setIsSubmitting(prev => ({...prev, status: false}));
          })
        .then(resMeta =>
        {
          message.success(`Berhasil ${action} ${dataHeadMeta.name}`);       
          
          if(onSuccess)
            onSuccess();
          
          if(dataHeadMetaIndex == dataHeadMetas.length - 1)
          {
            refershForm();
              
            if(redirectUrl)
            {
              message.loading('Redirecting...')                
              router.push(redirectUrl)
            } 
          }
        })        
      })

    })
    .catch(e =>
    {
      message.error('Gagal registrasi data!');
      setIsSubmitting(prev => ({...prev, status: false}));
    })

  }, [isSubmitting]);

  // get data content
  useEffect(() =>
  {
    if(dataContentId == null)
      return form.resetFields()
  
    axios.get(`${process.env.API_BASE_URL}/api/dataContent?where[id][__eq]=${dataContentId}&include[DataContentMetaValues]&include[Kelompok]`)
    .then(res =>
    {
      let data = res.data[0];

      data = {...data, fkDesa_id: data.Kelompok.fkDesa_id}
      setDesaId(data.Kelompok.fkDesa_id);

      const dataContentMetaValues = {};

      data.DataContentMetaValues.forEach(dataContentMetaValue =>
      {
        const { fkDataHeadMeta_id, value } = dataContentMetaValue;
        dataContentMetaValues[fkDataHeadMeta_id] = value;
      })

      data = {...data, ...dataContentMetaValues}

      console.log('OOOOOO');
      console.log(data);
      form.setFieldsValue(data)
    })
    .catch(e =>
    {
      console.error(e);
    })
    
  }, [dataContentId])

  return(
    <Form
      form={form}
      layout="vertical"
      onFinish={onFinish}>

      <Form.Item
          label='Nama'
          name='nama'
          rules={[{ required: true, message: 'Wajib diisi' }]}
      >
          <Input /> 
      </Form.Item>

      <Form.Item
          label='Desa'
          name='fkDesa_id'          
          hidden={!inputDesaKelompok}
          initialValue={!inputDesaKelompok ? -1 : null}
          rules={[{ required: true, message: 'Wajib diisi' }]}
      >
          <SelectDesa           
            onlyId={onlyDesaId}
            onSelect={desaId => setDesaId(desaId)}            
          />
      </Form.Item>

      <Form.Item
          label='Kelompok'
          name='fkKelompok_id'          
          hidden={!inputDesaKelompok}
          initialValue={!inputDesaKelompok ? -1 : null}
          rules={[{ required: true, message: 'Wajib diisi' }]}
      >
        <SelectKelompok onlyId={onlyKelompokId} desaId={desaId} />
      </Form.Item>

      {
        presenceStatus == 'permit' &&
        <Form.Item
          label='Alasan isin'
          name='reason'          
          rules={[{ required: true, message: 'Wajib diisi' }]}
        >
          <Input /> 
        </Form.Item>
      }

      {
        dataHeadMetas.map(dataHeadMeta => 
        <>
          <Form.Item
            label={dataHeadMeta.name}
            name={dataHeadMeta.id}
            validateFirst={true}
            initialValue={[]}  
            rules={[{ required: true, message: 'Wajib isi' }]}
            >
              {
                (dataHeadMeta.type == 'number' || dataHeadMeta.type == 'text') && <Input type={dataHeadMeta.type} />
                ||            
                (dataHeadMeta.type == 'multipleChoice' && dataHeadMeta.DataHeadMetaValueOptions) &&
                <Select>
                  {
                    dataHeadMeta.DataHeadMetaValueOptions.map(DataHeadMetaValueOption =>
                      <Option value={DataHeadMetaValueOption.value}>
                        {DataHeadMetaValueOption.value}
                      </Option>
                    )
                  }
                </Select>
              }
            </Form.Item>
            <Form.Item
              noStyle
              shouldUpdate={(prevValues, curValues) =>
                prevValues[dataHeadMeta.id] !== curValues[dataHeadMeta.id]
              }
            >
            {() =>
              form.getFieldValue(dataHeadMeta.id) == 'Isi lainnya...' &&
              <Form.Item
                name={`${dataHeadMeta.id}.other`}
                validateFirst={true}
                initialValue={[]}  
                rules={[{ required: true, message: 'Wajib isi' }]}
              >
                <Input />
              </Form.Item>
            }
            </Form.Item>
          </>
        )        
      }

      <Form.Item>
          <Button type="primary" htmlType="submit" disabled={isSubmitting.status}>
              {type == 'POST' && 'Registrasi'}
              {type == 'PATCH' && 'Ubah data'}
          </Button>
      </Form.Item>
  </Form>
  )
}