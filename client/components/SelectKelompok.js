import { Select } from "antd";
import { useEffect, useState } from "react";
import PropTypes from 'prop-types'
import axios from "axios";

export default function SelectKelompok(props)
{
  const [kelompoks, setKelompoks] = useState([])
  const {desaId} = props

  let fetched = false

  useEffect(() =>
  {
    if(fetched)
      return

    fetched = true

    axios.get(`${process.env.API_BASE_URL}/api/kelompok`)
    .then(res =>
    {
      setKelompoks(res.data);     
    })       
    .catch(e =>
    {
      console.error(e);
    })

  }, [])

  const options = kelompoks
  .filter(kelompok => !props.onlyId || kelompok.id == props.onlyId)
  .map(kelompok => 
    kelompok.fkDesa_id == desaId ?
      <Select.Option key={kelompok.id} value={kelompok.id}>
        {kelompok.name}
      </Select.Option> : null
  )


  return(
    <Select 
      placeholder='Pilih kelompok'      
      {...props}
    >      
      {options}
    </Select>
  )
}

SelectKelompok.propTypes = {
  desaId: PropTypes.number
}