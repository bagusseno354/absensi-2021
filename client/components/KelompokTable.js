import { Table, Space, Modal, Button, Popconfirm, message, Row, Col } from "antd";
import { useEffect, useState } from "react";
import KelompokForm from './KelompokForm'
import { PlusCircleOutlined } from '@ant-design/icons'
import Title from 'antd/lib/typography/Title'

export default function KelompokTable(props)
{
  const [kelompoks, setKelompoks] = useState(null)
  const [columns, setColumns] = useState(null)
  const [dataVersion, setDataVersion] = useState(0)
  const [showPostForm, setShowPostForm] = useState(false)
  const [showUpdateForm, setShowUpdateForm] = useState(false)
  const [kelompokIdToUpdate, setKelompokIdToUpdate] = useState(null)

  let fetched = false

  useEffect(() =>
  {
    if(fetched)
      return

    fetched = true

    fetch(props.url ? props.url : `${process.env.API_BASE_URL}/api/kelompok?where[fKDesa_id][__eq]=${props.desaId}`)
    .then(res =>
    {
      if(res.status == 404)
        setKelompoks([])

      if(!res.ok)
        return Promise.reject(res.status)
      
      return res.json()      
    })    
    .then(json =>
    {              
      const {id, ...columns} = json[0]

      setKelompoks(json)

      if(json.length == 0)
        return;

      const newColumns = Object.keys(columns).map(column => 
      {
        return ({
          title: column.charAt(0).toUpperCase() + column.slice(1),
          dataIndex: column,
          key: column,    
        })
      })

      if(props.enableActions)
        newColumns.push({
          title: 'Action',
          dataIndex: 'action',
          key: 'action',
          render: (text, record) =>
          (
            <Space size="middle">
              <a onClick={() => edit(record.id)}>Edit</a>
              <Popconfirm title="Yakin menghapus kelompok ini?" placement="leftTop" onConfirm={() => del(record.id)} okText="Yes" cancelText="No">
                <a>Delete</a>
              </Popconfirm>
            </Space>
          )
        })

      setColumns(newColumns)
    })
    .catch(e =>
    {
      console.error(e);
    })
  }, [dataVersion])

  const onSuccess = () => 
  {
    setDataVersion(dataVersion+1)

    if(showUpdateForm)
      setShowUpdateForm(false)
  }

  const edit = (kelompokId) =>
  {
    setKelompokIdToUpdate(kelompokId)
    setShowUpdateForm(true)
  }

  const del = (kelompokId) =>
  {
    const hide = message.loading('Menghapus...')

    fetch(`${process.env.API_BASE_URL}/api/kelompok/${kelompokId}`, {
      method: 'DELETE',
      credentials: 'include'
    }).then(res =>
    {
      hide() 
      
      setDataVersion(prev => prev+1)

      if(!res.ok)
        return Promise.reject(res.status)

      message.success('Terhapus')    
    })
    .catch(e =>
    {
      message.error(`Gagal menghapus! Error: ${e}`)

      console.error(e);
    })
  }

  return(
    <div style={{overflow: 'scroll'}}>
      <Row>
        <Col span={12} style={{alignItems: 'center', display: 'flex'}}>
          Daftar kelompok
        </Col>
        <Col span={12} style={{textAlign: 'right'}}>
          <Button 
            icon={<PlusCircleOutlined />} 
            type="primary"
            onClick={() => setShowPostForm(true)}
          >
            Tambah data
          </Button>
        </Col>
      </Row>
      <Table dataSource={kelompoks} columns={columns} style={{marginTop: 24}} />
      <Modal 
        visible={showPostForm}
        onCancel={() => setShowPostForm(false)}
        footer={[]}
      >
        <Title level={2}>
          Tambah data
        </Title>
        <KelompokForm desaId={props.desaId} onSuccess={onSuccess} type='POST' resetFieldsOnSuccess={true} />
      </Modal>
      <Modal 
        visible={showUpdateForm}
        onCancel={() => setShowUpdateForm(false)}
        footer={[]}
      >
        <Title level={2}>
          Ubah data
        </Title>
        <KelompokForm onSuccess={onSuccess} type='PATCH' kelompokId={kelompokIdToUpdate} />
      </Modal>
    </div>
  )
}