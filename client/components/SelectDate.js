import { Select } from "antd";
import getDaysInMonth from '../lib/get-days-in-month';

export default function SelectDate(props)
{    
  const max = getDaysInMonth(props.month, props.year).length
  const dates = [...Array(max).keys()]

  const datesOptions = dates.map((v, k) => <Select.Option key={k+1} value={k+1}>{k+1}</Select.Option>)

  return(
    <Select {...props}>
      {datesOptions}
    </Select>
  )
}