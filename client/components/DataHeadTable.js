import { Table, Space, Modal, Button, Popconfirm, message, Row, Col } from "antd";
import { useEffect, useState } from "react";
import DataHeadForm from './DataHeadForm'
import { PlusCircleOutlined } from '@ant-design/icons'
import Title from 'antd/lib/typography/Title'
import Link from "next/link";

export default function DataHeadTable(props)
{
  const [dataHeads, setDataHeads] = useState(null)
  const [columns, setColumns] = useState(null)
  const [dataVersion, setDataVersion] = useState(0)
  const [showPostForm, setShowPostForm] = useState(false)
  const [showUpdateForm, setShowUpdateForm] = useState(false)
  const [dataHeadIdToUpdate, setDataHeadIdToUpdate] = useState(null)

  let fetchedDataHead = false

  useEffect(() =>
  {
    if(fetchedDataHead)
      return

    fetchedDataHead = true

    fetch(props.url ? props.url : `${process.env.API_BASE_URL}/api/dataHead`)
    .then(res =>
    {
      if(res.status == 404)
        setDataHeads([])

      if(!res.ok)
        return Promise.reject(res.status)
      
      return res.json()      
    })    
    .then(json =>
    {              
      const {id, fkUser_creatorId, updatedAt, ...columns} = json[0]

      json = json.map((v, k) => ({key: k, ...v}))
      
      setDataHeads(json)

      if(json.length == 0)
        return;

      let newColumns = [];

      if(columns)
        newColumns = Object.keys(columns).map(column => 
        {
          if(column == 'name')
            return ({
              title: column.charAt(0).toUpperCase() + column.slice(1),
              dataIndex: column,
              key: column,              
              render: (text, record) =>
              (
                <Link href={`/admin/data/${record.id}`}>{record.name}</Link> 
              )
            })

          return ({
            title: column.charAt(0).toUpperCase() + column.slice(1),
            dataIndex: column,
            key: column,    
          })
        })

      if(props.enableActions)
        newColumns.push({
          title: 'Action',
          dataIndex: 'action',
          key: 'action',
          render: (text, record) =>
          (
            <Space size="middle">
              <a onClick={() => edit(record.id)}>Edit</a>
              <Popconfirm title="Yakin menghapus data ini? Seluruh data orang beserta presensi yang terkait pada data ini akan ikut terhapus!" placement="leftTop" onConfirm={() => del(record.id)} okText="Yes" cancelText="No">
                <a>Delete</a>
              </Popconfirm>
            </Space>
          )
        })

      setColumns(newColumns)
    })
    .catch(e =>
    {
      console.error(e);
    })
  }, [dataVersion])

  const onSuccess = () => 
  {
    setDataVersion(dataVersion+1)

    if(showUpdateForm)
      setShowUpdateForm(false)
  }

  const edit = (dataHeadId) =>
  {
    setDataHeadIdToUpdate(dataHeadId)
    setShowUpdateForm(true)
  }

  const del = (dataHeadId) =>
  {
    const hide = message.loading('Menghapus...')

    fetch(`${process.env.API_BASE_URL}/api/dataHead/${dataHeadId}`, 
    {
      method: 'DELETE',
      credentials: 'include'
    }).then(res =>
    {
      hide()    
      
      if(!res.ok)     
        return Promise.reject(res.status)      

      message.success('Terhapus')
      
      setDataVersion(prev => prev+1)      
    })
    .catch(e =>
    {
      message.error(`Gagal menghapus! Error: ${e}`)

      console.error(e);
    })
  }

  return(
    <div style={{overflow: 'scroll'}}>
      <Row>
        <Col span={12} style={{alignItems: 'center', display: 'flex'}}>
          Daftar data
        </Col>
        <Col span={12} style={{textAlign: 'right'}}>
          <Button 
            icon={<PlusCircleOutlined />} 
            type="primary"
            onClick={() => setShowPostForm(true)}
          >
            Tambah data
          </Button>
        </Col>
      </Row>
      <Table dataSource={dataHeads} columns={columns} style={{marginTop: 24}} />
      <Modal 
        visible={showPostForm}
        onCancel={() => setShowPostForm(false)}
        footer={[]}
      >
        <Title level={2}>
          Tambah data
        </Title>
        <DataHeadForm onSuccess={onSuccess} type='POST' resetFieldsOnSuccess={true} />
      </Modal>
      <Modal 
        visible={showUpdateForm}
        onCancel={() => setShowUpdateForm(false)}
        footer={[]}
      >
        <Title level={2}>
          Ubah data
        </Title>
        <DataHeadForm onSuccess={onSuccess} type='PATCH' dataHeadId={dataHeadIdToUpdate} />
      </Modal>
    </div>
  )
}