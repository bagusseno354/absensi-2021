import { Table, Space, Modal, Button, Popconfirm, message, Row, Col } from "antd";
import { useEffect, useState } from "react";
import UserFormPost from './UserForm/UserFormPost'
import UserFormPatch from './UserForm/UserFormPatch'
import { PlusCircleOutlined } from '@ant-design/icons'
import Title from 'antd/lib/typography/Title'

export default function UserTable(props)
{
  const [users, setUsers] = useState(null)
  const [columns, setColumns] = useState(null)
  const [dataVersion, setDataVersion] = useState(0)
  const [callTrigger, trigger] = useState(0)
  const [showPostForm, setShowPostForm] = useState(false)
  const [showUpdateForm, setShowUpdateForm] = useState(false)
  const [dataIdToUpdate, setDataIdToUpdate] = useState(null)

  let fetched = false

  useEffect(() =>
  {
    if(fetched)
      return

    fetched = true

    fetch(props.url ? props.url : `${process.env.API_BASE_URL}/api/user`, {
      credentials: 'include'
    })
    .then(res =>
    {
      if(res.status == 404)
        setUsers([])

      if(!res.ok)
        return Promise.reject(res.status)
      
      return res.json()      
    })    
    .then(json =>
    {              
      const {id, fkUser_creatorId, updatedAt, ...columns} = json[0]
      
      setUsers(json)

      if(json.length == 0)
        return;

      const newColumns = Object.keys(columns).map(column => 
      {
        if(column == 'name')
          return ({
            title: column.charAt(0).toUpperCase() + column.slice(1),
            dataIndex: column,
            key: column,              
            render: (text, record) =>
            (
              <span>{record.name}</span>
            )
          })

        return ({
          title: column.charAt(0).toUpperCase() + column.slice(1),
          dataIndex: column,
          key: column,    
        })
      })

      if(props.enableActions)
        newColumns.push({
          title: 'Action',
          dataIndex: 'action',
          key: 'action',
          render: (text, record) =>
          (            
            <Space size="middle">
              <a onClick={() => edit(record.id)}>Edit</a>
              {record.fkRole_id != 'superadmin' && <Popconfirm title="Yakin menghapus user ini?" placement="leftTop" onConfirm={() => del(record.id)} okText="Yes" cancelText="No">
                <a>Delete</a>
              </Popconfirm>}
            </Space>
          )
        })

      setColumns(newColumns)
    })
    .catch(e =>
    {
      console.error(e);
    })
  }, [dataVersion])

  const onSuccess = () => 
  {
    setDataVersion(dataVersion+1)

    if(showUpdateForm)
      setShowUpdateForm(false)
  }

  const edit = (userId) =>
  {
    trigger(Math.random())
    setDataIdToUpdate(userId)
    setShowUpdateForm(true)    
  }

  const del = (userId) =>
  {
    const hide = message.loading('Menghapus...')

    fetch(`${process.env.API_BASE_URL}/api/user/${userId}`, {
      method: 'DELETE',
      credentials: 'include'
    }).then(res =>
    {
      hide() 

      if(!res.ok)
        return Promise.reject(res.status)

      message.success('Terhapus')
      
      setDataVersion(prev => prev+1)

    })
    .catch(e =>
    {
      message.error(`Gagal menghapus! Error: ${e}`)

      console.error(e);
    })
  }

  return(
    <div style={{overflow: 'scroll'}}>
      <Row>
        <Col span={12} style={{alignItems: 'center', display: 'flex'}}>
          Daftar user
        </Col>
        <Col span={12} style={{textAlign: 'right'}}>
          <Button 
            icon={<PlusCircleOutlined />} 
            type="primary"
            onClick={() => setShowPostForm(true)}
          >
            Tambah user
          </Button>
        </Col>
      </Row>
      <Table dataSource={users} columns={columns} style={{marginTop: 24}} enableActions={true} />
      <Modal 
        visible={showPostForm}
        onCancel={() => setShowPostForm(false)}
        footer={[]}
      >
        <Title level={2}>
          Tambah user
        </Title>
        <UserFormPost onSuccess={onSuccess} resetFieldsOnSuccess={true} />
      </Modal>
      <Modal 
        visible={showUpdateForm}
        onCancel={() => setShowUpdateForm(false)}
        footer={[]}
      >
        <Title level={2}>
          Ubah user
        </Title>
        <UserFormPatch onSuccess={onSuccess} userId={dataIdToUpdate} callTrigger={callTrigger} />
      </Modal>
    </div>
  )
}