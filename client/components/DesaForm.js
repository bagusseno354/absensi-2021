import { Button, Form, Input, message } from 'antd';
import { useEffect } from 'react';
import { useRouter } from 'next/router';

export default function DesaForm(props)
{
  const router = useRouter()
  const [form] = Form.useForm()
  let isSubmitting = false;
  let action = 'menambah'

  // default
  const {type = 'POST'} = props

  if(type == 'PATCH')
    action = 'mengubah'

  const onFinish = (values) =>
  {
    if(isSubmitting)
      return

    isSubmitting = true

    const hide = message.loading(`Mencoba ${action}...`);
    
    const url = type == 'POST' ? `${process.env.API_BASE_URL}/api/desa` : `${process.env.API_BASE_URL}/api/desa/${props.desaId}`
    
    fetch(url, 
    {
      method: type,
      headers: {
        'Content-Type': 'application/json',      
      },
      body: JSON.stringify(values),
      credentials: 'include',
    })
    .then(res => 
    {
      isSubmitting = false;
      hide();

      if(res.ok)
      {
        message.success(`Berhasil ${action}`)     
        
        if(props.resetFieldsOnSuccess)
          form.resetFields()

        if(props.onSuccess)
          props.onSuccess()
          
        if(props.redirectUrl)
        {
          message.loading('Redirecting...')                
          router.push(props.redirectUrl)
        }        
      }
      else return Promise.reject(res.status)
    })
    .catch(e =>
    {
      console.error(e);

      isSubmitting = false

      message.error(`Gagal ${action}! Error: ${e}`)
    })
  }

  useEffect(() =>
  {
    if(props.desaId == null)
      return form.resetFields()
    
    fetch(`${process.env.API_BASE_URL}/api/desa?where[id][__eq]=${props.desaId}`)
    .then(res =>
    {
      if(!res.ok)
        return Promise.reject(res.status)
      
      return res.json()
    })
    .then(json =>
    {
      form.setFieldsValue(json[0])
    })
    .catch(e =>
    {
      console.error(e);
    })

  }, [props.desaId])

  return(
    <Form
      form={form}
      wrapperCol={{ span: 14 }}
      layout="vertical"
      onFinish={onFinish}>

      <Form.Item
          label='Nama desa'
          name='name'
          rules={[{ required: true, message: 'Wajib diisi' }]}
      >
          <Input /> 
      </Form.Item>
      
      <Form.Item>
          <Button type="primary" htmlType="submit">
              {type == 'POST' && 'Buat data baru'}
              {type == 'PATCH' && 'Ubah data'}
          </Button>
      </Form.Item>
  </Form>
  )
}