export default json =>
Object.assign({},
...function*() {
  // use negative lookahead to match ":" followed by null followed by comma or closing curly bracket character
  // replace null with "undefined"
  const o = JSON.parse(JSON.stringify(json).replace(/(?![:])null(?=[,}])/g, `"${void 0}"`));
  // set value to undefined
  for (const key in o) yield {[key]: o[key] === `"${void 0}"` ? void o[key] : o[key]}
}());