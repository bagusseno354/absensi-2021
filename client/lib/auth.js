import cookie from 'cookie'
import { generatePageError } from 'next-with-error';

function redirect(res)
{
    res.writeHead(302, {
        Location: '/admin/login'
    })
    res.end()
}

export async function getServerSideProps(context) 
{
    if(!context.req.headers.cookie)
    {
        redirect(context.res)
        return {}
    }

    const cookies = cookie.parse(context.req.headers.cookie)

    if(!cookies.token)
    {
        redirect(context.res)
        return {}
    }

    let res
    try {
        res = await fetch(`${process.env.API_BASE_URL}/api/user/me?include[Role][attributes]=id,name&include[User_kelompok][include][Kelompok][attributes]=id,name&include[User_kelompok][include][Kelompok][include][Desa][attributes]=id,name&include[User_desa][include][Desa][attributes]=id,name`, {
            headers: {
              'cookie': `token=${cookies.token}`,
              'Content-Type': 'application/json'
            },
        })  
    }
    catch(e)
    {
        console.log(e);
        return generatePageError(500);
    }

    if(!res.ok)
    {
        redirect(context.res)
        return {
            props: {
                ...context.props
            }
        }
    }

    const user = await res.json()    

    return {
        props: {
            user,
            ...context.props
        }
    }
}